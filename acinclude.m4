dnl
dnl $Id: acinclude.m4,v 1.6 2009-02-09 23:09:50 hehi Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILING],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([profiling],
	        [AC_HELP_STRING([--enable-profiling],
			        [Compile code to enable profiling])],
                [],[enable_profiling=no])
  if test "x$enable_profiling" = "xyes" ; then 
    CFLAGS="$CFLAGS -pg" 
    CXXFLAGS="$CXXFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  fi
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_STRICT],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([strict],
	        [AC_HELP_STRING([--enable-strict],
			        [Require strictly correct code])],
                [],[enable_strict=no])
  if test "x$enable_strict" = "xyes" ; then 
    CFLAGS="$CFLAGS -Wall -Werror -pedantic -ansi" 
    CXXFLAGS="$CXXFLAGS -Wall -Werror -pedantic -ansi"
  fi
])

dnl ------------------------------------------------------------------
dnl
dnl AC_ROOT_RCUGUI([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUGUI],
[
    AC_REQUIRE([ROOT_PATH])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcugui-prefix],
        [AC_HELP_STRING([--with-rcugui-prefix],
		[Prefix where Rcugui is installed])],
        rcugui_prefix=$withval, rcugui_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcugui-url],
        [AC_HELP_STRING([--with-rcugui-url],
		[Base URL where the Rcugui dodumentation is installed])],
        rcugui_url=$withval, rcugui_url="")
    if test "x${RCUGUI_CONFIG+set}" != xset ; then 
        if test "x$rcugui_prefix" != "x" ; then 
	    RCUGUI_CONFIG=$rcugui_prefix/bin/rcugui-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUGUI_CONFIG, rcugui-config, no)
    rcugui_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Rcugui version >= $rcugui_min_version)

    # Check if we got the script
    rcugui_found=no    
    if test "x$RCUGUI_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUGUI_CPPFLAGS=`$RCUGUI_CONFIG --cppflags`
       RCUGUI_INCLUDEDIR=`$RCUGUI_CONFIG --includedir`
       RCUGUI_LIBS=`$RCUGUI_CONFIG --libs`
       RCUGUI_LTLIBS=`$RCUGUI_CONFIG --ltlibs`
       RCUGUI_LIBDIR=`$RCUGUI_CONFIG --libdir`
       RCUGUI_LDFLAGS=`$RCUGUI_CONFIG --ldflags`
       RCUGUI_LTLDFLAGS=`$RCUGUI_CONFIG --ltldflags`
       RCUGUI_PREFIX=`$RCUGUI_CONFIG --prefix`
       RCUGUI_READER=`$RCUGUI_CONFIG --have-reader`
       RCUGUI_LOWLEVEL=`$RCUGUI_CONFIG --have-lowlevel`

       # Check the version number is OK.
       rcugui_version=`$RCUGUI_CONFIG -V` 
       rcugui_vers=`echo $rcugui_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcugui_regu=`echo $rcugui_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcugui_vers -ge $rcugui_regu ; then 
            rcugui_found=yes
       fi
    fi
    AC_MSG_RESULT($rcugui_found - is $rcugui_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUGUI, [Whether we have rcugui])


    if test "x$rcugui_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUGUI_LIBDIR $RCUGUI_LIBS $ROOTAUXLIBS $ROOTGLIBS"
    	CPPFLAGS="$CPPFLAGS $RCUGUI_CPPFLAGS $ROOTCFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcugui_linkedtree_h=0
        AC_CHECK_HEADER([rcugui/LinkedTree.h], [have_rcugui_linkedtree_h=1])

        # Check the library. 
        have_librcugui=no
        AC_MSG_CHECKING(for -lrcugui)


        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcugui/LinkedTree.h>],
                        [TGCompositeFrame p;
			 RcuGui::LinkedTree t(p);])], 
                        [have_librcugui=yes])
        AC_MSG_RESULT($have_librcugui)

        if test $have_rcugui_linkedtree_h -gt 0    && \
            test "x$have_librcugui"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUGUI)
        else 
            rcugui_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the Rcugui documentation is installed)
    if test "x$rcugui_url" = "x" && \
	test ! "x$RCUGUI_PREFIX" = "x" ; then 
       RCUGUI_URL=${RCUGUI_PREFIX}/share/doc/rcugui/html
    else 
	RCUGUI_URL=$rcugui_url
    fi	
    AC_MSG_RESULT($RCUGUI_URL)
   
    if test "x$rcugui_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUGUI_URL)
    AC_SUBST(RCUGUI_PREFIX)
    AC_SUBST(RCUGUI_CPPFLAGS)
    AC_SUBST(RCUGUI_INCLUDEDIR)
    AC_SUBST(RCUGUI_LDFLAGS)
    AC_SUBST(RCUGUI_LIBDIR)
    AC_SUBST(RCUGUI_LIBS)
    AC_SUBST(RCUGUI_LTLIBS)
    AC_SUBST(RCUGUI_LTLDFLAGS)
    AC_SUBST(RCUGUI_READER)
    AC_SUBST(RCUGUI_LOWLEVEL)
])

dnl
dnl AC_RCUDATA([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUDATA],
[
    AC_REQUIRE([ROOT_PATH])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcudata-prefix],
        [AC_HELP_STRING([--with-rcudata-prefix],
		[Prefix where RcuData is installed])],
        rcudata_prefix=$withval, rcudata_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcudata-url],
        [AC_HELP_STRING([--with-rcudata-url],
		[Base URL where the RcuData dodumentation is installed])],
        rcudata_url=$withval, rcudata_url="")
    if test "x${RCUDATA_CONFIG+set}" != xset ; then 
        if test "x$rcudata_prefix" != "x" ; then 
	    RCUDATA_CONFIG=$rcudata_prefix/bin/rcudata-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUDATA_CONFIG, rcudata-config, no)
    rcudata_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuData version >= $rcudata_min_version)

    # Check if we got the script
    rcudata_found=no    
    if test "x$RCUDATA_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUDATA_CPPFLAGS=`$RCUDATA_CONFIG --cppflags`
       RCUDATA_INCLUDEDIR=`$RCUDATA_CONFIG --includedir`
       RCUDATA_LIBS=`$RCUDATA_CONFIG --libs`
       RCUDATA_LTLIBS=`$RCUDATA_CONFIG --ltlibs`
       RCUDATA_LIBDIR=`$RCUDATA_CONFIG --libdir`
       RCUDATA_LDFLAGS=`$RCUDATA_CONFIG --ldflags`
       RCUDATA_LTLDFLAGS=`$RCUDATA_CONFIG --ltldflags`
       RCUDATA_PREFIX=`$RCUDATA_CONFIG --prefix`
       RCUDATA_READER=`$RCUDATA_CONFIG --have-reader`
       RCUDATA_LOWLEVEL=`$RCUDATA_CONFIG --have-lowlevel`

       # Check the version number is OK.
       rcudata_version=`$RCUDATA_CONFIG -V` 
       rcudata_vers=`echo $rcudata_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcudata_regu=`echo $rcudata_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcudata_vers -ge $rcudata_regu ; then 
            rcudata_found=yes
       fi
    fi
    AC_MSG_RESULT($rcudata_found - is $rcudata_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUDATA_CHANNEL_H, 
                [Whether we have rcudata/Channel.h header])
    AH_TEMPLATE(HAVE_RCUDATA, [Whether we have rcudata])


    if test "x$rcudata_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUDATA_LIBDIR $RCUDATA_LIBS -L$ROOTLIBDIR $ROOTLIBS $ROOTAUXLIBS"
    	CPPFLAGS="$CPPFLAGS $RCUDATA_CPPFLAGS $ROOTCFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcudata_channel_h=0
        AC_CHECK_HEADER([rcudata/Channel.h], [have_rcudata_channel_h=1])

        # Check the library. 
        have_librcudata=no
        AC_MSG_CHECKING(for -lrcudata)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcudata/Channel.h>],
                        [new RcuData::Channel;])], 
                        [have_librcudata=yes])
        AC_MSG_RESULT($have_librcudata)

        if test $have_rcudata_channel_h -gt 0    && \
            test "x$have_librcudata"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUDATA)
        else 
            rcudata_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the RcuData documentation is installed)
    if test "x$rcudata_url" = "x" && \
	test ! "x$RCUDATA_PREFIX" = "x" ; then 
       RCUDATA_URL=${RCUDATA_PREFIX}/share/doc/rcudata/html
    else 
	RCUDATA_URL=$rcudata_url
    fi	
    AC_MSG_RESULT($RCUDATA_URL)
   
    if test "x$rcudata_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUDATA_URL)
    AC_SUBST(RCUDATA_PREFIX)
    AC_SUBST(RCUDATA_CPPFLAGS)
    AC_SUBST(RCUDATA_INCLUDEDIR)
    AC_SUBST(RCUDATA_LDFLAGS)
    AC_SUBST(RCUDATA_LIBDIR)
    AC_SUBST(RCUDATA_LIBS)
    AC_SUBST(RCUDATA_LTLIBS)
    AC_SUBST(RCUDATA_LTLDFLAGS)
    AC_SUBST(RCUDATA_READER)
    AC_SUBST(RCUDATA_LOWLEVEL)
])

dnl ------------------------------------------------------------------

dnl
dnl $Id: acinclude.m4,v 1.6 2009-02-09 23:09:50 hehi Exp $
dnl $Author: hehi $
dnl $Date: 2009-02-09 23:09:50 $
dnl
dnl Autoconf macro to check for existence or ROOT on the system
dnl Synopsis:
dnl
dnl  ROOT_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples: 
dnl 
dnl    ROOT_PATH(3.03/05, , AC_MSG_ERROR(Your ROOT version is too old))
dnl    ROOT_PATH(, AC_DEFINE([HAVE_ROOT]))
dnl 
dnl The macro defines the following substitution variables
dnl
dnl    ROOTCONF           full path to root-config
dnl    ROOTEXEC           full path to root
dnl    ROOTCINT           full path to rootcint
dnl    ROOTLIBDIR         Where the ROOT libraries are 
dnl    ROOTINCDIR         Where the ROOT headers are 
dnl    ROOTCFLAGS         Extra compiler flags
dnl    ROOTLIBS           ROOT basic libraries 
dnl    ROOTGLIBS          ROOT basic + GUI libraries
dnl    ROOTAUXLIBS        Auxilary libraries and linker flags for ROOT
dnl    ROOTAUXCFLAGS      Auxilary compiler flags 
dnl    ROOTRPATH          Same as ROOTLIBDIR
dnl
dnl The macro will fail if root-config and rootcint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF, root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC, root , no, $rootbin)
  AC_PATH_PROG(ROOTCINT, rootcint , no, $rootbin)
	
  if test ! x"$ROOTCONF" = "xno" && \
     test ! x"$ROOTCINT" = "xno" ; then 

    # define some variables 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
	
    if test $1 ; then 
      AC_MSG_CHECKING(wether ROOT version >= [$1])
      vers=`$ROOTCONF --version | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      requ=`echo $1 | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    # otherwise, we say no_root
    no_root="yes"
  fi

  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)

  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

#
# EOF
#
