//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Acq.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:31:44 2006
    @brief   
    @ingroup fmdxx    
*/
#include <fmdxx/Acq.h>
#include <rcuxx/DebugGuard.h>
#include <TFile.h>
#include <TParameter.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/fmd/FmdAL_ANA_I.h>
#include <rcuxx/fmd/FmdAL_ANA_U.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdAL_DIG_U.h>
#include <rcuxx/fmd/FmdCalIter.h>
#include <rcuxx/fmd/FmdClock.h>
#include <rcuxx/fmd/FmdCommand.h>
#include <rcuxx/fmd/FmdConstants.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/fmd/FmdGTL_U.h>
#include <rcuxx/fmd/FmdHoldWait.h>
#include <rcuxx/fmd/FmdL0Timeout.h>
#include <rcuxx/fmd/FmdL0Triggers.h>
#include <rcuxx/fmd/FmdL1Timeout.h>
#include <rcuxx/fmd/FmdL1Triggers.h>
#include <rcuxx/fmd/FmdPulser.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdSampleClock.h>
#include <rcuxx/fmd/FmdShapeBias.h>
#include <rcuxx/fmd/FmdShiftClock.h>
#include <rcuxx/fmd/FmdStatus.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdTopBottom.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdVA_REC_UM.h>
#include <rcuxx/fmd/FmdVA_REC_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_IM.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_SUP_UM.h>
#include <rcuxx/fmd/FmdVA_SUP_UP.h>
#include <rcuxx/fmd/FmdVFP.h>
#include <rcuxx/fmd/FmdVFS.h>
#include <rcuxx/rcu/RcuACTFEC.h>


//____________________________________________________________________
void
Fmdxx::Acq::SetRange(unsigned int min, unsigned int max) 
{
  if (max > 127)  max = 127;
  if (min > max)  min = max;
  fMinStrip           = min;
  fMaxStrip           = max;
}

//____________________________________________________________________
unsigned int 
Fmdxx::Acq::Setup(int          nRun, 
		  int          nevents, 
		  Trigger_t    mode,
		  unsigned int mask, 
		  unsigned int addr)
{
  Rcuxx::DebugGuard g(fDebug, "FmdAcq::Setup");
  std::cout << "Setting up the FMD " << std::flush;
  unsigned int ret = 0;

#if 0  
  unsigned int i   = 4;
  unsigned int imem[32];
  imem[0] = 0x64000a; // Number of altro timebins
  imem[1] = 0x7003a0; // Default is 0x3a0 -> 928
  imem[2] = 0x660011; // BC CSR1 - 
  imem[3] = 0x70037f; // Turn off SCLK interrupt 

  if (fMinStrip != 0 || fMaxStrip != 127) {
    std::cout << "Range(" << fMinStrip << "," << fMaxStrip <<  ") " 
	      << std::flush;
    fFmd.Range()->SetMin(fMinStrip);
    fFmd.Range()->SetMax(fMaxStrip);
    fFmd.Range()->Set();
    fFmd.Range()->WriteInstructions(&(imem[i]));
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  if (fOver > 0) {
    std::cout << "Over(" << fOver << ") " << std::flush;
    // if ((ret = fFmd.SampleClock()->Update())) throw ret;
    // if ((ret = fFmd.ShiftClock()->Update())) throw ret;
    fFmd.ShiftClock()->SetDivision(fOver * 4);
    fFmd.ShiftClock()->Set();
    fFmd.ShiftClock()->WriteInstructions(&(imem[i]));
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
    // Set number of time bins to (# strips)*(oversampling)+4+margin
    size_t samples = fOver * (fMaxStrip - fMinStrip + 1) + 4 + 10;
    if (samples > 0x3a0) samples = 0x3a0;
    imem[1] = 0x700000 + samples;
  }  
  bool changeDacs = false;
  if (fPulser > 0) {
    std::cout << "Pulser(0x" << std::hex << fPulser << ") " 
	      << std::dec << std::flush;
    fFmd.Pulser()->SetValue(fPulser);
    fFmd.Pulser()->Set();
    fFmd.Pulser()->WriteInstructions(/*&(imem[i])*/);
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
    fFmd.PulserOn()->WriteInstructions(/*&(imem[i])*/);
    changeDacs = true;
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  if (fShape != 0x0) {
    std::cout << "Shape(0x" << std::hex << fShape << ") " 
	      << std::dec << std::flush;
    fFmd.ShapeBias()->Set(fShape);
    fFmd.ShapeBias()->Set();
    fFmd.ShapeBias()->WriteInstructions(/*&(imem[i])*/);
    changeDacs = true;
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  if (fVFS != 0x0) {
    std::cout << "VFS(0x" << std::hex << fVFS << ") " 
	      << std::dec << std::flush;
    fFmd.VFS()->Set(fVFS);
    fFmd.VFS()->Set();
    fFmd.VFS()->WriteInstructions(&(imem[i]));
    changeDacs = true;
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  if (fVFP != 0x0) {
    std::cout << "VFP(0x" << std::hex << fVFP << ") " 
	      << std::dec << std::flush;
    fFmd.VFP()->Set(fVFP);
    fFmd.VFP()->Set();
    fFmd.VFP()->WriteInstructions(&(imem[i]));
    changeDacs = true;
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  if (fHold != 0x0) {
    std::cout << "Hold(" << fHold << ") " << std::flush;
    fFmd.HoldWait()->SetClocks(fHold);
    fFmd.HoldWait()->Set();
    fFmd.HoldWait()->WriteInstructions(&(imem[i]));
    i += 3;
    imem[i] = 0x003a000a; // Wait 10 clock cycles
  }
  
  if (changeDacs) {
    std::cout << " DAC" << std::flush;
    fFmd.ChangeDacs()->WriteInstructions(&(imem[i]));
    i += 3;
    imem[i] = 0x003a00FF; // Wait 255 clock cycles
  }
  imem[i] = 0x390000;
  std::cout << std::endl;
  // fRcu.IMEM()->Set(0, i+1, imem);
#endif
  
  ret = RcuData::Acq::Setup(nRun, nevents, mode, mask, addr);
  return ret;
}

  

//____________________________________________________________________
unsigned int 
Fmdxx::Acq::WriteInfo()
{
  Rcuxx::DebugGuard g(fDebug, "FmdAcq::WriteInfo");
  unsigned int ret = RcuData::Acq::WriteInfo();
  typedef  TParameter<int> Param_t;
  fFile->cd();
  TDirectory* fmd = fFile->mkdir("Fmd");
  fmd->cd();
  // return 0;

  std::cout << "Writing FMD information to output\n  " << std::flush;
  Rcuxx::RcuACTFEC*    actfec = fRcu.ACTFEC();
  for (size_t i = 0; i < 4; i++) {
    if (!actfec->IsOn(i)) continue;
    std::cout << "  Board # " << std::setw(2) << i << ": " << std::flush;

    fmd->cd();
    TDirectory* board = fmd->mkdir(Form("board_%02d", i));
    board->cd();
    
    // Shaping biases 
    Rcuxx::FmdShapeBias* shapebias = fFmd.ShapeBias();
    if (shapebias) {
      shapebias->SetAddress(i);
      if ((ret = shapebias->Update())) return ret;
      if (shapebias->Top() == 0 || shapebias->Bottom() == 0) {
	std::cerr << "Warning, FmdAcq: unlikely values of shaping biases" 
		  << std::endl;
	shapebias->Print();
      }
      TDirectory* d = board->mkdir(shapebias->Name().c_str());
      d->cd();
      Param_t* top = new Param_t("top",shapebias->Top()); top->Write();
      Param_t* bot = new Param_t("bot",shapebias->Bottom()); bot->Write();
      std::cout << "Shape(" << shapebias->Top() << "," << shapebias->Bottom() 
		<< ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // VFP
    Rcuxx::FmdVFP* vfp = fFmd.VFP();
    if (vfp) {
      vfp->SetAddress(i);
      if ((ret = vfp->Update())) return ret;
      if (vfp->Top() == 0 || vfp->Bottom() == 0) {
	std::cerr << "Warning, FmdAcq: unlikely values of vfp biases"
		  << std::endl;
	vfp->Print();
      }
      TDirectory* d = board->mkdir(vfp->Name().c_str());
      d->cd();
      Param_t* top = new Param_t("top",vfp->Top()); top->Write();
      Param_t* bot = new Param_t("bot",vfp->Bottom()); bot->Write();
      std::cout << "VFP(" << vfp->Top() << "," << vfp->Bottom() << ") " 
		<< std::flush;
      usleep(fWait);
      board->cd();
    }

    // VFS 
    Rcuxx::FmdVFS* vfs = fFmd.VFS();
    if (vfs) {
      vfs->SetAddress(i);
      if ((ret = vfs->Update())) return ret;
      if (vfs->Top() == 0 || vfs->Bottom() == 0) {
	std::cerr << "Warning, FmdAcq: unlikely values of vfs biases"
		  << std::endl;
	vfs->Print();
      }
      TDirectory* d = board->mkdir(vfs->Name().c_str());
      d->cd();
      Param_t* top = new Param_t("top",vfs->Top()); top->Write();
      Param_t* bot = new Param_t("bot",vfs->Bottom()); bot->Write();
      std::cout << "VFS(" << vfs->Top() << "," << vfs->Bottom() << ") " 
		<< std::flush;
      usleep(fWait);
      board->cd();
    }

    // Pulser 
    Rcuxx::FmdPulser* pulser = fFmd.Pulser();
    if (pulser) {
      pulser->SetAddress(i);
      if ((ret = pulser->Update())) return ret;
      TDirectory* d = board->mkdir(pulser->Name().c_str());
      d->cd();    
      Param_t* value = new Param_t("value",pulser->Value()); value->Write();
      Param_t* test = new Param_t("test",pulser->Test()); test->Write();
      std::cout << "Pulser(" << pulser->Value() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // Shift clock 
    Rcuxx::FmdShiftClock* shiftclock = fFmd.ShiftClock();
    if (shiftclock) {
      shiftclock->SetAddress(i);
      if ((ret = shiftclock->Update())) return ret;
      if (shiftclock->Division() == 0) {
	std::cerr << "Warning, FmdAcq: invalid value of shift clock division"
		  << std::endl;
	shiftclock->Print();
	return 1;
      }
      TDirectory* d = board->mkdir(shiftclock->Name().c_str());
      d->cd();
      Param_t* div = new Param_t("div", shiftclock->Division()); div->Write();
      Param_t* phase = new Param_t("phase",shiftclock->Phase());phase->Write();
      std::cout << "Shift(" << shiftclock->Division() << "," 
		<< shiftclock->Phase() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // Sample clock
    Rcuxx::FmdSampleClock* sampleclock = fFmd.SampleClock();
    if (sampleclock) {
      sampleclock->SetAddress(i);
      if ((ret = sampleclock->Update())) return ret;
      if (sampleclock->Division() == 0) {
	std::cerr << "Warning, FmdAcq: unvalid value of sample clock division "
		  << std::endl;
	sampleclock->Print();
	return 1;
      }
      TDirectory* d = board->mkdir(sampleclock->Name().c_str());
      d->cd();
      Param_t* div = new Param_t("div",sampleclock->Division()); div->Write();
      Param_t* phase =new Param_t("phase",sampleclock->Phase());phase->Write();
      std::cout << "Sample(" << sampleclock->Division() << "," 
		<< sampleclock->Phase() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // Hold wait
    Rcuxx::FmdHoldWait* holdwait = fFmd.HoldWait();
    if (holdwait) {
      holdwait->SetAddress(i);
      if ((ret = holdwait->Update())) return ret;
      TDirectory* d = board->mkdir(holdwait->Name().c_str());
      d->cd();
      Param_t* clocks = new Param_t("clocks",holdwait->Clocks());
      clocks->Write();
      std::cout << "Hold(" << holdwait->Clocks() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }
    
    // L0 timeout
    Rcuxx::FmdL0Timeout* l0timeout = fFmd.L0Timeout();
    if (l0timeout) {
      l0timeout->SetAddress(i);
      if ((ret = l0timeout->Update())) return ret;
      TDirectory* d = board->mkdir(l0timeout->Name().c_str());
      d->cd();
      Param_t* clocks = new Param_t("clocks",l0timeout->Clocks());
      clocks->Write();
      std::cout << "L0(" << l0timeout->Clocks() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // L1 timeout 
    Rcuxx::FmdL1Timeout* l1timeout = fFmd.L1Timeout();
    if (l1timeout) {
      l1timeout->SetAddress(i);
      if ((ret = l1timeout->Update())) return ret;
      TDirectory* d = board->mkdir(l1timeout->Name().c_str());
      d->cd();
      Param_t* clocks = new Param_t("clocks",l1timeout->Clocks());
      clocks->Write();
      std::cout << "L1(" << l1timeout->Clocks() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }
  
    Rcuxx::FmdRange* range = fFmd.Range();
    if (range) {
      range->SetAddress(i);
      if ((ret = range->Update())) return ret;
      TDirectory* d = board->mkdir(range->Name().c_str());
      d->cd();
      Param_t* min = new Param_t("min", range->Min()); min->Write();
      Param_t* max = new Param_t("max", range->Max()); max->Write();
      std::cout << "Range(" << range->Min() << "," << range->Max() << ") "
		<< std::flush;
      usleep(fWait);
      board->cd();
    }

    // Number of triggers
    Rcuxx::FmdL0Triggers* l0counts = fFmd.L0Triggers();
    if (l0counts) {
      l0counts->SetAddress(i);
      if ((ret = l0counts->Update())) return ret;
      TDirectory* d = board->mkdir(l0counts->Name().c_str());
      d->cd();
      Param_t* n = new Param_t("pre_run", l0counts->Recieved()); n->Write();
      std::cout << "L0counts(" << l0counts->Recieved() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }

    // Number of triggers
    Rcuxx::FmdL1Triggers* l1counts = fFmd.L1Triggers();
    if (l1counts) {
      l1counts->SetAddress(i);
      if ((ret = l1counts->Update())) return ret;
      TDirectory* d = board->mkdir(l1counts->Name().c_str());
      d->cd();
      Param_t* n = new Param_t("pre_run", l1counts->Recieved()); n->Write();
      std::cout << "L1counts(" << l1counts->Recieved() << ") " << std::flush;
      usleep(fWait);
      board->cd();
    }
    std::cout << std::endl;
  }
  return ret;
}

//____________________________________________________________________
unsigned int
Fmdxx::Acq::CleanUp(int ret) 
{
  Rcuxx::DebugGuard g(fDebug, "FmdAcq::CleanUp");
  typedef  TParameter<int> Param_t;
  ret = Rcuxx::Acq::CleanUp(ret);
  if (!fFile || !fFile->cd()) {
    std::cerr << "couldn't cd to output file" << std::endl;
    return ret;
  }
  if (!fFile->cd("Fmd")) {
    std::cerr << "couldn't cd to 'Fmd' in " << fFile->GetName() << std::endl;
    return 0;  // ret;
  }
  TDirectory* fmd = gDirectory;

  Rcuxx::RcuACTFEC*    actfec = fRcu.ACTFEC();
  for (size_t i = 0; i < 4; i++) {
    if (!actfec->IsOn(i)) continue;
    std::cout << "  Board # " << std::setw(2) << i << ": " << std::flush;

    fmd->cd(Form("board_%02d", i));
    TDirectory* board = gDirectory;

    Rcuxx::FmdL0Triggers* l0counts = fFmd.L0Triggers();
    if (l0counts) {
      l0counts->SetAddress(i);
      if ((ret = l0counts->Update())) return ret;
      if (board->cd(l0counts->Name().c_str())) {
	Param_t* n = new Param_t("post_run", l0counts->Recieved()); n->Write();
	std::cout << "L0counts(" << l0counts->Recieved() << ") " << std::flush;
	usleep(fWait);
      }
      else 
	std::cerr << "Couldn't cd to '" << l0counts->Name() << "' in " 
		  << fFile->GetName() << std::endl;
      board->cd();
    }

    Rcuxx::FmdL1Triggers* l1counts = fFmd.L1Triggers();
    if (l1counts) {
      l1counts->SetAddress(i);
      if ((ret = l1counts->Update())) return ret;
      if (board->cd(l1counts->Name().c_str())) {
	Param_t* n = new Param_t("post_run", l1counts->Recieved()); n->Write();
	std::cout << "L1counts(" << l1counts->Recieved() << ") " << std::flush;
	usleep(fWait);
      }
      else 
	std::cerr << "Couldn't cd to '" << l1counts->Name() << "' in " 
		  << fFile->GetName() << std::endl;
      board->cd();
    }
    std::cout << std::endl;
  }
  return ret;
}

//____________________________________________________________________
int
Fmdxx::Acq::SoftwareTrigger()
{
  Rcuxx::DebugGuard g(fDebug, "FmdAcq::SoftwareTrigger");
  unsigned int ret = 0;
  if ((ret = fFmd.FakeTrigger()->Commit())) return ret;
  ret = fRcu.SWTRG()->Commit();
  return ret;
}

//____________________________________________________________________
//
// EOF
// 
