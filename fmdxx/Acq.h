// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:31:33 2006
    @brief   
    @ingroup fmdxx    
*/
#ifndef FMDXX__ACQ_H
#define FMDXX__ACQ_H
#ifndef RCUDATA_ACQ_H
# include <rcudata/Acq.h>
#endif
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif

/** @defgroup fmdxx Basic FMD classes 
 */
/** @namespace Fmdxx Base namespace for all FMD classes 
    @ingroup fmdxx
 */
namespace Fmdxx
{
  /** @class Acq
      @brief Abstract base class to do data aquisition. 
      @ingroup fmdxx
  */
  class Acq : public RcuData::Acq
  {
  public:
    /** Constructor
	@param fmd  Reference to FMD interface
	@param rcu  Reference to RCU object 
	@param out  Output file name.  If left blank, an automatic
	name, based on the run number will be chosen. */
    Acq(Rcuxx::Rcu& rcu, Rcuxx::Fmd& fmd, const char* out="") 
      : RcuData::Acq(rcu, out), 
	fFmd(fmd), 
	fOver(0),
	fShape(0), 
	fVFS(0), 
	fVFP(0), 
	fHold(0),
	fPulser(0),
	fMinStrip(0), 
	fMaxStrip(127)
    {}
    /** Set up 
	@param nRun Run number
	@param nevents Number of events to process
	@param mode The trigger mode. 
	@param mask Mask of stuff to commit, 
	@param addr Address to execute */ 
    virtual unsigned int Setup(int          nRun=0, 
			       int          nevents=-1, 
			       Trigger_t    mode=kSoftwareTrigger,
			       unsigned int mask=(kACTFEC|kFECRST|kTRCFG1|
						  kIMEM|kPMEM|kACL), 
			       unsigned int addr=0);

    /** Set range 
	@param min First strip 
	@param max Last strip */
    virtual void SetRange(unsigned int min, unsigned int max); 
    /** @param pulser Calibration pulser value */
    virtual void SetPulser(unsigned int pulser) { fPulser = (pulser & 0xff);}
    /** @param bias Shaping bias value */
    virtual void SetShapeBias(unsigned int bias) { fShape = (bias & 0xff); }
    /** @param vfs VFS bias value */
    virtual void SetVFS(unsigned int vfs) { fVFS = (vfs & 0xff); }
    /** @param vfp VFP bias value */
    virtual void SetVFP(unsigned int vfp) { fVFP = (vfp & 0xff); }
    /** @param hold Time from L0 to hold in 25ns steps */
    virtual void SetHoldWait(unsigned int hold) { fHold = (hold & 0xff); }
    /** @param over Oversampling rate */
    virtual void SetOver(unsigned int over) { fOver = (over & 0xff); }
  protected:
    /** Low-level interface to FMD */ 
    Rcuxx::Fmd& fFmd;
    /** Write information about run to the output file */
    virtual unsigned int WriteInfo();
    /** Create a software trigger.  
	@return error-code on failure, 0 on success */
    virtual int SoftwareTrigger();
    /** Clean up after run.  
	@param ret Return value from run 
	@return 0 on success, error code otherwise */
    virtual unsigned int CleanUp(int ret);
    /** Oversampling */
    unsigned int fOver;
    /** Shaping bias */ 
    unsigned int fShape;
    /** VFS */ 
    unsigned int fVFS;
    /** VFP */ 
    unsigned int fVFP;
    /** Holdwait */ 
    unsigned int fHold;
    /** Pulser */
    unsigned int fPulser;
    /** Minimum strip */
    unsigned int fMinStrip;
    /** Maximum strip */
    unsigned int fMaxStrip;    
  };
}

#endif  
//
// EOF
//
