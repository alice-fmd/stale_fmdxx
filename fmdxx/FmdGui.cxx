//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Main.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:27:23 2006
    @brief   
    @ingroup fmdxx_gui            
*/
#include <fmdxx/FmdGui.h>
#include <rcugui/Rcu.h>
#include <rcugui/LinkedView.h>
#include <rcuxx/DebugGuard.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/Rcu.h>
#include <iostream>
#include <algorithm>
#include <rcuxx/Rcu.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcugui/Main.h>
#include <rcugui/bc/Monitored.h>
#include <rcugui/rcu/ActiveChannels.h>
#include <iostream>
#include <rcuxx/fmd/FmdAL_ANA_I.h>
#include <rcuxx/fmd/FmdAL_ANA_U.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdAL_DIG_U.h>
#include <rcuxx/fmd/FmdCalIter.h>
#include <rcuxx/fmd/FmdClock.h>
#include <rcuxx/fmd/FmdCommand.h>
#include <rcuxx/fmd/FmdConstants.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/fmd/FmdGTL_U.h>
#include <rcuxx/fmd/FmdHoldWait.h>
#include <rcuxx/fmd/FmdL0Timeout.h>
#include <rcuxx/fmd/FmdL0Triggers.h>
#include <rcuxx/fmd/FmdL1Timeout.h>
#include <rcuxx/fmd/FmdL1Triggers.h>
#include <rcuxx/fmd/FmdMeb.h>
#include <rcuxx/fmd/FmdPulser.h>
#include <rcuxx/fmd/FmdRange.h>
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdSampleClock.h>
#include <rcuxx/fmd/FmdShapeBias.h>
#include <rcuxx/fmd/FmdShiftClock.h>
#include <rcuxx/fmd/FmdStatus.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdTopBottom.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdVA_REC_UM.h>
#include <rcuxx/fmd/FmdVA_REC_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_IM.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_SUP_UM.h>
#include <rcuxx/fmd/FmdVA_SUP_UP.h>
#include <rcuxx/fmd/FmdVFP.h>
#include <rcuxx/fmd/FmdVFS.h>

namespace 
{
  // bool fDebug;

  //==================================================================
  struct TopBottom : public RcuGui::Register 
  {
    TGLayoutHints           fHints;
    TGCompositeFrame        fFrame;
    RcuGui::LabeledIntEntry fTop;
    RcuGui::LabeledIntEntry fBot;
    Rcuxx::FmdTopBottom&    fLow;
    TopBottom(TGCompositeFrame& f, Rcuxx::FmdTopBottom& low, 
	      Int_t l=0, Int_t r=0,
	      Int_t opt=kHorizontalFrame)
      : RcuGui::Register(f, low, kHorizontalFrame, l, r, 0, 0), 
	fLow(low),
	fHints(kLHintsExpandX|kLHintsExpandY),
	fFrame(&fFields, 0, 0, kVerticalFrame),
	fTop(fFrame, "Top", 0, 0xff, opt),
	fBot(fFrame, "Bottom", 0, 0xff, opt)
    {
      fFields.AddFrame(&fFrame, &fHints);
      Get();
    }
    void Get()
    {
      fTop.SetValue(fLow.Top());
      fBot.SetValue(fLow.Bottom());
    }
    void Set()
    {
      fLow.SetTop(fTop.GetValue());
      fLow.SetBottom(fBot.GetValue());
    }
  };
  
  //====================================================================
  struct ShapeBias : public TopBottom
  {
    ShapeBias(TGCompositeFrame& f,Rcuxx::FmdShapeBias& low)
      : TopBottom(f,low, 0, 3)
    {}
  };
  struct VFP : public TopBottom
  {
    VFP(TGCompositeFrame& f, Rcuxx::FmdVFP& low): TopBottom(f, low, 3){}
  };
  struct VFS : public TopBottom
  {
    VFS(TGCompositeFrame& f, Rcuxx::FmdVFS& low) : TopBottom(f, low, 3) {}
  };
  //____________________________________________________________________
  struct Pulser : public RcuGui::Register 
  {
    TGLayoutHints           fHints;
    TGCompositeFrame        fFrame;
    RcuGui::LabeledIntEntry fLevel;
    RcuGui::LabeledIntEntry fStep;
    Rcuxx::FmdPulser&       fLow;
    Pulser(TGCompositeFrame& f, Rcuxx::FmdPulser& low) 
      : RcuGui::Register(f, low, kHorizontalFrame, 0, 3, 0, 0),
	fLow(low), 
	fHints(kLHintsExpandX|kLHintsExpandY),
	fFrame(&fFields, 0, 0, kVerticalFrame),
	fLevel(fFrame,"Level",0,0xff,kHorizontalFrame),
	fStep(fFrame,"Step size",0,0xff,kHorizontalFrame)
    {
      fFields.AddFrame(&fFrame, &fHints);
      Get();
    }
    void Get()
    {
      fLevel.SetValue(fLow.Value());
      fStep.SetValue(fLow.Test());
    }
    void Set()
    {
      fLow.SetValue(fLevel.GetValue());
      fLow.SetTest(fStep.GetValue());
    }
  };

  //____________________________________________________________________
  struct CalIter : public RcuGui::Register 
  {
    TGLayoutHints           fHints;
    TGCompositeFrame        fFrame;
    RcuGui::LabeledIntEntry fNum;
    Rcuxx::FmdCalIter&      fLow;
    CalIter(TGCompositeFrame& f, Rcuxx::FmdCalIter& low) 
      : RcuGui::Register(f, low, kHorizontalFrame, 0, 3, 0, 0),
	fLow(low), 
	fHints(kLHintsExpandX|kLHintsExpandY),
	fFrame(&fFields, 0, 0, kVerticalFrame),
	fNum(fFrame,"Events/strip/pulse",0,0xffff,kHorizontalFrame)
    {
      fFields.AddFrame(&fFrame, &fHints);
      Get();
    }
    void Get()
    {
      fNum.SetValue(fLow.Value());
    }
    void Set()
    {
      fLow.SetValue(fNum.GetValue());
    }
  };
#if 0      
  struct Pulser : public TopBottom
  {
    Pulser(TGCompositeFrame& f, Rcuxx::FmdPulser& low) 
      : TopBottom(f, low, 0, 3) 
    {}
  };
#endif
  //____________________________________________________________________
  struct Clock : public RcuGui::Register 
  {
    TGLayoutHints           fHints;
    TGCompositeFrame        fFrame;
    RcuGui::LabeledIntEntry fDivision;
    RcuGui::LabeledIntEntry fPhase;
    Rcuxx::FmdClock&        fLow;
    Clock(TGCompositeFrame& f, Rcuxx::FmdClock& low,Int_t opt=kHorizontalFrame)
      : RcuGui::Register(f, low, kHorizontalFrame, 0, 3, 0, 0), 
	fLow(low),
	fHints(kLHintsExpandX|kLHintsExpandY),
	fFrame(&fFields, 0, 0, kVerticalFrame),
	fDivision(fFrame,"Division",0,0xff,opt),
	fPhase(fFrame,"Phase",0,0xff,opt)
    {
      fFields.AddFrame(&fFrame, &fHints);
      Get();
    }
    void Get()
    {
      fPhase.SetValue(fLow.Phase());
      fDivision.SetValue(fLow.Division());
    }
    void Set()
    {
      fLow.SetPhase(fPhase.GetValue());
      fLow.SetDivision(fDivision.GetValue());
    }
  };
  struct ShiftClock : public Clock
  {
    ShiftClock(TGCompositeFrame& f, Rcuxx::FmdShiftClock& low) : Clock(f,low){}
  };
  struct SampleClock : public Clock
  {
    SampleClock(TGCompositeFrame& f, Rcuxx::FmdSampleClock& low):Clock(f,low){}
  };

  //____________________________________________________________________
  struct HoldWait : public RcuGui::Register 
  {
    RcuGui::LabeledIntEntry  fView;
    Rcuxx::FmdHoldWait& fLow;
    HoldWait(TGCompositeFrame& f, Rcuxx::FmdHoldWait& low, 
	     Int_t opt=kHorizontalFrame) 
      : RcuGui::Register(f, low, kHorizontalFrame, 3, 0, 0, 0),
	fLow(low),
	fView(fFields, "Hold Wait", 0, 0xffff, opt)
    {
      Get();
    }
    void Get() { fView.SetValue(fLow.Clocks()); }
    void Set() { fLow.SetClocks(fView.GetValue()); }
    void Print() const { fLow.Print(); }
  };

  //____________________________________________________________________
  struct L0Timeout : public RcuGui::Register 
  {
    RcuGui::LabeledIntEntry  fView;
    Rcuxx::FmdL0Timeout& fLow;
    L0Timeout(TGCompositeFrame& f, Rcuxx::FmdL0Timeout& low, 
	     Int_t opt=kHorizontalFrame) 
      : RcuGui::Register(f, low, kHorizontalFrame, 3, 0, 0, 0),
	fLow(low),
	fView(fFields, "L0 Timeout", 0, 0xffff, opt)
    {
      Get();
    }

    void Get() { fView.SetValue(fLow.Clocks()); }
    void Set() { fLow.SetClocks(fView.GetValue()); }
    void Print() const { fLow.Print(); }
  };

  //____________________________________________________________________
  struct L1Timeout : public RcuGui::Register 
  {
    RcuGui::LabeledIntEntry  fView;
    Rcuxx::FmdL1Timeout& fLow;
    L1Timeout(TGCompositeFrame& f, Rcuxx::FmdL1Timeout& low, 
	      Int_t opt=kHorizontalFrame) 
      : RcuGui::Register(f, low, kHorizontalFrame, 3, 0, 0, 0),
	fLow(low),
	fView(fFields, "L1 Timeout", 0, 0xffff, opt)
    {
      Get();
    }

    void Get() { fView.SetValue(fLow.Clocks()); }
    void Set() { fLow.SetClocks(fView.GetValue()); }
    void Print() const { fLow.Print(); }
  };

  //____________________________________________________________________
  struct Range : public RcuGui::Register 
  {
    TGLayoutHints           fHints;
    TGCompositeFrame        fFrame;
    RcuGui::LabeledIntEntry fMin;
    RcuGui::LabeledIntEntry fMax;
    Rcuxx::FmdRange& fLow;
    Range(TGCompositeFrame& f, Rcuxx::FmdRange& low)
      : RcuGui::Register(f, low, kHorizontalFrame, 0, 3, 0, 0), 
	fLow(low),
	fHints(kLHintsExpandX|kLHintsExpandY),
	fFrame(&fFields, 0, 0, kVerticalFrame),
	fMin(fFrame,"First",0,0xff,kHorizontalFrame),
	fMax(fFrame,"Last",0,0xff,kHorizontalFrame)
    {
      fFields.AddFrame(&fFrame, &fHints);
      Get();
    }
    void Get()
    {
      fMin.SetValue(fLow.Min());
      fMax.SetValue(fLow.Max());
    }
    void Set()
    {
      fLow.SetMin(fMin.GetValue());
      fLow.SetMax(fMax.GetValue());
    }
  };

  //____________________________________________________________________
  struct L0CNT : public RcuGui::Register 
  {
    Rcuxx::FmdL0Triggers& fL0;
    RcuGui::LabeledIntView fView;
    L0CNT(TGCompositeFrame& f, Rcuxx::FmdL0Triggers& l0s) 
      : RcuGui::Register(f, l0s),
	fL0(l0s),
	fView(fFields, "L0", 0, 0xffff, kHorizontalFrame)
    {
      Get();
    }
    void Get() { fView.SetValue(fL0.Recieved()); }
    void Print() const { fL0.Print(); }
  };

  //____________________________________________________________________
  struct Meb : public RcuGui::Register 
  {
    RcuGui::LabeledIntView fCount;
    RcuGui::LabeledIntEntry fMax;
    TGCheckButton fEnable;
    Rcuxx::FmdMeb& fMeb;
    Meb(TGCompositeFrame& f, Rcuxx::FmdMeb& meb) 
      : RcuGui::Register(f, meb),
	fMeb(meb),
	fCount(fFields, "Free buffers", 0, 0xf, kHorizontalFrame), 
	fMax(fFields, "# of buffers", 0, 0xf, kHorizontalFrame), 
	fEnable(&fFields, "Enable")
    {
      Get();
    }
    void Get() 
    {
      fCount.SetValue(fMeb.Counter());
      fMax.SetValue(fMeb.Maximum());
      fEnable.SetState(fMeb.IsEnabled() ? kButtonDown : kButtonUp);
    }
    void Set()
    {
      fMeb.SetMaximum(fMax.GetValue() & 0xf);
      fMeb.SetEnabled(fEnable.IsDown());
    }
  };

  //____________________________________________________________________
  struct Status : public RcuGui::Register 
  {
    RcuGui::ErrorBit  fCalOn; 
    RcuGui::ErrorBit  fTriggerBusy;
    RcuGui::ErrorBit  fDacBusy;
    RcuGui::ErrorBit  fReadoutBusy;
    RcuGui::ErrorBit  fBoxBusy;
    RcuGui::ErrorBit  fIncompleteRo;
    RcuGui::ErrorBit  fTriggerOverlap;
    RcuGui::ErrorBit  fTriggerTimeout; 
    RcuGui::ErrorBit  fTestOn;
    RcuGui::ErrorBit  fCalManOn;
    Rcuxx::FmdStatus& fLow;
    Status(TGCompositeFrame& f, Rcuxx::FmdStatus& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fCalOn(fFields, "Pulser on", "Input to VA1's is calibration pulser", 
	       0xc0c0c0, 0x00aa00),
	fTriggerBusy(fFields, "Trigger busy", "Trigger handling busy",
		     0xc0c0c0, 0xaa0000),
	fDacBusy(fFields, "Dac busy", "Dac handling busy",
		 0xc0c0c0, 0xaa0000),
	fReadoutBusy(fFields, "Readout busy", "Readout handling busy",
		     0xc0c0c0, 0xaa0000),
	fBoxBusy(fFields, "Trigger box busy", "Trigger box busy",
		     0xc0c0c0, 0xaa0000),
	fIncompleteRo(fFields, "Incomplete", "Incomplete Readout",
		      0xc0c0c0, 0xaa0000),
	fTriggerOverlap(fFields, "Trigger overlap", "Untimely triggers seen",
			0xc0c0c0, 0xaa0000),
	fTriggerTimeout(fFields, "Trigger timed out", "Missing trigger",
			0xc0c0c0, 0xaa0000),
	fTestOn(fFields, "Test on", "VA1s in test mode", 0xc0c0c0, 0x00aa00),
	fCalManOn(fFields, "CalMan", "Calibration manager on", 
		  0xc0c0c0, 0x00aa00)
    {
      Get();
    }
    virtual ~Status() {}
    void Get()
    {
      fCalOn.SetState(!fLow.IsCalOn()); 
      fTriggerBusy.SetState(!fLow.IsTriggerBusy());
      fDacBusy.SetState(!fLow.IsDacBusy());
      fReadoutBusy.SetState(!fLow.IsReadoutBusy());
      fBoxBusy.SetState(!fLow.IsBoxBusy());
      fIncompleteRo.SetState(!fLow.IsIncompleteRo());
      fTriggerOverlap.SetState(!fLow.IsTriggerOverlap());
      fTriggerTimeout.SetState(!fLow.IsTriggerTimeout()); 
      fTestOn.SetState(!fLow.IsTestOn());
      fCalManOn.SetState(!fLow.IsCalManOn());
    }
  };


  //___________________________________________________________________
  struct T1 : public RcuGui::Monitored
  {
    T1(TGCompositeFrame& f, Rcuxx::FmdT1& c, Rcuxx::FmdT1_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct FLASH_I : public RcuGui::Monitored
  {
    FLASH_I(TGCompositeFrame& f, Rcuxx::FmdFLASH_I& c,
	    Rcuxx::FmdFLASH_I_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct AL_DIG_I : public RcuGui::Monitored
  {
    AL_DIG_I(TGCompositeFrame& f, Rcuxx::FmdAL_DIG_I& c,
	     Rcuxx::FmdAL_DIG_I_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct AL_ANA_I : public RcuGui::Monitored
  {
    AL_ANA_I(TGCompositeFrame& f, Rcuxx::FmdAL_ANA_I& c,
	     Rcuxx::FmdAL_ANA_I_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct VA_REC_IP : public RcuGui::Monitored
  {
    VA_REC_IP(TGCompositeFrame& f, Rcuxx::FmdVA_REC_IP& c,
	      Rcuxx::FmdVA_REC_IP_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct T2 : public RcuGui::Monitored
  {
    T2(TGCompositeFrame& f, Rcuxx::FmdT2& c, Rcuxx::FmdT2_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct VA_SUP_IP : public RcuGui::Monitored
  {
    VA_SUP_IP(TGCompositeFrame& f, Rcuxx::FmdVA_SUP_IP& c,
	      Rcuxx::FmdVA_SUP_IP_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct VA_REC_IM : public RcuGui::Monitored
  {
    VA_REC_IM(TGCompositeFrame& f, Rcuxx::FmdVA_REC_IM& c,
	      Rcuxx::FmdVA_REC_IM_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct VA_SUP_IM : public RcuGui::Monitored
  {
    VA_SUP_IM(TGCompositeFrame& f, Rcuxx::FmdVA_SUP_IM& c, 
	      Rcuxx::FmdVA_SUP_IM_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct GTL_U : public RcuGui::Monitored
  {
    GTL_U(TGCompositeFrame& f, Rcuxx::FmdGTL_U& c, Rcuxx::FmdGTL_U_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, true) {}
  };
  //___________________________________________________________________
  struct T3 : public RcuGui::Monitored
  {
    T3(TGCompositeFrame& f, Rcuxx::FmdT3& c, Rcuxx::FmdT3_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct T1SENS : public RcuGui::Monitored
  {
    T1SENS(TGCompositeFrame& f, Rcuxx::FmdT1SENS& c, Rcuxx::FmdT1SENS_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct T2SENS : public RcuGui::Monitored
  {
    T2SENS(TGCompositeFrame& f, Rcuxx::FmdT2SENS& c, Rcuxx::FmdT2SENS_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct AL_DIG_U : public RcuGui::Monitored
  {
    AL_DIG_U(TGCompositeFrame& f, Rcuxx::FmdAL_DIG_U& c,
	     Rcuxx::FmdAL_DIG_U_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, true) {}
  };
  //___________________________________________________________________
  struct AL_ANA_U : public RcuGui::Monitored
  {
    AL_ANA_U(TGCompositeFrame& f, Rcuxx::FmdAL_ANA_U& c,
	     Rcuxx::FmdAL_ANA_U_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, true) {}
  };
  //___________________________________________________________________
  struct T4 : public RcuGui::Monitored
  {
    T4(TGCompositeFrame& f, Rcuxx::FmdT4& c, Rcuxx::FmdT4_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame) {}
  };
  //___________________________________________________________________
  struct VA_REC_UP : public RcuGui::Monitored
  {
    VA_REC_UP(TGCompositeFrame& f, Rcuxx::FmdVA_REC_UP& c, 
	      Rcuxx::FmdVA_REC_UP_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, true) {}
  };
  //___________________________________________________________________
  struct VA_SUP_UP : public RcuGui::Monitored
  {
    VA_SUP_UP(TGCompositeFrame& f, Rcuxx::FmdVA_SUP_UP& c, 
	      Rcuxx::FmdVA_SUP_UP_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, true) {}
  };
  //___________________________________________________________________
  struct VA_SUP_UM : public RcuGui::Monitored
  {
    VA_SUP_UM(TGCompositeFrame& f, Rcuxx::FmdVA_SUP_UM& c, 
	      Rcuxx::FmdVA_SUP_UM_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, false) {}
  };
  //___________________________________________________________________
  struct VA_REC_UM : public RcuGui::Monitored
  {
    VA_REC_UM(TGCompositeFrame& f, Rcuxx::FmdVA_REC_UM& c, 
	      Rcuxx::FmdVA_REC_UM_TH& t)
      : RcuGui::Monitored(f, c, t, kVerticalFrame, false) {}
  };

  //__________________________________________________________________
  struct BiasCommands : public RcuGui::Register
  {
    BiasCommands(TGCompositeFrame&    p, 
		 Rcuxx::AltroCommand& changeDacs,
		 Rcuxx::AltroCommand& pulserOn, 
		 Rcuxx::AltroCommand& pulserOff, 
		 Rcuxx::AltroCommand& testOn, 
		 Rcuxx::AltroCommand& testOff,
		 Rcuxx::AltroCommand& calRun,
		 Rcuxx::FmdStatus&    status)
      : Register(p, status),
	fChangeDacs(fFields, changeDacs, "Change DACs", 4,4,16), 
	fPulserOn(fFields, pulserOn, "Turn on pulser", 4,4,16), 
	fPulserOff(fFields, pulserOff, "Turn off pulser", 4,4,16), 
	fTestOn(fFields, testOn, "Turn on test mode", 4,4,16), 
	fTestOff(fFields, testOff, "Turn off test mode", 4,4,16), 
	fCalRun(fFields, calRun, "Turn on Calibration Run", 4,4,16), 
	fPulserStatus(fFields, "Pulser", "Pulser is on", 0x00aa00, 0xcfcfcf),
	fTestStatus(fFields, "Test", "Test is on", 0x00add00, 0xcfcfcf),
	fCalStatus(fFields, "Cal.", "Calibration manager is on", 
		   0x00add00, 0xcfcfcf),
	fStatus(status)
    {
      fGroup.SetTitle("Commands and status");
      fChangeDacs.Connect("RcuGui::Register", this, "Update()");
      fPulserOn.Connect("RcuGui::Register", this, "Update()");
      fPulserOff.Connect("RcuGui::Register", this, "Update()");
      fTestOn.Connect("RcuGui::Register", this, "Update()");
      fTestOff.Connect("RcuGui::Register", this, "Update()");
      fCalRun.Connect("RcuGui::Register", this, "Update()");
      Get();
    }
    void Get()
    {
      fTestStatus.SetState(fStatus.IsTestOn());
      fPulserStatus.SetState(fStatus.IsCalOn());
      fCalStatus.SetState(fStatus.IsCalManOn());
    }
    /** Interface to the command ChangeDacs */
    RcuGui::Command fChangeDacs; 
    /** Interface to the command PulserOn */
    RcuGui::Command fPulserOn; 
    /** Interface to the command PulserOff */
    RcuGui::Command fPulserOff; 
    /** Interface to the command TestOn */
    RcuGui::Command fTestOn; 
    /** Interface to the command TestOff */
    RcuGui::Command fTestOff; 
    /** Interface to the command TestOff */
    RcuGui::Command fCalRun; 
    /** status indicator for pulser mode */
    RcuGui::ErrorBit fPulserStatus;
    /** status indicator for test mode */
    RcuGui::ErrorBit fTestStatus;
    /** status indicator for test mode */
    RcuGui::ErrorBit fCalStatus;
    /** Interface to status */
    Rcuxx::FmdStatus& fStatus;
  };
}


//____________________________________________________________________
Fmdxx::Main::Main() 
  : RcuGui::Main()
{
  fgInstance = this;
}  

//____________________________________________________________________
void 
Fmdxx::Main::AddRcu(Rcuxx::Rcu& rcu, Int_t maxFEC, const UInt_t* mask) 
{
  maxFEC   = maxFEC & 0x30003;
  const UInt_t dmask[] = { 0xffff, 0xff, 0xffff, 0, 0, 0, 0, 0 };
  if (!mask) mask = dmask;
  RcuGui::Main::AddRcu(rcu, maxFEC, mask);
  if (!fRcu) return;
  fRcu->ACTFEC()->Get();
}

//____________________________________________________________________
void 
Fmdxx::Main::AddFmd(Rcuxx::Fmd& fmd) 
{
  if (fBc) {
    std::cerr << "Already have an FMD tab" << std::endl;
    return;
  }
  fBc  = new Fmd(fTab, fmd);
  fOptionMenu.AddEntry("Debug &FMDD", Rcuxx::Rcu::kBc);
}

//____________________________________________________________________
Fmdxx::Fmd::Fmd(TGTab& tabs, Rcuxx::Fmd& fmd) 
  : RcuGui::Bc(tabs, fmd), 
    fTriggers(fTab.AddTab("Triggers")),
    fTrgCommands(fTriggers, "Commands", kHorizontalFrame),
    fFakeTrigger(0),
    fSoftReset(0),
    fTrgLow(fTriggers),
    fTrgLeft(&fTrgLow),
    fTrgRight(&fTrgLow),
    fHoldWait(0),
    fL0Timeout(0),
    fL1Timeout(0),
    fRange(0),
    fShiftClock(0),
    fSampleClock(0),
    fStatus(0),
    fMeb(0),
    fBiases(fTab.AddTab("Biases")),
    fBiasCommands(0),
    // fBiasCommands(fBiases, "Commands", kHorizontalFrame),
    fBiasLow(fBiases),
    fBiasLeft(&fBiasLow),
    fBiasRight(&fBiasLow),
    // fChangeDacs(0),
    // fPulserOn(0),
    // fPulserOff(0),
    fShapeBias(0),
    fVFS(0),
    fVFP(0),
    fPulser(0),
    fCalIter(0),
    fL0CNT(0),
    fT1(0),
    fFLASH_I(0),
    fAL_DIG_I(0),
    fAL_ANA_I(0),
    fVA_REC_IP(0),
    fT2(0),
    fVA_SUP_IP(0),
    fVA_REC_IM(0),
    fVA_SUP_IM(0),
    fGTL_U(0),
    fT3(0),
    fT1SENS(0),
    fT2SENS(0),
    fAL_DIG_U(0),
    fAL_ANA_U(0),
    fT4(0),
    fVA_REC_UP(0),
    fVA_SUP_UP(0),
    fVA_SUP_UM(0),
    fVA_REC_UM(0)
{
  // Config/status
  if (fmd.Meb()) fMeb = new Meb(*fConfigStatus, *fmd.Meb());
  
  // if (fSCLKCNT) fSCLKCNT->GetGroup()->SetTitle("RPINC counter");
    
  // Triggers 
  fTriggers->AddFrame(&fTrgCommands, &fHints);
  if (fmd.FakeTrigger()) 
    fFakeTrigger = new RcuGui::Command(fTrgCommands, *fmd.FakeTrigger());
  if (fmd.SoftReset()) 
    fSoftReset = new RcuGui::Command(fTrgCommands, *fmd.SoftReset());
  if (fmd.Status())     fStatus       = new Status(*fTriggers,*fmd.Status());
  fTriggers->AddFrame(&fTrgLow, &fHints);
  fTrgLow.AddFrame(&fTrgLeft, &fLowerHints);
  fTrgLow.AddFrame(&fTrgRight, &fLowerHints);
  if (fmd.Range())      fRange        = new Range(fTrgLeft, *fmd.Range());
  if (fmd.ShiftClock()) fShiftClock   = new ShiftClock(fTrgLeft, 
							*fmd.ShiftClock());
  if (fmd.SampleClock()) fSampleClock = new SampleClock(fTrgLeft, 
							 *fmd.SampleClock());
  if (fmd.HoldWait())   fHoldWait     = new HoldWait(fTrgRight, 
						      *fmd.HoldWait());
  if (fmd.L0Timeout())  fL0Timeout    = new L0Timeout(fTrgRight, 
						       *fmd.L0Timeout());
  if (fmd.L1Timeout())  fL1Timeout    = new L1Timeout(fTrgRight, 
						       *fmd.L1Timeout());

  // Counters
  if (fmd.L0Triggers())  fL0CNT = new L0CNT(*fCounters,*fmd.L0Triggers());

  // Biases
#if 0
  fBiases->AddFrame(&fBiasCommands, &fHints);
  if (fmd.ChangeDacs()) fChangeDacs = new RcuGui::Command(fBiasCommands, 
						   *fmd.ChangeDacs());
  if (fmd.PulserOn())   fPulserOn   = new RcuGui::Command(fBiasCommands, 
						   *fmd.PulserOn());
  if (fmd.PulserOff())  fPulserOff  = new RcuGui::Command(fBiasCommands, 
						   *fmd.PulserOff());
#endif
  if (fmd.ChangeDacs() && fmd.PulserOn() && fmd.PulserOff() && 
      fmd.TestOn() && fmd.TestOff() && fmd.Status())
    fBiasCommands = new BiasCommands(*fBiases, 
				     *fmd.ChangeDacs(),
				     *fmd.PulserOn(),
				     *fmd.PulserOff(),
				     *fmd.TestOn(),
				     *fmd.TestOff(),
				     *fmd.CalibrationRun(),
				     *fmd.Status());
  fBiases->AddFrame(&fBiasLow, &fHints);
  fBiasLow.AddFrame(&fBiasLeft, &fLowerHints);
  fBiasLow.AddFrame(&fBiasRight, &fLowerHints);
  if (fmd.ShapeBias())  fShapeBias  = new ShapeBias(fBiasLeft,
						     *fmd.ShapeBias());
  if (fmd.VFP())	 fVFP        = new VFP(fBiasRight, *fmd.VFP());
  if (fmd.VFS())	 fVFS        = new VFS(fBiasRight, *fmd.VFS());
  if (fmd.Pulser())	 fPulser     = new Pulser(fBiasLeft, *fmd.Pulser());
  if (fmd.CalIter())	 fCalIter    = new CalIter(fBiasLeft, *fmd.CalIter());

  // Monitors
  if (fmd.AL_DIG_I()) fAL_DIG_I = new AL_DIG_I(fMonLeft,*fmd.AL_DIG_I(),
					       *fmd.AL_DIG_I_TH());
  if (fmd.AL_ANA_I()) fAL_ANA_I = new AL_ANA_I(fMonLeft,*fmd.AL_ANA_I(),
					       *fmd.AL_ANA_I_TH());
  if (fmd.AL_DIG_U()) fAL_DIG_U = new AL_DIG_U(fMonLeft,*fmd.AL_DIG_U(),
					       *fmd.AL_DIG_U_TH());
  if (fmd.AL_ANA_U()) fAL_ANA_U = new AL_ANA_U(fMonLeft,*fmd.AL_ANA_U(),
					       *fmd.AL_ANA_U_TH());
  if (fmd.T1SENS())   fT1SENS   = new T1SENS(fMonLeft,*fmd.T1SENS(),
					     *fmd.T1SENS_TH());
  if (fmd.T2SENS())   fT2SENS   = new T2SENS(fMonLeft,*fmd.T2SENS(),
					     *fmd.T2SENS_TH());
  if (fmd.T1())	      fT1       = new T1(fMonLeft,*fmd.T1(),*fmd.T1_TH());
  if (fmd.T2())	      fT2       = new T2(fMonLeft,*fmd.T2(),*fmd.T2_TH());
  if (fmd.T3())	      fT3       = new T3(fMonLeft,*fmd.T3(),*fmd.T3_TH());
  if (fmd.T4())	      fT4       = new T4(fMonLeft,*fmd.T4(),*fmd.T4_TH());


  // Right hand side 
  if (fmd.VA_REC_IP())	fVA_REC_IP = new VA_REC_IP(fMonRight,*fmd.VA_REC_IP(),
						   *fmd.VA_REC_IP_TH());
  if (fmd.VA_REC_UP())	fVA_REC_UP = new VA_REC_UP(fMonRight,*fmd.VA_REC_UP(),
						   *fmd.VA_REC_UP_TH());

  if (fmd.VA_REC_IM())	fVA_REC_IM = new VA_REC_IM(fMonRight,*fmd.VA_REC_IM(),
						   *fmd.VA_REC_IM_TH());
  if (fmd.VA_REC_UM())	fVA_REC_UM = new VA_REC_UM(fMonRight,*fmd.VA_REC_UM(),
						   *fmd.VA_REC_UM_TH());

  if (fmd.VA_SUP_IP())	fVA_SUP_IP = new VA_SUP_IP(fMonRight,*fmd.VA_SUP_IP(),
						   *fmd.VA_SUP_IP_TH());
  if (fmd.VA_SUP_UP())	fVA_SUP_UP = new VA_SUP_UP(fMonRight,*fmd.VA_SUP_UP(),
						   *fmd.VA_SUP_UP_TH());

  if (fmd.VA_SUP_IM())	fVA_SUP_IM = new VA_SUP_IM(fMonRight,*fmd.VA_SUP_IM(),
						   *fmd.VA_SUP_IM_TH());
  if (fmd.VA_SUP_UM())	fVA_SUP_UM = new VA_SUP_UM(fMonRight,*fmd.VA_SUP_UM(),
						   *fmd.VA_SUP_UM_TH());

  if (fmd.GTL_U())	fGTL_U     = new GTL_U(fMonRight,*fmd.GTL_U(),
					       *fmd.GTL_U_TH());
  if (fmd.FLASH_I())	fFLASH_I   = new FLASH_I(fMonRight,*fmd.FLASH_I(),
						 *fmd.FLASH_I_TH());

  // Set Initial address 
  HandleAddress();
}

//____________________________________________________________________
Fmdxx::Fmd::~Fmd()
{
#if 0
  if (fShapeBias)	delete fShapeBias;
  if (fVFS)		delete fVFS;
  if (fVFP)		delete fVFP;
  if (fPulser)		delete fPulser;
  if (fCalIter)		delete fCalIter;

  if (fShiftClock)	delete fShiftClock;
  if (fSampleClock)	delete fSampleClock;

  if (fHoldWait)	delete fHoldWait;
  if (fL0Timeout)	delete fL0Timeout;
  if (fL1Timeout)	delete fL1Timeout;
  if (fRange)		delete fRange;
  if (fTriggers)	delete fTriggers;
  if (fStatus)		delete fStatus;
  if (fBiasCommands)    delete fBiasCommands;
  
  // Monitor
  if (fT1)		delete fT1;
  if (fFLASH_I)		delete fFLASH_I;
  if (fAL_DIG_I)	delete fAL_DIG_I;
  if (fAL_ANA_I)	delete fAL_ANA_I;
  if (fVA_REC_IP)	delete fVA_REC_IP;
  if (fT2)		delete fT2;
  if (fVA_SUP_IP)	delete fVA_SUP_IP;
  if (fVA_REC_IM)	delete fVA_REC_IM;
  if (fVA_SUP_IM)	delete fVA_SUP_IM;
  if (fGTL_U)		delete fGTL_U;
  if (fT3)		delete fT3;
  if (fT1SENS)		delete fT1SENS;
  if (fT2SENS)		delete fT2SENS;
  if (fAL_DIG_U)	delete fAL_DIG_U;
  if (fAL_ANA_U)	delete fAL_ANA_U;
  if (fT4)		delete fT4;
  if (fVA_REC_UP)	delete fVA_REC_UP;
  if (fVA_SUP_UP)	delete fVA_SUP_UP;
  if (fVA_SUP_UM)	delete fVA_SUP_UM;
  if (fVA_REC_UM)	delete fVA_REC_UM;
#endif
}

//____________________________________________________________________
void 
Fmdxx::Fmd::HandleBroadcast() 
{
  RcuGui::Bc::HandleBroadcast();
  Bool_t what = !(fBroadcast.IsDown());
  if (what) 
    HandleAddress();
  else {
    if (fShapeBias)	fShapeBias->HandleBroadcast();
    if (fVFS)		fVFS->HandleBroadcast();
    if (fVFP)		fVFP->HandleBroadcast();
    if (fPulser)	fPulser->HandleBroadcast();
    if (fCalIter)	fCalIter->HandleBroadcast();

    if (fShiftClock)	fShiftClock->HandleBroadcast();
    if (fSampleClock)	fSampleClock->HandleBroadcast();

    if (fHoldWait)	fHoldWait->HandleBroadcast();
    if (fL0Timeout)	fL0Timeout->HandleBroadcast();
    if (fL1Timeout)	fL1Timeout->HandleBroadcast();
    if (fRange)		fRange->HandleBroadcast();

    if (fStatus)	fStatus->HandleBroadcast();
    if (fBiasCommands)  fBiasCommands->HandleBroadcast();
    
    // Monitor
    if (fT1)		fT1->HandleBroadcast();
    if (fFLASH_I)	fFLASH_I->HandleBroadcast();
    if (fAL_DIG_I)	fAL_DIG_I->HandleBroadcast();
    if (fAL_ANA_I)	fAL_ANA_I->HandleBroadcast();
    if (fVA_REC_IP)	fVA_REC_IP->HandleBroadcast();
    if (fT2)		fT2->HandleBroadcast();
    if (fVA_SUP_IP)	fVA_SUP_IP->HandleBroadcast();
    if (fVA_REC_IM)	fVA_REC_IM->HandleBroadcast();
    if (fVA_SUP_IM)	fVA_SUP_IM->HandleBroadcast();
    if (fGTL_U)		fGTL_U->HandleBroadcast();
    if (fT3)		fT3->HandleBroadcast();
    if (fT1SENS)	fT1SENS->HandleBroadcast();
    if (fT2SENS)	fT2SENS->HandleBroadcast();
    if (fAL_DIG_U)	fAL_DIG_U->HandleBroadcast();
    if (fAL_ANA_U)	fAL_ANA_U->HandleBroadcast();
    if (fT4)		fT4->HandleBroadcast();
    if (fVA_REC_UP)	fVA_REC_UP->HandleBroadcast();
    if (fVA_SUP_UP)	fVA_SUP_UP->HandleBroadcast();
    if (fVA_SUP_UM)	fVA_SUP_UM->HandleBroadcast();
    if (fVA_REC_UM)	fVA_REC_UM->HandleBroadcast();
  }
}

//____________________________________________________________________
void
Fmdxx::Fmd::HandleUpdate()
{
  // Monitor
  unsigned int ret = 0;
  if (fT1		&& !fT1->Update())        return;
  if (fFLASH_I		&& !fFLASH_I->Update())   return;
  if (fAL_DIG_I		&& !fAL_DIG_I->Update())  return;
  if (fAL_ANA_I		&& !fAL_ANA_I->Update())  return;
  if (fVA_REC_IP	&& !fVA_REC_IP->Update()) return;
  if (fT2		&& !fT2->Update())        return;
  if (fVA_SUP_IP	&& !fVA_SUP_IP->Update()) return;
  if (fVA_REC_IM	&& !fVA_REC_IM->Update()) return;
  if (fVA_SUP_IM	&& !fVA_SUP_IM->Update()) return;
  if (fGTL_U		&& !fGTL_U->Update())     return;
  if (fT3		&& !fT3->Update())        return;
  if (fT1SENS		&& !fT1SENS->Update())    return;
  if (fT2SENS		&& !fT2SENS->Update())    return;
  if (fAL_DIG_U		&& !fAL_DIG_U->Update())  return;
  if (fAL_ANA_U		&& !fAL_ANA_U->Update())  return;
  if (fT4		&& !fT4->Update())        return;
  if (fVA_REC_UP	&& !fVA_REC_UP->Update()) return;
  if (fVA_SUP_UP	&& !fVA_SUP_UP->Update()) return;
  if (fVA_SUP_UM	&& !fVA_SUP_UM->Update()) return;
  if (fVA_REC_UM	&& !fVA_REC_UM->Update()) return;
}  
//____________________________________________________________________
void
Fmdxx::Fmd::HandleAddress() 
{
  RcuGui::Bc::HandleAddress();
  UInt_t board   = fBoardAddress.GetValue();
  // fFMD.SetAddress(board);
  if (fShapeBias)	fShapeBias->HandleAddress(board,0,0);
  if (fVFS)		fVFS->HandleAddress(board,0,0);
  if (fVFP)		fVFP->HandleAddress(board,0,0);
  if (fPulser)		fPulser->HandleAddress(board,0,0);
  if (fCalIter)		fCalIter->HandleAddress(board,0,0);

  if (fShiftClock)	fShiftClock->HandleAddress(board,0,0);
  if (fSampleClock)	fSampleClock->HandleAddress(board,0,0);

  if (fHoldWait)	fHoldWait->HandleAddress(board,0,0);
  if (fL0Timeout)	fL0Timeout->HandleAddress(board,0,0);
  if (fL1Timeout)	fL1Timeout->HandleAddress(board,0,0);
  if (fRange)		fRange->HandleAddress(board,0,0);
  if (fStatus)		fStatus->HandleAddress(board,0,0);
    if (fBiasCommands)  fBiasCommands->HandleAddress(board,0,0);

  // Monitor
  if (fT1)		fT1->HandleAddress(board,0,0);
  if (fFLASH_I)		fFLASH_I->HandleAddress(board,0,0);
  if (fAL_DIG_I)	fAL_DIG_I->HandleAddress(board,0,0);
  if (fAL_ANA_I)	fAL_ANA_I->HandleAddress(board,0,0);
  if (fVA_REC_IP)	fVA_REC_IP->HandleAddress(board,0,0);
  if (fT2)		fT2->HandleAddress(board,0,0);
  if (fVA_SUP_IP)	fVA_SUP_IP->HandleAddress(board,0,0);
  if (fVA_REC_IM)	fVA_REC_IM->HandleAddress(board,0,0);
  if (fVA_SUP_IM)	fVA_SUP_IM->HandleAddress(board,0,0);
  if (fGTL_U)		fGTL_U->HandleAddress(board,0,0);
  if (fT3)		fT3->HandleAddress(board,0,0);
  if (fT1SENS)		fT1SENS->HandleAddress(board,0,0);
  if (fT2SENS)		fT2SENS->HandleAddress(board,0,0);
  if (fAL_DIG_U)	fAL_DIG_U->HandleAddress(board,0,0);
  if (fAL_ANA_U)	fAL_ANA_U->HandleAddress(board,0,0);
  if (fT4)		fT4->HandleAddress(board,0,0);
  if (fVA_REC_UP)	fVA_REC_UP->HandleAddress(board,0,0);
  if (fVA_SUP_UP)	fVA_SUP_UP->HandleAddress(board,0,0);
  if (fVA_SUP_UM)	fVA_SUP_UM->HandleAddress(board,0,0);
  if (fVA_REC_UM)	fVA_REC_UM->HandleAddress(board,0,0);
}

//____________________________________________________________________
//
// EOF
//7
