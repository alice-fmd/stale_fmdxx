// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    FmdGui.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:27:36 2006
    @brief   
    @ingroup fmdxx_gui            
*/
#ifndef FMDXX_GUI_MAIN_H
#define FMDXX_GUI_MAIN_H
#ifndef RCUGUI_MAIN_H
# include <rcugui/Main.h>
#endif
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif
#ifndef RCUGUI_BC_H
# include <rcugui/Bc.h>
#endif
#ifndef RCUGUI_REGSITER_H
# include <rcugui/Register.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 

namespace Rcuxx
{
  class Fmd;
}

namespace RcuGui 
{
  class LabeledIntEntry;
  class Register;
  class command;
}

namespace Fmdxx
{
  class Fmd;
    
  //__________________________________________________________________
  /** @class Main 
      @brief Main interface class 
      @ingroup fmdxx_gui
  */
  class Main : public RcuGui::Main
  {
  public:
    /** Constructor */
    Main();
    /** Destructor */
    virtual ~Main() { }
    /** Add an FMD tab 
	@param fmd Pointer to low-level FMD object */
    virtual void AddFmd(Rcuxx::Fmd& fmd);
    /** Add an RCU tab 
	@param rcu Pointer to low-level Rcu object 
	@param maxFEC Bit mask of possible FEC's
	@param maxALTRO Maximum number of ALTRO's per FEC. */
    virtual void AddRcu(Rcuxx::Rcu& rcu, Int_t maxFEC=0xFFFFFFFF, 
			const UInt_t* mask=0);
  protected:
    /** Add BC interface - hidden. @see AddFmd
	@param bc Interface */
    virtual void AddBc(Rcuxx::Bc& bc) {}
  };

  /** @struct Fmd
      @brief A Gui tab for the FMD
      @ingroup fmdxx_gui        
  */
  class Fmd : public RcuGui::Bc
  {
  public:
    /** Constructor 
	@param tab Tab to add this to
	@param low Pointer to low-level interface  */
    Fmd(TGTab& tab, Rcuxx::Fmd& low);
    virtual ~Fmd();
    /** Handle setting broadcast mode */
    void HandleBroadcast();
    /** Handle setting the address. */
    void HandleAddress();
    /** Handle update button */
    void HandleUpdate();
  private:
    /** Pointer to low-level interface */
    // Rcuxx::Fmd& fFMD;

    /** Interface to the Trigger counters register */
    RcuGui::Register* fL0CNT;

    /** Trigger settings tab */
    TGCompositeFrame* fTriggers;
    /** Command frame */
    TGGroupFrame fTrgCommands;
    /** Interface to the command FakeTrigger */
    RcuGui::Command* fFakeTrigger; 
    /** Interface to the command SoftReset */
    RcuGui::Command* fSoftReset;
    /** Low */
    TGHorizontalFrame fTrgLow;
    /** Left */
    TGVerticalFrame fTrgLeft;
    /** Right */
    TGVerticalFrame fTrgRight;
    /** Interface to the Shift Clock register */
    RcuGui::Register* fShiftClock;
    /** Interface to the Sample Clock register */
    RcuGui::Register* fSampleClock;
    /** Interface to the Timing register */
    RcuGui::Register* fHoldWait;
    /** Interface to L0 timeout */
    RcuGui::Register* fL0Timeout;
    /** Interface to L1 timeout */
    RcuGui::Register* fL1Timeout;
    /** Interface to the Range register */
    RcuGui::Register* fRange;
    /** Interface to the Status register */
    RcuGui::Register* fStatus;
    /** Interface to the Status register */
    RcuGui::Register* fMeb;

    /** Bias settings tab */
    TGCompositeFrame* fBiases;
    /** Command frame */
    RcuGui::Register* fBiasCommands;
#if 0    
    TGGroupFrame fBiasCommands;
    /** Interface to the command ChangeDacs */
    RcuGui::Command* fChangeDacs; 
    /** Interface to the command PulserOn */
    RcuGui::Command* fPulserOn; 
    /** Interface to the command PulserOff */
    RcuGui::Command* fPulserOff; 
    /** Interface to the command TestOn */
    RcuGui::Command* fTestOn; 
    /** Interface to the command TestOff */
    RcuGui::Command* fTestOff; 
    /** status indicator for pulser mode */
    RcuGui::ErrorBit* fPulserStatus;
    /** status indicator for test mode */
    RcuGui::ErrorBit* fTestStatus;
#endif
    
    /** Low */
    TGHorizontalFrame fBiasLow;
    /** Left */
    TGVerticalFrame fBiasLeft;
    /** Right */
    TGVerticalFrame fBiasRight;
    /** Interface to the Shaping bias register */
    RcuGui::Register* fShapeBias;
    /** Interface to the VFS register */
    RcuGui::Register* fVFS;
    /** Interface to the VFP register */
    RcuGui::Register* fVFP;
    /** Interface to the Pulser register */
    RcuGui::Register* fPulser;
    /** Interface to the calibration iteration register */
    RcuGui::Register* fCalIter;
    
    // Monitor
    /** Pointer to T1 interface */
    RcuGui::Register*	fT1;
    /** Pointer to FLASH_I interface */
    RcuGui::Register*	fFLASH_I;
    /** Pointer to AL_DIG_I interface */
    RcuGui::Register*	fAL_DIG_I;
    /** Pointer to AL_ANA_I interface */
    RcuGui::Register*	fAL_ANA_I;
    /** Pointer to VA_REC_IP interface */
    RcuGui::Register*	fVA_REC_IP;
    /** Pointer to T2 interface */
    RcuGui::Register*	fT2;
    /** Pointer to VA_SUP_IP interface */
    RcuGui::Register*	fVA_SUP_IP;
    /** Pointer to VA_REC_IM interface */
    RcuGui::Register*	fVA_REC_IM;
    /** Pointer to VA_SUP_IM interface */
    RcuGui::Register*	fVA_SUP_IM;
    /** Pointer to GTL_U interface */
    RcuGui::Register*	fGTL_U;
    /** Pointer to T3 interface */
    RcuGui::Register*	fT3;
    /** Pointer to T1SENS interface */
    RcuGui::Register*	fT1SENS;
    /** Pointer to T2SENS interface */
    RcuGui::Register*	fT2SENS;
    /** Pointer to AL_DIG_U interface */
    RcuGui::Register*	fAL_DIG_U;
    /** Pointer to AL_ANA_U interface */
    RcuGui::Register*	fAL_ANA_U;
    /** Pointer to T4 interface */
    RcuGui::Register*	fT4;
    /** Pointer to VA_REC_UP interface */
    RcuGui::Register*	fVA_REC_UP;
    /** Pointer to VA_SUP_UP interface */
    RcuGui::Register*	fVA_SUP_UP;
    /** Pointer to VA_SUP_UM interface */
    RcuGui::Register*	fVA_SUP_UM;
    /** Pointer to VA_REC_UM interface */
    RcuGui::Register*	fVA_REC_UM;

    /** Group to hold command buttons */
    // TGGroupFrame*   fCommandGroup;
  };
}


#endif
//
// EOF
//

  
  
