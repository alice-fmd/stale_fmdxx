// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    GainMaker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser gain. 
    @ingroup fmdxx_data    
*/
#include "config.h"
#include "fmdxx/GainMaker.h"
#include <rcudata/Channel.h>
#include <rcudata/DebugGuard.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <TString.h>
#include <TSystem.h>
#include <TF1.h>
#include <iostream>

//____________________________________________________________________
Fmdxx::GainMaker::GainMaker()
  : fReader(0), 
    fOutput(0), 
    fMeter(-1, 10),
    fFunc(0), 
    fPar(0),
    fNSamples(0), 
    fPulseStep(0),
    fSample(0),
    fCurrentStrip(0), 
    fCurrentPulse(0), 
    fCurrentSample(0)
{
  DGUARD("ctor");
  SetNSamples();
  SetPulseStep();
  SetSample();
  fFunc = new TF1("response", "pol1", 0, 255);
  fPar  = 1;
}
  
//____________________________________________________________________
void
Fmdxx::GainMaker::Exec(const char* src, const char* out, 
		       bool tree, bool all, bool wait)
{
  DGUARD("%s,%s", src, out);
  if (!Start(src, out, tree, all, wait)) { 
    std::cout << "Start failed!" << std::endl;
    return;
  }
  std::cout << "Starting the loop" << std::endl;
  Loop();
  std::cout << "At end of loop" << std::endl;
  End();
}

//____________________________________________________________________
bool
Fmdxx::GainMaker::Start(const char* src, const char* out, 
			bool tree, bool all, bool wait)
{
  DGUARD("%s,%s", src, out);
  // Check some settings 
  if (fNSamples <= 0) {
    std::cerr << "Invalid number of events/pulse/strip: " 
	      << fNSamples << std::endl;
    return false;
  }
  if (fPulseStep <= 0) {
    std::cerr << "Invalid pulse height step: " << fPulseStep << std::endl;
    return false;
  }
  if (fSample >= fOverSample) {
    std::cerr << "Invalid sample number: " << fSample << std::endl;
    return false;
  }
  if (fMinStrip > fMaxStrip || fMinStrip > 127 || fMaxStrip > 127) {
    std::cerr << "Invalid strip range: [" << fMinStrip << "," 
	      << fMaxStrip << "]" << std::endl;
    return 0;
  }

  // Print out a banner of information 
  std::cout << "Events/pulse/strip: "  << fNSamples << "\n" 
	    << "Pulse height step:  "  << fPulseStep << "\n" 
	    << "Strip range:        [" << fMinStrip << "," << fMaxStrip <<"]\n"
	    << "Over sampling rate: "  << fOverSample << "\n" 
	    << "Sample number:      "  << fSample << "\n" 
	    << "Time-bin offset:    "  << fOffset << "\n" 
	    << "Extra offset:       "  << fExtraOffset << std::endl;

  // Close old reader if any
  if (fReader) {
    delete fReader;
    fReader = 0;
  }

  // Make new reader 
  fReader = RcuData::Reader::Create(*this, src, -1, 0, tree, all, wait);
  if (!fReader) {
    std::cerr << "GainMaker: No reader made for input " 
	      << src << std::endl;
    return false;
  }
  // Open output if any
  if (out && out[0] != '\0') {
    fOutput = TFile::Open(out, "RECREATE");
    if (!fOutput) 
      std::cerr << "Failed to make output file " << out << std::endl;
  }  
  return true;
}

//____________________________________________________________________
void
Fmdxx::GainMaker::Loop()
{
  DGUARD("Looping over data");
  if (!fReader) {
    std::cerr << "No reader definend " << std::endl;
    return;
  }
  long ev = 0;
  // Reset some counters 
  fCurrentStrip  = fMinStrip;
  fCurrentPulse  = 0;
  fCurrentSample = 0;
  Bool_t done    = kFALSE;
  Bool_t reset   = kFALSE;
  Int_t  total   = fNSamples * 255 / fPulseStep * (fMaxStrip - fMinStrip + 1);
  fMeter.Reset(total, Form("Strip %3d, Pulse %3d", fCurrentStrip, 
			   fCurrentPulse), 1);
  while (true) {
    // Step counters if we need to 
    int  ret  = fReader->GetNextEvent();
    bool stop = false;
    switch (ret) {
    case RcuData::Reader::kData:       break;
    case RcuData::Reader::kSkip:       // Fall-through
    case RcuData::Reader::kNoData:     continue; break;
    case RcuData::Reader::kEndOfData:  // Fall-through
    case RcuData::Reader::kMaxReached: // Fall-through
    case RcuData::Reader::kError:      stop = true; break;
    }
    fCurrentSample++;
    if (fCurrentSample >= fNSamples) {
      fCurrentSample =  0;
      fCurrentPulse  += fPulseStep;
      if (fCurrentPulse >= 256) {
	fCurrentPulse = 0;
	fCurrentStrip++;
      }
      fMeter.SetPrefix(Form("Strip %3d, Pulse %3d",
			    fCurrentStrip,fCurrentPulse));
      if (fCurrentStrip > fMaxStrip) stop = true;
    }

    ev++;
    fMeter.Step();
    if (stop) break;
  }
  std::cout << std::endl;
}

//____________________________________________________________________
void
Fmdxx::GainMaker::End()
{
  DGUARD("At the end");
  if (fReader) {
    delete fReader;
    fReader = 0;
  }
  if (fFunc) fTop.CalculateGain(fFunc, fPar);
  if (fOutput) {
    fOutput->cd();
    fTop.WriteOut();
    std::cout << "Flushing to disk, please wait ... " << std::flush;
    fOutput->Write();
    fOutput->Close();
    fOutput = 0;
    std::cout << "done" << std::endl;
  }
}

  
//____________________________________________________________________
bool
Fmdxx::GainMaker::GotChannel(RcuData::Channel& c, bool valid)
{
  DGUARD("Got a channel %p (%svalid)", &c, (valid ? "" : "not "));
  if (fCurrentStrip > fMaxStrip) return false;
  Gains::Rcu*   rcu   = fTop.GetOrAdd(c.DDL());
  Gains::Board* board = rcu->GetOrAdd(c.Board());
  Gains::Chip*  chip  = board->GetOrAdd(c.Chip());
  Gains::Chan*  chan  = chip->GetOrAdd(c.ChanNo());
  fCurrent            = chan->GetOrAdd(fCurrentStrip);

  if (!valid) return true;
  unsigned t   = fOffset + fExtraOffset + fSample;
  unsigned adc = c.fData[t];
  fCurrent->Fill(fCurrentPulse, adc);
  
  return false; // Do not continue 
}

//____________________________________________________________________
bool
Fmdxx::GainMaker::GotData(RcuData::uint32_t t, RcuData::uint32_t adc) 
{
  DGUARD("Got ADC counts 0x%03x @ %4d", adc, t);
  // Check that we got the current channel 
  if (!fCurrent) { 
    std::cout << "No current object" << std::endl;
    return false;
  }
  unsigned match = fOffset + fExtraOffset + fSample;
  // As we read raw data backward, we need to reverse the comparison
  if (t > match) return true;
  if (t < match) return false;
  // OK, we're were we want to be. 
  fCurrent->Fill(fCurrentPulse, adc);
  return false;
}


//____________________________________________________________________
//
// EOF
//
