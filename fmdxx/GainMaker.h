// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    GainMaker.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser gain. 
    @ingroup fmdxx_data    
*/
#ifndef FMDXX_DATA_GAINMAKER
#define FMDXX_DATA_GAINMAKER
#ifndef RCUDATA_Reader
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_READER
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_PROGRESSMETER
# include <rcudata/ProgressMeter.h>
#endif
#ifndef RCUDATA_TYPES
# include <rcudata/Types.h>
#endif
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef FMDXX_GAINS
# include <fmdxx/Gains.h>
#endif
#ifndef FMDXX_MakerUtil
# include <fmdxx/MakerUtil.h>
#endif

namespace Fmdxx 
{
  /** @class GainMaker
      @brief Class to find pulser gain
      @ingroup fmdxx
  */
  class GainMaker : public MakerUtil, public RcuData::ChannelVisitor
  {
  public:
    /** Constructor */
    GainMaker();
    /** Called when ever we get a new channel from reader.
	@param c      Channel object
	@param hasAll @c true if @a c has all information.  
	@return @c true, means continue with this channel */
    virtual bool GotChannel(RcuData::Channel& c, bool hasAll);
    /** Called when ever we get a new ADC value for the current
	channel. 
	@param t   Time bin
	@param adc ADC value 
	@return  @c true, means continue with this channel */
    virtual bool GotData(RcuData::uint32_t t, RcuData::uint32_t adc);
    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param tree  Whether to make an output tree 
	@param all   Analyse all events 
	@param wait  Wait for data */
    virtual bool Start(const char* src, 
		       const char* out, 
		       bool        tree=false,
		       bool        all=true, 
		       bool        wait=true);
    /** Execute job.
	@param src   Input source
	@param out   Output file
	@param tree  Whether to make an output tree 
	@param all   Analyse all events 
	@param wait  Wait for data */
    virtual void Exec(const char* src, 
		      const char* out, 
		      bool        tree=false,
		      bool        all=true, 
		      bool        wait=true);
    /** Loop over the data */
    virtual void Loop();
    /** At end of job */
    virtual void End();
    /** Set number of events/pulse/strip 
	@param n     Number of events/pulse/strip */
    void SetNSamples(unsigned int n=100) { fNSamples = n; }
    /** Set pulse height step size 
	@param step  Step size of pulse */
    void SetPulseStep(unsigned int step=32) { fPulseStep = step; }
    /** Set the sample to use 
	@param t     Sample to use (@f$ 0\leq
	t<\mbox{oversample~rate}@f$)*/
    void SetSample(unsigned int t=2) { fSample = t; }
    /** Set the fit function to use 
	@param f Fit function 
	@param par  Parameter that is the gain */
    void SetFitFunction(TF1* f, unsigned par) { fFunc = f; fPar  = par; }
    
  protected:
    /** Reader */
    RcuData::Reader*       fReader;
    /** Output file */
    TFile*                 fOutput;
    /** Top-level of cache */
    Gains::Top             fTop;
    /** Current strip */ 
    Gains::Strip*          fCurrent;
    /** Progress meter */
    RcuData::ProgressMeter fMeter;
    /** Fit function */
    TF1*                   fFunc;
    /** parameter that is the gain */
    unsigned int           fPar;
    /** Sample to use */
    unsigned int           fSample;
    unsigned int           fNSamples;
    unsigned int           fPulseStep;
    /** Current strip no */
    unsigned int           fCurrentStrip;
    /** Current pulser value */
    unsigned int           fCurrentPulse;
    /** Current event in iteration */
    int           	   fCurrentSample;
  };
}
#endif
//
// EOF
//

