//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Gains.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup fmdxx_gains
*/
#include "fmdxx/Gains.h"
#include <rcudata/ProgressMeter.h>
#include <TObjArray.h>
#include <TDirectory.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>

namespace 
{
  RcuData::ProgressMeter fgWriteMeter;
  std::ofstream*         fCSV = 0;
}

namespace Fmdxx 
{
  namespace Gains
  {    
    //==================================================================
    const char* Top::GetName() const 
    {
      return Form("top_%01d", fId);
    }
    //________________________________________________________________
    Top::Elem_t* Top::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Top::WriteOut() 
    {
      fgWriteMeter.Reset(Count(),"Writing");
      TDirectory* savDir = gDirectory;
      for (Base_t::Iter_t i = Begin(); i != End(); ++i) 
	WriteElem(i->second);
      if (fSummary) fSummary->Write();
      std::cout << std::endl;
      if (savDir) savDir->cd();
    }
    //________________________________________________________________
    void Top::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Top::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Top::Fill(unsigned int rcu, 
		   unsigned int board,   
		   unsigned int chip, 
		   unsigned int channel, 
		   unsigned int strip, 
		   unsigned int pulse,   
		   unsigned int adc) 
    {
      Gains::Rcu* b = GetOrAdd(rcu);
      if (!b) return;
      b->Fill(board, chip, channel, strip, pulse, adc);
    }
    //__________________________________________________________________
    void Top::CalculateGain(TF1* f, unsigned int par) 
    {
      fgWriteMeter.Reset(Count(),"Calculating");
      fCSV = new std::ofstream("gain.csv");
      *fCSV << "Board,Chip,Channel,Strip,Gain,Error" << std::endl;
      Double_t low  = 100000000;
      Double_t high = 0;
      Double_t step = 0;
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	i->second->CalculateGain(f, par);
	if (!i->second->GetSummary()) continue;
	Double_t l = i->second->GetSummary()->GetXaxis()->GetXmin();
	Double_t h = i->second->GetSummary()->GetXaxis()->GetXmax();
	step       = std::max((h - l), step);
	low        = std::min(l, low);
	high       = std::max(h, high);
      }
      double        xlow  = low;
      double        xhigh = step * fCont.size() - .5;
      unsigned long nbins = long(xhigh - xlow);
      if (nbins > 1000000) {
	std::cout << std::endl;
	return;
      }
      fSummary = new TH1F(GetName(), 
			  Form("Summary of gains for rcu %d", fId),  
			  nbins, xlow, xhigh);
      fSummary->SetDirectory(0);
      fSummary->SetXTitle("Strip number");
      fSummary->SetYTitle("Gain [ADC/DAC]");
      fSummary->SetFillColor(6);
      fSummary->SetFillStyle(3001);
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	unsigned num = i->first;
	TH1*     h   = i->second->GetSummary();
	if (!h) continue;
	for (size_t j = 1; j <= h->GetNbinsX(); j++) {
	  double x  = low + step * num + h->GetBinCenter(j);
	  Int_t bin = fSummary->FindBin(x);
	  fSummary->SetBinContent(bin, h->GetBinContent(j));
	  fSummary->SetBinError(bin, h->GetBinError(j));
	}
      }
      std::cout << std::endl;
    }
    
    //==================================================================
    const char* Rcu::GetName() const 
    {
      return Form("rcu_%01d", fId);
    }
    //________________________________________________________________
    Rcu::Elem_t* Rcu::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Rcu::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Rcu::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Rcu::Fill(unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int pulse,   unsigned int adc) 
    {
      Gains::Board* b = GetOrAdd(board);
      if (!b) return;
      b->Fill(chip, channel, strip, pulse, adc);
    }
    //__________________________________________________________________
    void Rcu::CalculateGain(TF1* f, unsigned int par) 
    {
      *fCSV << fId << ',';
      Double_t low  = 100000000;
      Double_t high = 0;
      Double_t step = 0;
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	i->second->CalculateGain(f, par);
	if (!i->second->GetSummary()) continue;
	Double_t l = i->second->GetSummary()->GetXaxis()->GetXmin();
	Double_t h = i->second->GetSummary()->GetXaxis()->GetXmax();
	step       = std::max((h - l), step);
	low        = std::min(l, low);
	high       = std::max(h, high);
      }
      double        xlow  = low;
      double        xhigh = step * fCont.size() - .5;
      unsigned long nbins = long(xhigh - xlow);
      if (nbins > 1000000) {
	std::cout << std::endl;
	return;
      }
      fSummary = new TH1F(GetName(), 
			  Form("Summary of gains for rcu %d", fId),  
			  nbins, xlow, xhigh);
      fSummary->SetDirectory(0);
      fSummary->SetXTitle("Strip number");
      fSummary->SetYTitle("Gain [ADC/DAC]");
      fSummary->SetFillColor(6);
      fSummary->SetFillStyle(3001);
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	unsigned num = i->first;
	TH1*     h   = i->second->GetSummary();
	if (!h) continue;
	for (size_t j = 1; j <= h->GetNbinsX(); j++) {
	  double x  = low + step * num + h->GetBinCenter(j);
	  Int_t bin = fSummary->FindBin(x);
	  fSummary->SetBinContent(bin, h->GetBinContent(j));
	  fSummary->SetBinError(bin, h->GetBinError(j));
	}
      }
      std::cout << std::endl;
    }
    
    //==================================================================
    const char* Board::GetName() const 
    {
      return Form("board_%02d", fId);
    }
    //________________________________________________________________
    Board::Elem_t* Board::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Board::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Board::CountElem(Elem_t* e) const { return e->Count(); }
    
    //________________________________________________________________
    void Board::Fill(unsigned int chip,  unsigned int channel, 
		     unsigned int strip, unsigned int pulse, 
		     unsigned int adc) 
    {
      Gains::Chip* c = GetOrAdd(chip);
      if (!c) return;
      c->Fill(channel, strip, pulse, adc);
    }
    //__________________________________________________________________
    void Board::CalculateGain(TF1* f, unsigned int par) 
    {
      *fCSV << fId << ',';
      Double_t low  = 100000000;
      Double_t high = 0;
      Double_t step = 0;
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	i->second->CalculateGain(f, par);
	if (!i->second->GetSummary()) continue;
	Double_t l = i->second->GetSummary()->GetXaxis()->GetXmin();
	Double_t h = i->second->GetSummary()->GetXaxis()->GetXmax();
	step       = std::max((h - l), step);
	low        = std::min(l, low);
	high       = std::max(h, high);
      }
      double        xlow  = low;
      double        xhigh = step * fCont.size() - .5;
      unsigned long nbins = long(xhigh - xlow);
      if (nbins > 1000000) return;
      fSummary = new TH1F(GetName(), 
			  Form("Summary of gains for board %d", fId), 
			  nbins, xlow, xhigh);
      fSummary->SetDirectory(0);
      fSummary->SetXTitle("Strip number");
      fSummary->SetYTitle("Gain [ADC/DAC]");
      fSummary->SetFillColor(5);
      fSummary->SetFillStyle(3001);
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	if (i!=Begin()) *fCSV << ',';
	unsigned num = i->first;
	TH1*     h   = i->second->GetSummary();
	if (!h) continue;
	for (size_t j = 1; j <= h->GetNbinsX(); j++) {
	  double x  = low + step * num + h->GetBinCenter(j);
	  Int_t bin = fSummary->FindBin(x);
	  fSummary->SetBinContent(bin, h->GetBinContent(j));
	  fSummary->SetBinError(bin, h->GetBinError(j));
	}
      }
    }
  
    //==================================================================
    const char* Chip::GetName() const 
    {
      return Form("chip_%01d", fId);
    }
    //________________________________________________________________
    Chip::Elem_t* Chip::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chip::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chip::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chip::Fill(unsigned int channel, unsigned int strip, 
		    unsigned int pulse,   unsigned int adc) 
    {
      Gains::Chan* c = GetOrAdd(channel);
      if (!c) return;
      c->Fill(strip, pulse, adc);
    }
    //__________________________________________________________________
    void Chip::CalculateGain(TF1* f, unsigned int par) 
    {
      *fCSV << fId << ',';
      Double_t low  = 100000000;
      Double_t high = 0;
      Double_t step = 0;
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	if (i!=Begin()) *fCSV << ",,";
	i->second->CalculateGain(f, par);
	if (!i->second->GetSummary()) continue;
	Double_t l = i->second->GetSummary()->GetXaxis()->GetXmin();
	Double_t h = i->second->GetSummary()->GetXaxis()->GetXmax();
	step       = std::max((h-l), step);
	low        = std::min(l, low);
	high       = std::max(h, high);
      }
      double        xlow  = low;
      double        xhigh = step * fCont.size() - .5; 
      unsigned long nbins = long(xhigh - xlow);
      if (nbins > 1000000) return;
      fSummary = new TH1F(GetName(), 
			  Form("Summary of gains for chip %d", fId), 
			  nbins, xlow, xhigh);
      fSummary->SetDirectory(0);
      fSummary->SetXTitle(Form("Strip number", step));
      fSummary->SetYTitle("Gain [ADC/DAC]");
      fSummary->SetFillColor(4);
      fSummary->SetFillStyle(3001);
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	unsigned num = i->first;
	TH1*     h   = i->second->GetSummary();
	if (!h) continue;
	for (size_t j = 1; j <= h->GetNbinsX(); j++) {
	  double x  = low + step * num + h->GetBinCenter(j);
	  Int_t bin = fSummary->FindBin(x);
	  fSummary->SetBinContent(bin, h->GetBinContent(j));
	  fSummary->SetBinError(bin, h->GetBinError(j));
	}
      }
    }

    //==================================================================
    const char* Chan::GetName() const 
    {
      return Form("channel_%02d", fId);
    }
    //________________________________________________________________
    Chan::Elem_t* Chan::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chan::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chan::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chan::Fill(unsigned int strip, 
		    unsigned int pulse,   unsigned int adc) 
    {
      Gains::Strip* s = GetOrAdd(strip);
      if (!s) return;
      s->Fill(pulse, adc);
    }
    //__________________________________________________________________
    void Chan::CalculateGain(TF1* f, unsigned int par) 
    {
      *fCSV << fId << ",";
      fSummary = new TH1F(GetName(), 
			  Form("Distribution of gains for channel %2d", fId),
			  128, -.5, 127.5);
      fSummary->SetDirectory(0);
      fSummary->SetXTitle("Strip number");
      fSummary->SetYTitle("Gain [ADC/DAC]");
      fSummary->SetFillColor(3);
      fSummary->SetFillStyle(3001);
      for (Iter_t i = Begin(); i != End(); ++i) {
	if (!i->second) continue;
	if (i!=Begin()) *fCSV << ",,,";
	unsigned int num  = i->first;
	Float_t      err  = 0;
	Float_t      gain = i->second->CalculateGain(f, par, err);
	if (gain <= 0 || TMath::IsNaN(gain)) continue;
	Int_t        bin  = fSummary->FindBin(num);
	fSummary->SetBinContent(bin, gain);
	fSummary->SetBinError(bin, err);
      }
    }


    //==================================================================
    const char* Strip::GetName() const 
    {
      return Form("strip_%03d", fId);
    }
    //________________________________________________________________
    size_t Strip::Count() const
    {
      size_t ret = fCont.size();
      return ret;
    }

    //________________________________________________________________
    TH1* Strip::GetOrAdd(unsigned int pulse) 
    {
      if (!fCont[pulse]) {
	size_t chan  = ChanNo();
	size_t chip  = ChipNo();
	size_t board = BoardNo();
	std::string name(Form("b%02d_a%d_c%02d_s%03d_p%03d", 
			      board, chip, chan, fId, pulse));
	std::string title(Form("ADC Spectrum for Board %d, Chip %d, " 
			       "Channel %2d, Strip %4d, Pulse %3d", 
			       board, chip, chan, fId, pulse));
	// Info("Fill", "Making histogram %s", name.c_str());
	TH1I* hist = new TH1I(name.c_str(), title.c_str(), 1024, -.5, 1023.5);
	fCont[pulse] = hist;
	fCont[pulse]->SetXTitle("ADC counts");
	fCont[pulse]->SetYTitle("Events");
	fCont[pulse]->SetFillColor(2);
	fCont[pulse]->SetFillStyle(3001);
	fCont[pulse]->SetDirectory(0);
      }
      return fCont[pulse];
    }
    
    //________________________________________________________________
    void Strip::Fill(unsigned int s, unsigned int adc) 
    {
      TH1* h = GetOrAdd(s);
      // std::cout << h->GetName() << " fill at " << adc << std::endl;
      h->AddBinContent(adc+1);
      h->SetEntries(fCont[s]->GetEntries()+1);
    }
    //__________________________________________________________________
    void Strip::WriteOut() 
    {
      TDirectory* savDir = gDirectory;
      std::string name(GetName());
      if (!gDirectory->GetDirectory(name.c_str())) 
	gDirectory->mkdir(name.c_str())->cd();
      else 
	gDirectory->cd(name.c_str());
      for (Iter_t i = Begin(); i != End(); ++i) {
	fgWriteMeter.Step();
	WriteElem(i->second);
      }
      if (fGraph) fGraph->Write();
      savDir->cd();
    }

    //__________________________________________________________________
    Float_t Strip::CalculateGain(TF1* f, unsigned int par, Float_t& err) 
    {
      *fCSV << fId << ',';
      if (!f) return -1;
      if (fCont.size() <= 0) {
	std::cerr << "No histograms in this strip " << GetName() 
		  << std::endl;
	return -1;
      }
      fGraph = new TGraphErrors(fCont.size());
      fGraph->SetName(GetName());
      size_t chan  = ChanNo();
      size_t chip  = ChipNo();
      size_t board = BoardNo();
      fGraph->SetTitle(Form("ADC vs. Pulse Board %d, Chip %d, " 
			    "Channel %2d, Strip %4d", 
			    board, chip, chan, fId));
      fGraph->SetFillColor(2);
      fGraph->SetFillStyle(3001);
      fGraph->SetLineColor(2);
      fGraph->SetLineWidth(2);
      fGraph->SetLineStyle(2);
      fGraph->SetMarkerStyle(21);
      fGraph->SetMarkerColor(2);
      unsigned int j = 0;
      for (Iter_t i = Begin(); i != End(); ++i) {
	fgWriteMeter.Step();
	TH1*         hist  = i->second;
	unsigned int pulse = i->first;
	if (!hist) continue;
	// hist->Fit("gaus", "Q0");
	// TF1* gaus = hist->GetFunction("gaus");
	double mean = hist->GetMean();
	double rms  = hist->GetRMS();
	// Exclude outliers 
	hist->GetXaxis()->SetRangeUser(mean-2*rms, mean+2*rms);
	mean = hist->GetMean();
	rms  = hist->GetRMS();
	fGraph->SetPoint(j, pulse, mean);
	fGraph->SetPointError(j, 0, rms);
	hist->GetXaxis()->SetRangeUser(-.5, 1023.5);
	j++;
      }
      // TF1* gf = new TF1(*f);
      
      fGraph->Fit(f, "Q0");
      if (f->GetParameter(par) == 0) fGraph->Fit(f, "Q0");
      TF1*    gf   = fGraph->GetFunction(f->GetName());
      if (!gf) 
	std::cerr << "No function for " << fGraph->GetName() << std::endl;
      Float_t gain = (gf ? gf->GetParameter(par) : -1);
      if (TMath::IsNaN(gain)) 
	gain = -1;
      else 
	err  = (gf ? gf->GetParError(par) : 0);
      *fCSV << gain << ',' << err << std::endl;

      fGraph->GetXaxis()->SetTitle("Pulse");
      fGraph->GetYaxis()->SetTitle("#bar{ADC}");
      return gain;
    }

    //__________________________________________________________________
    void Strip::WriteElem(Elem_t* e) 
    {
      if (!e) return;
      e->Write();
    }
    
    //__________________________________________________________________
    void
    Strip::Reset() 
    {
      Base_t::Reset();
    }
    //__________________________________________________________________
    void
    Strip::Clear() 
    {
      Base_t::Clear();
    }
  }
}
//
// EOF
//
