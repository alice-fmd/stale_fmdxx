// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Gains.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Cache of gain scan data 
    @ingroup fmdxx_data    
*/
#ifndef FMDXX_DATA_GAINS
#define FMDXX_DATA_GAINS
#ifndef RCUDATA_CONTAINER
# include <rcudata/Container.h>
#endif
class TF1;
class TGraphErrors;

namespace Fmdxx 
{
  namespace Gains 
  {
    struct Rcu;
    struct Board;
    struct Chip;
    struct Chan;
    struct Strip;
    
    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup rcudata_spectra
    */
    struct Top : public RcuData::Container<Rcu>
    {
      /** Type of base class */
      typedef RcuData::Container<Rcu> Base_t;
      /** Constructor */
      Top(unsigned int id=0) : Base_t(id) {}
      /** @return Get name */
      const char* GetName() const;
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of board 
	  @return Pointer to board containeer */
      Rcu* GetOrAdd(unsigned int id);
      /** Put information in file */
      void WriteOut();
      /** Fill histograms 
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number 
	@param strip    Strip number 
	@param pulse    Pulse value
	@param adc      ADC Value */
      virtual void Fill(unsigned int rcu, 
			unsigned int board,   
			unsigned int chip, 
			unsigned int channel, 
			unsigned int strip,     
			unsigned int pulse,   
			unsigned int adc);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain */
      virtual void CalculateGain(TF1* func, unsigned int par);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      // ClassDef(Rcu,1);
    };
    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup rcudata_spectra
    */
    struct Rcu : public RcuData::Container<Board>
    {
      /** Type of base class */
      typedef RcuData::Container<Board> Base_t;
      /** Constructor */
      Rcu(unsigned int id, Top& top) : Base_t(id), fMother(&top) {}
      /** @return Get name */
      const char* GetName() const;
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of board 
	  @return Pointer to board containeer */
      Board* GetOrAdd(unsigned int id);
      /** Fill histograms 
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number 
	@param strip    Strip number 
	@param pulse    Pulse value
	@param adc      ADC Value */
      virtual void Fill(unsigned int board,   unsigned int chip, 
			unsigned int channel, unsigned int strip,     
			unsigned int pulse,   unsigned int adc);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain */
      virtual void CalculateGain(TF1* func, unsigned int par);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      Top* fMother;
      // ClassDef(Rcu,1);
    };
	
    //__________________________________________________________________
    /** Container of Board information 
	@ingroup rcudata_spectra
    */
    struct Board : public RcuData::Container<Chip>
    {
      /** Type of base class */
      typedef RcuData::Container<Chip> Base_t;
      /** Constructor */
      Board() {}
      /** Constructor 
	  @param id   Board number
	  @param rcu Controlling RCU */
      Board(unsigned int id, Rcu& rcu) : Base_t(id), fRcu(&rcu) {}
      /** @return Get name */
      const char* GetName() const;
      /** Get a chip, or if it doesn't exist, make a new one. 
	  @param id Id of chip 
	  @return Pointer to chip containeer */
      Chip* GetOrAdd(unsigned int id);
      /** Fill histograms 
	@param chip     Chip number
	@param channel  Channel number 
	@param strip    Strip number
	@param pulse    Pulse height
	@param adc      ADC Value */
      virtual void Fill(unsigned int chip,  unsigned int channel, 
			unsigned int strip, unsigned int pulse,
			unsigned int adc);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain */
      virtual void CalculateGain(TF1* func, unsigned int par);
    protected: 
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Up link */
      Rcu* fRcu;
      // ClassDef(Board,1);
    };
    

    //__________________________________________________________________
    /** Container of Chip information 
	@ingroup rcudata_spectra
    */
    struct Chip : public RcuData::Container<Chan>
    {
      /** Type of base class */
      typedef RcuData::Container<Chan> Base_t;
      /** Constructor */
      Chip() {}
      /** Constructor 
	  @param n Id of chip 
	  @param b Back link to board */
      Chip(unsigned int n, Board& b) : Base_t(n), fBoard(&b) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Board& Mother() const { return *fBoard; }
      /** Get a channel, or if it doesn't exist, make a new one. 
	  @param id Id of chan 
	  @return Pointer to chan containeer */
      Chan* GetOrAdd(unsigned int id);
      /** Fill histograms 
	  @param channel  Channel number 
	  @param strip    Strip number
	  @param pulse    Pulse height
	  @param adc      ADC Value */
      virtual void Fill(unsigned int channel, unsigned int strip,
			unsigned int pulse,   unsigned int adc);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain */
      virtual void CalculateGain(TF1* func, unsigned int par);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Back link to board */
      Board* fBoard;
      // ClassDef(Chip,1);
    };

    //__________________________________________________________________
    /** Container of Channel information 
	@ingroup rcudata_spectra
    */
    struct Chan : public RcuData::Container<Strip>
    {
      /** Type of base class */                     
      typedef RcuData::Container<Strip> Base_t;
      /** Constructor */
      Chan() {}
      /** Constructor 
	  @param n Id of channel 
	  @param chip Back link to chip */
      Chan(unsigned int n, Chip& chip) : Base_t(n), fChip(&chip) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Chip& Mother() const { return *fChip; }
      /** Fill an ADC value into the appropriate spectrum
	  @param strip Strip number 
	  @param pulse Pulse height
	  @param data  ADC value */
      void Fill(unsigned int strip, unsigned int pulse, unsigned int data);
      /** Hide member function */
      Strip* GetOrAdd(unsigned int id);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain */
      virtual void CalculateGain(TF1* func, unsigned int par);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.
	  @param e Element to count
	  @return always 1 */
      virtual size_t CountElem(Elem_t* e)  const;
      /** Back link to chip */
      Chip* fChip;
      // ClassDef(Chan,1);
    };  
    //__________________________________________________________________
    /** Container of Channel information 
	@ingroup rcudata_spectra
    */
    struct Strip : public RcuData::Container<TH1>
    {
      /** base class type */
      typedef RcuData::Container<TH1> Base_t;
      /** Constructor */
      Strip() {}
      /** Constructor 
	  @param n Id of stripnel 
	  @param chan Back link to chip */
      Strip(unsigned int n, Chan& chan) : Base_t(n), fChan(&chan), fGraph(0) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Chan& Mother() const { return *fChan; }
      /** @return channel number */
      unsigned int ChanNo() const { return Mother().Id(); }
      /** @return chip number */ 
      unsigned int ChipNo() const { return Mother().Mother().Id(); }
      /** @return Board number */ 
      unsigned int BoardNo() const { return Mother().Mother().Mother().Id(); }
      /** Fill an ADC value into the appropriate spectrum
	  @param pulse Pulse height number
	  @param adc   ADC value */
      void Fill(unsigned int pulse, unsigned int adc);
      /** Clear (delete) all contained objects */
      virtual void Clear();
      /** Reset contents */
      virtual void Reset();
      /** @return number of contained object (recursive) */
      virtual size_t Count() const;
      /** Write histograms to disk. */
      void WriteOut();
      /** Hide member function */
      TH1* GetOrAdd(unsigned int id);
      /** Calculate the gain, using a fit to the given function. 
	  @param func Function to use for the fit 
	  @param par  Parameter number that represents the gain 
	  @param e    Return of error 
	  @return Gain */
      virtual Float_t CalculateGain(TF1* func, unsigned int par, Float_t& e);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This just returns 1. 
	  @return always 1 */
      virtual size_t CountElem(Elem_t*)  const { return 1; }
      /** Back link to chip */
      Chan* fChan;
      /** Graph of points */ 
      TGraphErrors* fGraph;
      // ClassDef(Strip,1);
    };      
  }
}
#endif
//
// EOF
//
