// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    MakerUtil.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Utility class for various FMD analysers
    @ingroup fmdxx_data    
*/
#ifndef FMDXX_DATA_MAKERUTIL
#define FMDXX_DATA_MAKERUTIL
namespace Fmdxx 
{
  /** @class MakerUtil
      @brief Utility class for various FMD analysers
      @ingroup fmdxx
  */
  class MakerUtil
  {
  public:
    /** Constructor */
    MakerUtil();
    /** @param board Bit mask of boards to analyse */
    void SetBoards(unsigned int board) { fBoards = board; }
    /** @param over Oversampling used when recording the data */
    virtual void SetOverSample(unsigned int over) { fOverSample = over; }
    /** Set strip range 
	@param min Minimum strip 
	@param max Maximum strip */
    void SetRange(unsigned int min, unsigned int max) 
    {
      fMinStrip = min;
      fMaxStrip = max;
    }
    /** Set the offset 
	@param o offset. */
    void SetOffset(unsigned o=14) { fOffset = o; }
    /** Set the extra off set 
	@param e Extra offset */
    void SetExtraOffset(unsigned e=5) { fExtraOffset = e; }
  protected:
    /** @param t Time bin
	@return  Sub-sample corresponding to the time bin */
    unsigned int Timebin2Sample(unsigned t) const;
    /** @param t Time bin
	@return  strip corresponding to the time bin */
    unsigned int Timebin2Strip(unsigned t) const;
    /** @param t Time bin
	@return  strip and sub-sample corresponding to the time bin */
    float Timebin2StripSample(unsigned t) const;
    /** Bit mask of boards */
    unsigned fBoards;
    /** Oversampling ratio used when recording the data */
    unsigned fOverSample;
    /** First strip */
    unsigned fMinStrip;
    /** Last strip */
    unsigned fMaxStrip;
    /** Offset */
    unsigned fOffset;
    /** Extra offset */
    unsigned fExtraOffset;
  };
  //__________________________________________________________________
  inline 
  MakerUtil::MakerUtil() 
    : fBoards(0x1), 
      fOverSample(4), 
      fMinStrip(0),
      fMaxStrip(127), 
      fOffset(14), 
      fExtraOffset(5)
  {}
  //__________________________________________________________________
  inline unsigned int
  MakerUtil::Timebin2Strip(unsigned t) const
  {
    unsigned int ret = fMinStrip;
    if (t >= fOffset - fExtraOffset) 
      ret += (t - fOffset - fExtraOffset) / fOverSample;
    return ret;
  }
  
  //__________________________________________________________________
  inline unsigned int
  MakerUtil::Timebin2Sample(unsigned t) const
  {
    unsigned int ret = (t - fOffset - fExtraOffset) % fOverSample;
    return ret;
  }

  //__________________________________________________________________
  inline float 
  MakerUtil::Timebin2StripSample(unsigned t) const
  {
    float tb  = t;
    float ret = (tb - fOffset - fExtraOffset) / fOverSample + fMinStrip;
#if 0
    std::cout << "\n"
	      << t << "->(" << tb << "-" << fOffset << "-" << fExtraOffset 
	      << ")/" << fOverSample << "+" << fMinStrip << "=" 
	      << ret << std::endl;
#endif
    return ret;
  }    
}

#endif
//
// EOF
//



