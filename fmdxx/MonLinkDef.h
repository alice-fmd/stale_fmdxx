// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: MonLinkDef.h,v 1.2 2009-02-09 23:09:50 hehi Exp $ 
//
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    fmdxx/GuiLinkDef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:57 2004
    @brief   Linkage specifications 
*/
#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Fmdxx;
#pragma link C++ class     Fmdxx::MonitorFrame;
#pragma link C++ class     Fmdxx::MonitorBoard;
#pragma link C++ class     Fmdxx::MonitorTree;


//____________________________________________________________________ 
//  
// EOF
//
