#include "Monitor.h"

#include <TStyle.h>
#include <TGListTree.h>
#include <TGraph.h>
#include <TSystem.h>
#include <TH2F.h>
#include "fmdGui.xpm"
#include <cmath>
#include <sstream>
#include <TClass.h>
#define  TOP_NAME      "top"
#define  RCU_TEMPL     "rcu %4d"
#define  BOARD_TEMPL   "board %2d"
#define  CHIP_TEMPL    "altro %1d"
#define  CHANNEL_TEMPL "channel %2d"
#define  STRIP_TEMPL   "strip %3d"
#define  SAMPLE_TEMPL  "sample %1d"
#define  RCU_PATH      "/"  TOP_NAME "/"  RCU_TEMPL 
#define  BOARD_PATH    RCU_PATH      "/"  BOARD_TEMPL 
#define  CHIP_PATH     BOARD_PATH    "/"  CHIP_TEMPL
#define  CHANNEL_PATH  CHIP_PATH     "/"  CHANNEL_TEMPL
#define  STRIP_PATH    CHANNEL_PATH  "/"  STRIP_TEMPL
#define  SAMPLE_PATH   STRIP_PATH    "/"  SAMPLE_TEMPL

//====================================================================
Fmdxx::MonitorBoard::MonitorBoard(unsigned int addr, 
				  const Coords_t& c, 
				  int nlevel)
  : fAddress(addr), fCoords(c), fMaxR(0), 
    fFrame(Form("board_x%02x", addr), "Hits", 1, -1, 1, 1, -1, 1),
    fShapesDrawn(false)
{
  fFrame.SetStats(kFALSE);
  fFrame.GetXaxis()->SetTitle("x [cm]");
  fFrame.GetYaxis()->SetTitle("y [cm]");
  fFrame.GetZaxis()->SetTitle("z [cm]");
  fFrame.SetDirectory(0);
  
  if (nlevel < 1) nlevel = gStyle->GetNumberOfColors();
  fCounts.resize(nlevel);
  fHits.resize(nlevel);
  for (int i = 0; i < nlevel; i++) {
    fCounts[i] = 0;
    fHits[i]   = new TGraph;
    int idx    = int(float(i)/nlevel * gStyle->GetNumberOfColors());
    int col    = gStyle->GetColorPalette(idx);
    fHits[i]->SetName(Form("board_x%02x_level_%02d", fAddress, i));
    fHits[i]->SetMarkerColor(col);
    fHits[i]->SetLineColor(col);
    fHits[i]->SetFillColor(col);
    fHits[i]->SetMarkerSize((i + 1) * .2);
  }
}


//____________________________________________________________________
Fmdxx::MonitorBoard::~MonitorBoard()
{
  for (Shapes_t::iterator i = fShapes.begin(); i != fShapes.end(); ++i) 
    delete i->second;
  for (Hits_t::iterator i = fHits.begin(); i != fHits.end(); ++i) 
    delete *i;
}

//____________________________________________________________________
unsigned int
Fmdxx::MonitorBoard::Channel2Module(unsigned int altro, 
				    unsigned int channel) const
{
  unsigned int ret = 0;
  unsigned int nA  = IsInner() ? 2 : 4;
  switch (altro) {
  case 0: ret = 0; break;
  case 1: ret = nA; break;
  case 2: ret = nA + nA / 2; break;
  }
  ret += (channel * nA) / 16;
  return ret;
}
     
//____________________________________________________________________
bool
Fmdxx::MonitorBoard::CheckModule(unsigned int altro, unsigned int channel) 
{
  unsigned int       m  = Channel2Module(altro, channel);
  Shapes_t::iterator i  = fShapes.find(m);
  if  (i != fShapes.end()) return false;

  // Create the shape 
  unsigned   nV    = fCoords.size();
  double     maxR  = 0;
  double     a     = M_PI / 180 * ((2 * m + 1) * Theta(Id()) + 
				   (IsLower() ? 180 : 0));
  double     x0, y0;
  TGraph*    g     = new TGraph(nV+1);
  fShapes[m]       = g;
  for (unsigned c = 0; c < nV; c++) {
    const Coord_t&  v = fCoords[nV - 1 - c];
    Coord_t         w(v.first * cos(a) - v.second * sin(a), 
		      v.first * sin(a) + v.second * cos(a));
    g->SetPoint(c, w.first, w.second);
    if (c == 0) { x0 = w.first; y0 = w.second; }
    maxR = std::max(maxR, sqrt(v.first*v.first + v.second*v.second));
  }
  g->SetName(Form("board0x%02x_mod0x%02x", fAddress, m));
  g->SetPoint(nV, x0, y0);
  g->SetFillColor((IsInner()?(m % 2 == 0 ? 18 : 17):(m % 2 == 0 ? 20 : 23)));
  g->SetFillStyle(3001);
  g->SetLineColor(1);
  g->SetLineWidth(1);
  g->SetLineStyle(2);
  fMaxR = std::max(maxR, fMaxR);
  return true;
}

//____________________________________________________________________
void
Fmdxx::MonitorBoard::Fill(unsigned int altro, unsigned int channel, 
			  unsigned int strip, unsigned int value, 
			  unsigned int min,   unsigned int max) 
{
  // Check if the module is there, and if not, create it.
  CheckModule(altro, channel);
  unsigned m = Channel2Module(altro, channel);
  unsigned u = (value - min);
  unsigned l = (max-min);
  int      i = std::min(int(fCounts.size()*u / l), int(fHits.size()-1));
  TGraph*  g = fHits[i];
  if (!g) return;

  unsigned n = (IsInner() ? 8 : 4);
  int      v = (channel % n) / 2;
  char     c = Id();
  double   a = (M_PI / 180  * ((2 * m + .5 + (channel % 2)) * Theta(c) + 
			       (IsLower() ? 180 : 0) + 
			       (double(rand()) / RAND_MAX -.5)* Theta(c) / 2));
  double   r = LowR(c) + (HighR(c) - LowR(c)) / Strips(c) * (strip + v * 128);
  if (r > fMaxR) {
    std::cerr << "Radius " << r << " >= " << fMaxR << " for board " 
	      << fAddress << "\n"
	      << "channel #\t" << channel << "\tstrip #\t" << strip 
	      << " ->\t" << r << "\t(" << LowR(c) << "+(" << HighR(c) 
	      << "-" << LowR(c) << ")/" << Strips(c) << "*(" 
	      << strip << "+" << v  << "*128)" << std::endl;
    return;
  }
  double   x = cos(a) * r;
  double   y = sin(a) * r;
  g->SetPoint(fCounts[i]++, x, y);
}

//____________________________________________________________________
void
Fmdxx::MonitorBoard::Clear() 
{
  for (size_t i = 0; i < fCounts.size(); i++) fCounts[i] = 0;
}

//____________________________________________________________________
void
Fmdxx::MonitorBoard::Draw(bool frame)
{
  std::cout << "Drawing board: " << GetName() << " # of shapes: " 
	    << fShapes.size() << std::endl;
  if (frame) fFrame.Draw();
  for (Shapes_t::iterator i = fShapes.begin(); i != fShapes.end(); ++i) {
    if (!i->second) continue;
    TGraph* g = i->second;
    // std::cout << "Drawing clone of " << g->GetName() << std::endl;
    g->DrawClone("f same");
    g->DrawClone("l same");
  }
  for (size_t i = 0; i < fHits.size(); i++) {
    if (!fHits[i] || !fCounts[i]) continue;
    TGraph* g = fHits[i];
    g->Set(fCounts[i]);
    g->Draw("same p");
  }
}

//____________________________________________________________________
void
Fmdxx::MonitorBoard::ReDraw()
{
  static double oldR = 0;
  if (!fShapesDrawn) { 
    for (Shapes_t::iterator i = fShapes.begin(); i != fShapes.end(); ++i) {
      if (!i->second) continue;
      TGraph* g = i->second;
      // std::cout << "Drawing clone of " << g->GetName() << std::endl;
      g->DrawClone("f same");
      g->DrawClone("l same");
    }
    fShapesDrawn = true;
  }
  
  for (size_t i = 0; i < fHits.size(); i++) {
    if (!fHits[i] || !fCounts[i]) continue;
    TGraph* g = fHits[i];
    g->Set(fCounts[i]);
  }
  gPad->Modified();
  gPad->Update();
  if (oldR != fMaxR) {
    AdjustFrame(fMaxR);
    oldR = fMaxR;
  }
}
  
//____________________________________________________________________
void
Fmdxx::MonitorBoard::AdjustFrame(double maxR) 
{
  // std::cout << "Adjusting frame to " << maxR << std::endl;
  double maxRR = 1.1 * maxR;
  fFrame.SetBins(1, -maxRR, maxRR, 1, -maxRR, maxRR);
}

//____________________________________________________________________
void
Fmdxx::MonitorBoard::SetFrameTitle(const char* title) 
{
  // std::cout << "Adjusting frame to " << maxR << std::endl;
  fFrame.SetTitle(title);
}

//====================================================================
Fmdxx::MonitorRcu::MonitorRcu(UInt_t rcu, UShort_t levels)
  : fRcu(rcu), 
    fLevels(levels),
    fBoards()
{
}
//____________________________________________________________________
void
Fmdxx::MonitorRcu::Clear() 
{
  for (Boards_t::iterator i = fBoards.begin(); i != fBoards.end(); ++i) 
    i->second->Clear();
}

//____________________________________________________________________
void
Fmdxx::MonitorRcu::Draw(bool frame)
{
  for (Boards_t::iterator i = fBoards.begin(); i != fBoards.end(); ++i) { 
    i->second->Draw(frame);
    frame = false;
  }
}

//____________________________________________________________________
void
Fmdxx::MonitorRcu::ReDraw()
{
  static double oldR = 0;
  double        maxR = 0;
  
  for (Boards_t::iterator i = fBoards.begin(); i != fBoards.end(); ++i) {
    maxR = TMath::Max(maxR, i->second->MaxR());
    i->second->ReDraw();
  }
  if (oldR != maxR) { 
    oldR = maxR;
    for (Boards_t::iterator i = fBoards.begin(); i != fBoards.end(); ++i) {
      i->second->AdjustFrame(maxR);
    }
  }
}
//____________________________________________________________________
Fmdxx::MonitorBoard*
Fmdxx::MonitorRcu::CheckBoard(unsigned int board)
{
  Boards_t::iterator i = fBoards.find(board);
  if (i != fBoards.end() && i->second) return i->second;
  const Coords_t& shape = GetShape(board);
  fBoards[board] = new MonitorBoard(board, shape, fLevels);
  fBoards[board]->SetFrameTitle(Form("FMD%d", RcuNo()));
  return fBoards[board];
}
//____________________________________________________________________
const Fmdxx::MonitorRcu::Coords_t&
Fmdxx::MonitorRcu::GetShape(unsigned int board)
{
  bool       inner   = (board == 0x0 || board == 0x10); 
  char       c       = inner ? 'I'    : 'O';
  Coords_t&  array   = inner ? fInner : fOuter;
  if (array.size()) return array;
 
  double   lowR    = MonitorBoard::LowR(c);
  double   highR   = MonitorBoard::HighR(c);
  double   theta   = MonitorBoard::Theta(c);
  double   waferR  = 13.4/2;
  
  // Initialize 
  Double_t tanTheta  = tan(theta * M_PI / 180.);
  Double_t tanTheta2 = pow(tanTheta,2);
  Double_t r2        = pow(waferR,2);
  Double_t yA        = tanTheta * lowR;
  Double_t lr2       = pow(lowR, 2);
  Double_t hr2       = pow(highR,2);
  Double_t xD        = lowR + sqrt(r2 - tanTheta2 * lr2);
  Double_t xD2       = pow(xD,2);
  Double_t yB        = sqrt(r2 - hr2 + 2 * highR * xD - xD2);
  Double_t xC        = ((xD + sqrt(-tanTheta2 * xD2 + r2 + r2 * tanTheta2)) 
			/ (1 + tanTheta2));
  Double_t yC        = tanTheta * xC;
  
  array.resize(6);
  array[0] = Coord_t(lowR,  -yA);
  array[1] = Coord_t(xC,    -yC);
  array[2] = Coord_t(highR, -yB);
  array[3] = Coord_t(highR,  yB);
  array[4] = Coord_t(xC,     yC);
  array[5] = Coord_t(lowR,   yA); 
  return array;
}

//____________________________________________________________________
void
Fmdxx::MonitorRcu::Fill(unsigned int board, 
			 unsigned int chip, 
			 unsigned int channel, 
			 unsigned int t, 
			 unsigned int adc, 
			 unsigned int min, 
			 unsigned int max)
{
  MonitorBoard* b = CheckBoard(board);
  b->Fill(chip, channel, t, adc, min, max);
}

//====================================================================
Fmdxx::MonitorTree::MonitorTree(TGCompositeFrame& p, UInt_t w, UInt_t h)
  : RcuGui::MonitorTree(p, w, h, TOP_NAME), 
    fCurrent(0),
    fSelectionChanged(false),
    fMinAdc(0), 
    fMaxAdc(1024), 
    fLevels(-1)
{
  gStyle->SetPalette(1);
  fTopEntry->SetUserData(fTop.GetSummary());
}

//____________________________________________________________________
void Fmdxx::MonitorTree::SetOverSample(unsigned int over)
{
  Spectra::Strip::fgChosenSample = (over == 1 ? 0 :
				    over == 2 ? 1 :
				    over == 4 ? 2 : 0);
  MakerUtil::SetOverSample(over);
}

//____________________________________________________________________
void
Fmdxx::MonitorTree::UpdateList()
{
  for (Rcus_t::iterator i = fRcus.begin(); i != fRcus.end(); ++i) 
    i->second->Clear();
  RcuGui::MonitorTree::UpdateList();
}

//____________________________________________________________________
void
Fmdxx::MonitorTree::HandleSelect()
{
  static TGListTreeItem* old = 0;
  static bool            first = true;
  if (!fSelectionChanged) 
    fSelectionChanged        = (old != fCurrentEntry);
  if (first) { 
    first = false;
    return;
  }
  old                        = fCurrentEntry;
  TObject* user              = Current();
  if (!user || !fCurrentEntry) return;
  // fSelectionChanged = true;
  
  TVirtualPad* pad = cd();
  if (fCurrentEntry == fTopEntry) { 
    if (fSelectionChanged) { 
      pad->Clear();
      pad->Divide(2,2);
    }
    pad->SetLogy(kFALSE);
    Int_t j    = 1;
    for (Rcus_t::iterator i = fRcus.begin(); i != fRcus.end(); ++i, j++) { 
      pad->cd();
      pad->cd(j);
      if (fSelectionChanged) i->second->Draw(true);
      else                   i->second->ReDraw();
    }    
 }
  else if (fCurrentEntry->GetParent() == fTopEntry) { 
    std::stringstream s(fCurrentEntry->GetText());
    char r, c, u, us;
    unsigned int rcu;
    s >> r >> c >> u >> us >> rcu;
    Rcus_t::iterator i = fRcus.find(rcu);
    if (i == fRcus.end() || !i->second) return;
    if (fSelectionChanged) i->second->Draw(true);
    else                   i->second->ReDraw();
  }
  else if (user && user->IsA()->InheritsFrom(TH1::Class())) {
    TH1*         h   = dynamic_cast<TH1*>(user);
    if (!h) return;
    pad->SetLogy(log10(h->GetMaximum() - h->GetMinimum()) > 2);
    h->Draw(h->GetDrawOption());
  }
  fSelectionChanged = false;
  UpdateCanvas();
}


//____________________________________________________________________
void
Fmdxx::MonitorTree::DrawDetector()
{
  std::cout << "Drawing detector" << std::endl;
}




//____________________________________________________________________
Fmdxx::MonitorRcu*
Fmdxx::MonitorTree::CheckRcu(unsigned int rcu)
{
  // std::cout << "Making board # " << board << std::endl;
  Rcus_t::iterator i = fRcus.find(rcu);
  if (i != fRcus.end() && i->second) return i->second;
  return fRcus[rcu] = new MonitorRcu(rcu, fLevels);
}

  

//____________________________________________________________________
bool
Fmdxx::MonitorTree::MakeChannel(unsigned int rcu, 
				unsigned int board, 
				unsigned int chip, 
				unsigned int channel)
{
  Spectra::Rcu*   rcuCache   = fTop.Get(rcu);
  Spectra::Board* boardCache = (rcuCache  ? rcuCache->Get(board)    : 0);
  Spectra::Chip*  chipCache  = (boardCache? boardCache->Get(chip)   : 0);
  Spectra::Chan*  chanCache  = (chipCache ? chipCache->Get(channel) : 0);
  
  TGListTreeItem* rcuEntry   = 0;
  TGListTreeItem* boardEntry = 0;
  TGListTreeItem* chipEntry  = 0;
  TGListTreeItem* chanEntry  = 0;
  std::string name;
  if (!chanCache) {
    if (!chipCache) {
      if (!boardCache) {
	if (!rcuCache) {
	  name     = Form(RCU_TEMPL, rcu);
	  rcuCache = fTop.GetOrAdd(rcu);
	  rcuEntry = fList.AddItem(fTopEntry, name.c_str());
	  rcuEntry->SetUserData(rcuCache->GetSummary());
	}
	else { 
	  name     = Form(RCU_PATH, rcu);
	  rcuEntry = fList.FindItemByPathname(name.c_str());
	}
	name       = Form(BOARD_TEMPL, board);
	boardCache = rcuCache->GetOrAdd(board);
	boardEntry = fList.AddItem(rcuEntry, name.c_str());
	boardEntry->SetUserData(boardCache->GetSummary());
	fNeedSort.push_back(rcuEntry);
      }
      else {
	name       = Form(BOARD_PATH, rcu, board);
	boardEntry = fList.FindItemByPathname(name.c_str());
      }
      name      = Form(CHIP_TEMPL, chip);
      chipCache = boardCache->GetOrAdd(chip);
      chipEntry = fList.AddItem(boardEntry, name.c_str());
      chipEntry->SetUserData(chipCache->GetSummary());
      fNeedSort.push_back(boardEntry);
    }
    else {
      name      = Form(CHIP_PATH, rcu, board, chip);
      chipEntry = fList.FindItemByPathname(name.c_str());
    }
    name      = Form(CHANNEL_TEMPL, channel);
    chanCache = chipCache->GetOrAdd(channel);
    chanEntry = fList.AddItem(chipEntry, name.c_str());
    chanEntry->SetUserData(chanCache->GetSummary());
    fNeedSort.push_back(chipEntry);
    fNeedUpdate = true;
  }
  fCurrent     = chanCache;
  fCachedEntry = chanEntry;

  return true;
}

//____________________________________________________________________
bool
Fmdxx::MonitorTree::MakeTimebin(unsigned int rcu, 
				unsigned int board, 
				unsigned int chip, 
				unsigned int channel, 
				unsigned int t)
{
  // std::cout << "Calling, MakeTimebin" << std::endl;
  if (!fCurrent) { 
    std::cout << "No Current cache object" << std::endl;
    return false;
  }
  unsigned int    strip        = Timebin2Strip(t);
  unsigned int    sample       = Timebin2Sample(t);
  if (strip > fMaxStrip || strip < fMinStrip) return true;
  Spectra::Strip* stripCache   = fCurrent->Get(strip);
  TH1*            spectrum     = (stripCache?stripCache->Get(sample):0);
  TGListTreeItem* stripEntry   = 0;
  TGListTreeItem* sampleEntry  = 0;
  TGListTreeItem* channelEntry = 0;
  std::string name;

  if (!spectrum) {
    if (!stripCache) {
      name          = Form(CHANNEL_PATH, rcu, board, chip, channel);
      channelEntry  = fList.FindItemByPathname(name.c_str());
      name          = Form(STRIP_TEMPL, strip);
      stripCache    = fCurrent->GetOrAdd(strip);
      fCachedEntry  = fList.AddItem(channelEntry, name.c_str());
      fCachedEntry->SetUserData(stripCache->GetSummary());
      fNeedSort.push_back(channelEntry);
      fNeedUpdate   = true;
    }
    name     = Form(SAMPLE_TEMPL, sample);
    spectrum = stripCache->GetOrAdd(sample);
    
    if (!fCachedEntry) {
      std::string path;
      path          = Form(STRIP_PATH, rcu, board, chip, channel, strip);
      fCachedEntry = fList.FindItemByPathname(path.c_str());
    }
    fList.AddItem(fCachedEntry,name.c_str(), 
		  fHist1DIcon, fHist1DIcon)->SetUserData(spectrum);
    fNeedSort.push_back(fCachedEntry);
    fNeedUpdate = true;
  }
  return true;
}

//____________________________________________________________________
void
Fmdxx::MonitorTree::Fill(unsigned int rcu, 
			 unsigned int board, 
			 unsigned int chip, 
			 unsigned int channel, 
			 unsigned int t, 
			 unsigned int adc)
{
  unsigned int    strip        = Timebin2Strip(t);
  unsigned int    sample       = Timebin2Sample(t);
  if (strip > fMaxStrip || strip < fMinStrip) return;

  fTop.Fill(rcu, board, chip, channel, strip, sample, adc);
  if (adc < fMinAdc) return;
  MonitorRcu* r = CheckRcu(rcu);
  r->Fill(board, chip, channel, strip, adc, fMinAdc, fMaxAdc);

  return;
}

//____________________________________________________________________
void
Fmdxx::MonitorTree::Clear()
{
  fTop.Clear();
}

//____________________________________________________________________
void
Fmdxx::MonitorTree::Reset()
{
  fTop.Reset();
}

//====================================================================
Fmdxx::MonitorFrame::MonitorFrame(TGCompositeFrame& f)
  : RcuGui::MonitorFrame(f), 
    fExtraHints(kLHintsExpandX,0,3,3,3), 
    fExtraFrame(&fTopCont, "Extra", kVerticalFrame), 
    fMinAdc(fExtraFrame, "Min. ADC", 0, 1023), 
    fMaxAdc(fExtraFrame, "Max. ADC", 0, 1023), 
    fLevels(fExtraFrame, "Levels",   -1, 30), 
    fFmdTree(0)
{
  fMinAdc.fView.Connect("ValueSet(Long_t)", "Fmdxx::MonitorFrame", 
			this, "HandleMin()");
  fMaxAdc.fView.Connect("ValueSet(Long_t)", "Fmdxx::MonitorFrame", 
			this, "HandleMax()");
  fLevels.fView.Connect("ValueSet(Long_t)", "Fmdxx::MonitorFrame", 
			this, "HandleLevels()");
  fTopCont.AddFrame(&fExtraFrame, &fExtraHints);
}
//____________________________________________________________________
void
Fmdxx::MonitorFrame::SetFmdTree(MonitorTree& tree)
{
  fFmdTree           = &tree;
  RcuGui::MonitorFrame::SetTree(tree);
  fMinAdc.SetValue(fFmdTree->GetMinAdc());
  fMaxAdc.SetValue(fFmdTree->GetMaxAdc());
  fLevels.SetValue(fFmdTree->GetLevels());
}

//____________________________________________________________________
void
Fmdxx::MonitorFrame::HandleMin()
{
  unsigned int min = fMinAdc.GetValue();
  unsigned int max = fMaxAdc.GetValue();
  if (min >= max) { 
    min = max;
    fMinAdc.SetValue(max);
  }
  if (fFmdTree) fFmdTree->SetAdcRange(min, max);
}
//____________________________________________________________________
void
Fmdxx::MonitorFrame::HandleMax()
{
  unsigned int min = fMinAdc.GetValue();
  unsigned int max = fMaxAdc.GetValue();
  if (min >= max) { 
    max = min;
    fMaxAdc.SetValue(min);
  }
  if (fFmdTree) fFmdTree->SetAdcRange(min, max);
}
//____________________________________________________________________
void
Fmdxx::MonitorFrame::HandleLevels()
{}

//____________________________________________________________________
//
// EOF
//
