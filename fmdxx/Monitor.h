// -*- mode: C++ -*-
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    MonitorFrame.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of MonitorFrame
*/
#ifndef FMDXX_MONITOR
# define FMDXX_MONITOR
# ifndef FMDXX_SPECTRA
#  include <fmdxx/Spectra.h>
# endif
# ifndef FMDXX_MAKERUTIL
#  include <fmdxx/MakerUtil.h>
# endif
# ifndef RCUGUI_MONITORTREE_H
#  include <rcugui/MonitorTree.h>
# endif
# ifndef RCUGUI_MONITORFRAME_H
#  include <rcugui/MonitorFrame.h>
# endif
# ifndef ROOT_TH2F
#  include <TH2F.h>
# endif
# ifndef ROOT_TGraph
#  include <TGraph.h>
# endif
# ifndef __MAP__
#  include <map>
# endif
# ifndef __VECTOR__
#  include <vector>
# endif
// class TGraph;

namespace Fmdxx
{
  namespace Spectra 
  {
    class Rcu;
    class Chan;
  }
  
  /** @defgroup fmdxx_monitor GUI classes for reading raw data 
      @ingroup fmdxx
  */

  //____________________________________________________________________
  struct MonitorBoard  : public TObject
  {
    /** A coordinate pair */
    typedef std::pair<double,double> Coord_t;
    /** Vector of shape coordinates */
    typedef std::vector<Coord_t>     Coords_t;
    /** Map of shapes */
    typedef std::map<int, TGraph*>   Shapes_t;
    /** Vector of hits */ 
    typedef std::vector<TGraph*>     Hits_t;
    /** Vector of counts */ 
    typedef std::vector<unsigned int> Counts_t;
    /** Constructor 
	@param a Address of this board  
	@param c Coordinates to construct shapes from. */
    MonitorBoard(unsigned int addr, 
		 const Coords_t& c, 
		 int nlevels=-1);
    /** Destructor */
    virtual ~MonitorBoard();

    /** Clear */ 
    void Clear();
    /** Draw */ 
    void Draw(bool frame=true);
    /** Draw */ 
    void ReDraw();
    /** Fill */
    void Fill(unsigned int altro, 
	      unsigned int channel, 
	      unsigned int strip, 
	      unsigned int value, 
	      unsigned int min,
	      unsigned int max);

    /** @return @c true if this is an upper board */
    bool IsInner() const { return fAddress == 0x0 || fAddress == 0x10; }
    /** @return @c true if this is a lower board */
    bool IsLower() const { return fAddress < 0x10; }
    /** @return Identifier */ 
    char Id() const { return IsInner() ? 'I' : 'O'; }
    /** @return Maximum radius */
    double MaxR() const { return fMaxR; }
    /** Adjust the frame of the shapes, so that they are displayed
	right. 
	@param maxR Maximum radius */
    void AdjustFrame(double maxR);
    /** Set frame title */ 
    void SetFrameTitle(const char* title);
    
    /** @return Opening angle of modules */
    static double Theta(char c) { return (c == 'I' ? 36 : 18) / 2; }
    /** @return Number of modules */
    static unsigned int Modules(char c) { return c == 'I' ? 5 : 10; }
    /** @return The minimum radius */ 
    static double LowR(char c) { return c == 'I' ? 4.3 : 15.6; }
    /** @return The maximum radius */ 
    static double HighR(char c) { return c == 'I' ? 17.2 : 28.0; }
    /** @return The number of strips */
    static unsigned int Strips(char c) { return c == 'I' ? 512 : 256; }
  protected:
    /** Check that a module shape exists, and if not, create it. 
	@param altro   Altro address
	@param channel Channel address. 
	@return @c true if a new module was made */
    bool CheckModule(unsigned int altro, unsigned int channel);
    /** Convert ALTRO/Channel pair to module number. 
	@param altro   Altro address
	@param channel Channel address
	@return Corresponding module number  */
    unsigned Channel2Module(unsigned int altro, unsigned int channel) const;
    /** Address of the board */
    unsigned int    fAddress;
    /** Reference to coordinates */
    const Coords_t& fCoords;
    /** List of shapes */
    Shapes_t        fShapes;
    /** The maxium radius. */
    double          fMaxR;
    /** Graph of hits */
    Hits_t          fHits;
    /** Counts */ 
    Counts_t        fCounts;
    /** The frame */
    TH2F            fFrame;	
    /** Whether shapes hav been drawn */
    bool fShapesDrawn;
  };

  //____________________________________________________________________
  /** @struct MonitorDetector 
      @brief Display of full detector
      @ingroup fmdxx_monitor
  */
  struct MonitorRcu : public TObject
  {
  public:
    /** A coordinate pair */
    typedef std::pair<double,double> Coord_t;
    /** Vector of shape coordinates */
    typedef std::vector<Coord_t>     Coords_t;

    /** Constructor */
    MonitorRcu(UInt_t rcu, UShort_t levels=10);

    /** get the rcu number */
    UInt_t RcuNo() const { return fRcu; }
    /** Clear */ 
    void Clear();
    /** Draw */ 
    void Draw(bool frame=true);
    /** Draw */ 
    void ReDraw();
    /** Fill */
    void Fill(unsigned int board, 
	      unsigned int altro, 
	      unsigned int channel, 
	      unsigned int strip, 
	      unsigned int value, 
	      unsigned int min,
	      unsigned int max);
  protected:
    /** Make histograms, etc. for board. 
	@param board Board number 
	@return  */
    MonitorBoard* CheckBoard(unsigned int board);
    /** Create shapes for a detector */ 
    const Coords_t& GetShape(unsigned int board);
    /** Rcu number */
    UInt_t fRcu;
    /** Number of levels */
    UShort_t fLevels;
    /** List of boards */ 
    typedef std::map<unsigned int, MonitorBoard*> Boards_t;
    /** List of boards */
    Boards_t fBoards;
    /** Shape of an inner module */
    Coords_t fInner;
    /** Shape of an outer module */
    Coords_t fOuter;
  };

  //____________________________________________________________________
  /** @struct MonitorTree 
      @brief Special LinkedTree class for the monitor 
      @ingroup fmdxx_monitor
  */
  struct MonitorTree : public RcuGui::MonitorTree, public MakerUtil 
  {
    /** Constructor 
	@param top Reference to cache 
	@param f   Parent frame 
	@param w   Width
	@param h   Height  */
    MonitorTree(TGCompositeFrame& f, UInt_t w=800, UInt_t h=600);
    /** Check if we have all the stuff we need for this channel (that
	is, a cache object and an entry in the tree).  If not, we make
	it (recursively).
	@param board   Board number
	@param chip    Chip number
	@param channel Channel number
	@return @c true */
    bool MakeChannel(unsigned int rcu, 
		     unsigned int board, 
		     unsigned int chip, 
		     unsigned int channel);
    /** Check if we have everything we need for this time bin (that
	is, a cache object and an entry in the tree).  If not we make
	the needed stuff. 
	@param board   Board number  
	@param chip    Chip number   
	@param channel Channel number
	@param t       Timebin
	@return @c true on success, @c false otherwise */
    bool MakeTimebin(unsigned int rcu, 
		     unsigned int board, 
		     unsigned int chip, 
		     unsigned int channel, 
		     unsigned int t);
    /** Fill value into cache
	@param board   Board number  
	@param chip    Chip number   
	@param channel Channel number

	@param t       Timebin
	@param adc     ADC value */
    void Fill(unsigned int rcu, 
	      unsigned int board, 
	      unsigned int chip, 
	      unsigned int channel, 
	      unsigned int t,
	      unsigned int adc);
    /** Handle selection changed */
    void HandleSelect();
    /** Clear it */ 
    void Clear();
    /** Reset it */ 
    void Reset();
    /** Update list at every event, if needed */ 
    void UpdateList();
    /** Set ADC range */ 
    void SetAdcRange(unsigned min, unsigned max) 
    {
      fMinAdc = min;
      fMaxAdc = max;
    }
    void SetLevels(Int_t lvl=-1) { fLevels = lvl; }
    void SetOverSample(unsigned int over);
    unsigned GetMinAdc() const { return fMinAdc; }
    unsigned GetMaxAdc() const { return fMaxAdc; }
    int      GetLevels() const { return fLevels; }
  protected:
    /** Reference to cache */ 
    Spectra::Top   fTop;
    /** Pointer to current channel information */
    Spectra::Chan* fCurrent;

    /** Draw the detector */
    void DrawDetector();
    /** List of boards */ 
    typedef std::map<unsigned int, MonitorRcu*> Rcus_t;
    Rcus_t fRcus;
    MonitorRcu* CheckRcu(unsigned int rcu);
    /** Selection changged */
    bool fSelectionChanged;
    /** Cache of entry */
    TGListTreeItem* fCachedEntry;
    /** Min ADC for cut */ 
    unsigned int fMinAdc;
    /** Max ADC for cut */ 
    unsigned int fMaxAdc;
    /** Number of levels */
    int fLevels;
    
  };

  //____________________________________________________________________
  /** @struct MonitorFrame
      @brief Special frame class for the monitor 
      @ingroup fmdxx_monitor
  */
  struct MonitorFrame : public RcuGui::MonitorFrame 
  {
    MonitorFrame(TGCompositeFrame& f);
    virtual ~MonitorFrame() {}
    void HandleMin();
    void HandleMax();
    void HandleLevels();
    void SetFmdTree(MonitorTree& tree);
  public:
    TGLayoutHints fExtraHints;
    TGGroupFrame  fExtraFrame;
    RcuGui::LabeledIntEntry fMinAdc;
    RcuGui::LabeledIntEntry fMaxAdc;
    RcuGui::LabeledIntEntry fLevels;
    MonitorTree* fFmdTree;
    ClassDef(MonitorFrame,0);
  };
}

#endif

  
