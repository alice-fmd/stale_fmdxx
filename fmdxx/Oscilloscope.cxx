//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <fmdxx/Oscilloscope.h>
#include <rcugui/ConnectionDialog.h>
#include <rcugui/SourceDialog.h>
#include <readraw/Reader.h>
#include <readraw/Event.h>
#include <readraw/EventId.h>
#include <readraw/Header.h>
#include <rcudata/ProgressMeter.h>
#include <rcudata/Channel.h>
#include <rcugui/rcuMonitor.xpm>
#ifdef HAVE_LOWLEVEL
# include <rcuxx/Rcu.h>
# include <rcuxx/Altro.h>
#endif
#include <TTree.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TF1.h>
#include <TSystem.h>
#include <TGFrame.h>
#include <TGMenu.h>
#include <TGListBox.h>
#include <TGButtonGroup.h>
#include <TGCanvas.h>
#include <TGButton.h>
#include <TGListView.h>
#include <TGListTree.h>
#include <TGStatusBar.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGStatusBar.h>
#include <TApplication.h>
#include <TGClient.h>
#include <KeySymbols.h>
#include <iostream>

namespace Fmdxx 
{
  namespace Osc 
  {
    //________________________________________________________________
    void Rcu::Clear() 
    {
      for (BoardList::iterator i = fBoards.begin(); i != fBoard.end(); ++i){
	delete (*i)->second;
	i->second = 0;
      }
      fBoards.clear();
    }
    Board* Rcu::Find(UShort_t id) 
    {
      Board* ret = 0;
      BoardList::iterator i = fBoards.find(id);
      if  (i == fBoards.end()) ret = fBoards[id] = new Board(id);
      return ret;
    }
    void Rcu::Reset(UShort_t n) 
    {
      for (BoardList::iterator i = fBoards.begin(); i != fBoard.end(); ++i)
	i->second->Reset(n);
    }
    void Rcu::End(UShort_t n) 
    {
      for (BoardList::iterator i = fBoards.begin(); i != fBoard.end(); ++i)
	i->second->End(n);
    }
    void Rcu::Fill(UShort_t b, UShort_t a, UShort_t c, UShort_t adc)
    {
      Board* bb = Find(b);
      if (!bb) return;
      bb->Fill(a, c, adc);
    }
    //________________________________________________________________
    void Board::Clear() 
    {
      for (ChipList::iterator i = fChips.begin(); i != fChip.end(); ++i){
	delete (*i)->second;
	i->second = 0;
      }
      fChips.clear();
    }
    Chip* Board::Find(UShort_t id) 
    {
      Chip* ret = 0;
      ChipList::iterator i = fChips.find(id);
      if  (i == fChips.end()) ret = fChips[id] = new Chip(id);
      return ret;
    }
    void Board::Reset(UShort_t n) 
    {
      for (ChipList::iterator i = fChips.begin(); i != fChip.end(); ++i)
	i->second->Reset(n);
    }
    void Board::End(UShort_t n) 
    {
      for (ChipList::iterator i = fChips.begin(); i != fChip.end(); ++i)
	i->second->End(n);
    }
    void Board::Fill(UShort_t a, UShort_t c, UShort_t adc)
    {
      Chip* aa = Find(a);
      if (!aa) return;
      aa->Fill(a, c, adc);
    }
    //________________________________________________________________
    void Chip::Clear() 
    {
      for (ChanList::iterator i = fChans.begin(); i != fChan.end(); ++i){
	delete (*i)->second;
	i->second = 0;
      }
      fChans.clear();
    }
    Chan* Chip::Find(UShort_t id) 
    {
      Chan* ret = 0;
      ChanList::iterator i = fChans.find(id);
      if  (i == fChans.end()) ret = fChans[id] = new Chan(id);
      return ret;
    }
    void Chip::Reset(UShort_t n) 
    {
      for (ChanList::iterator i = fChans.begin(); i != fChan.end(); ++i)
	i->second->Reset(n);
    }
    void Chip::End(UShort_t n) 
    {
      for (ChanList::iterator i = fChans.begin(); i != fChan.end(); ++i)
	i->second->End(n);
    }
    void Chip::Fill(UShort_t c, UShort_t adc)
    {
      Chan* cc = Find(c);
      if (!cc) return;
      cc->Fill(adc);
    }
    //________________________________________________________________
    Chan::Chan(Int_t id) 
      : fId(id), fSumw(0), fSumw2(0), fN(0)
    {}
    void Chan::Reset(Int_t n) 
    {
      fSumw  = 0;
      fSumw2 = 0;
      fN     = 0;
      fGraph.Set(n);
      for (int i = 0; i < n; i++) {
	fGraph.SetPoint(i, 0, 0);
	fGraph,SetPointError(i, 0, 0);
      }
    }
    void Chan::Clear() 
    {
      fSumw  = 0;
      fSumw2 = 0;
      fN     = 0;
    }
    void Chan::End(Int_t n) 
    {
      double x = fGraph.GetX()[n];
      fGraph.SetPoint(n, x, fSumw / fN);
      fGraph.SetPointError(n, 0, TMath::Sqrt(fSumw2 / fN) / fN);
      fSumw  = 0;
      fSumw2 = 0;
      fN     = 0;
    }
    void Chan::Fill(UShort_t adc) 
    {
      fSumw  += adc;
      fSumw2 += adc * adc;
      fN++;
    }
  }
      
  OscilloscopeFrame::OscilloscopeFrame(TGCompositeFrame* parent) 
    : fMother(parent),
      fTopHints(kLHintsExpandX|kLHintsTop, 3, 3, 3, 3),
      fTopCont(&fMother),
      // fTopLeft(&fTopCont),
      // Operations
      fOperHints(kLHintsExpandX|kLHintsExpandY, 0, 2, 0, 0),
      fOper(&fTopCont),
      fRun(&fOper,      "Run",                         kHorizontalFrame),
      fStop(&fRun,      "Start",                       kStart),
      fStop(&fRun,      "Stop",                        kStop),
      // Misc.
      fMiscHints(kLHintsExpandX|kLHintsExpandY, 2, 0, 0, 0),
      fMisc(&fTopCont,  "Misc.",                       kVerticalFrame),
      fOverSample(fMisc,  "Over-samplling", 0, 256/2,  kHorizontalFrame),
      fPulser(fMisc,      "Pulser",         0, 256,    kHorizontalFrame),
      fStrip(fMisc,       "Strip",          0, 127,    kHorizontalFrame),
      fEvents(fMisc,      "Events",         0, INT_MAX,kHorizontalFrame),
      // Hold values
      fHoldOptHints(kLHintsExpandX, 0, 2, 0, 0),
      fHoldOpt(&fMisc),
      fNHold(fHoldOpt,     "Samples",        0, INT_MAX,kHorizontalFrame),
      fHoldRangeHints(kLHintsExpandX, 0, 2, 0, 0),
      fHoldRange(&fMisc),
      // Bottom
      fBottomHints(kLHintsExpandX|kLHintsExpandY, 3, 3, 0, 3),
      fBottom(&fMother),
      // List tree
      fViewHints(kLHintsExpandX, 0, 0, 0, 0),
      fView(&fBottom, 150, 546),
      fList(&fView, kHorizontalFrame),
      fIcon(gClient->GetPicture("h1_t.xpm")),
      // Canvas
      fEmCanvasHints(kLHintsRight|kLHintsTop, 0, 3, 7, 0)
      fEmCanvas(0,&fBottom,600,600),
      fCanvas("canvas", 10, 10, fEmCanvas.GetCanvasWindowId()),
      // Status
      fStatusHints(kLHintsExpandX|kLHintsBottom, 3, 3, 0, 3),
      fStatus(&fMother,  "Status",                  kHorizontalFrame),
      fLast(fStatus,     "Last event",  0, INT_MAX, kHorizontalFrame),
      fCounts(fStatus,   "Events read", 0, INT_MAX, kHorizontalFrame),
      fSourceHints(kLHintsExpandX),
      fSourceFrame(&fStatus),
      fSourceLabel(&fSourceFrame, "Source"),
      fSource(&fSourceFrame,"          "),
      // Other stuff
      fCurrentHisto(0),
      fCurrentEntry(0),
      fCounter(0),
      fFreq(1),
      fIsStop(false),
      fNeedUpdate(false)
  {
    parent->AddFrame(&fMother, new TGLayoutHints(kLHintsExpandX));

    // Top Frame
    fMother.AddFrame(&fTopCont, &fTopHints);
    // fTopCont.AddFrame(&fTopLeft, new TGLayoutHints(kLHintsTop, 3, 3, 3, 3));
    
    // Operations frame 
    fTopCont.AddFrame(&fOper, &fOperHints);

    // Control buttons
    fOper.AddFrame(&fRun, &fOperHints);
    fStop.SetEnabled(kFALSE);
    fStart.SetEnabled(kTRUE);
    fRun.Connect("Clicked(Int_t)", "Fmdxx::OscilloscopeFrame",
		 this, "HandleButtons(int)");
    fHoldRange.SetRange(0, 0xffff);
    
    // Bottom part
    fMother.AddFrame(&fBottom, &fBottomHints);

    // List view
    fSelect.AddFrame(&fView, &fViewHints);
    fList.Connect("Clicked(TGListTreeItem*,Int_t)", "Fmdxx::OscilloscopeFrame",
		  this, "HandleEntry(TGListTreeItem*,Int_t)");
    fList.Connect("KeyPressed(TGFame*,ULong_t,ULong_t)",
		  "Fmdxx::OscilloscopeFrame", this,
		  "HandleKey(TGListTreeItem*,UInt_t,UInt_t)");
    fList.Connect("ReturnPressed(TGListTreeItem*)",
		  "Fmdxx::OscilloscopeFrame", this,
		  "HandleReturn(TGListTreeItem*)");

    // embedded canvas
    fEmCanvas.AdoptCanvas(&fCanvas);
    fCanvas.SetBorderMode(0);
    fCanvas.SetBorderSize(0);
    fCanvas.SetFillColor(0);
    fBottom.AddFrame(&fEmCanvas, &fEmCanvasHints);
    fEmCanvas.Resize(600, 600);
    fView.SetWidth(200);

    // Status
    fMother.AddFrame(&fStatus,           &fStatusHints);
    fStatus.AddFrame(&fSourceFrame,      &fSourceHints);
    fSource.SetEnabled(kFALSE);
    fSourceFrame.AddFrame(&fSourceLabel, &fSourceHints);
    fSourceFrame.AddFrame(&fSource,      &fSourceHints);
  }
  //__________________________________________________________________
  OscilloscopeFrame::~OscilloscopeFrame()
  {}
  //__________________________________________________________________
  void 
  OscilloscopeFrame::HandleButtons(int id)
  {}
  
  //____________________________________________________________________
  void
  OscilloscopeFrame::HandleReturn(TGListTreeItem * f)
  {

    if (!f) { 
      fList.SetSelected(0);
      return;
    }
    fList.ToggleItem(f);
    gClient->NeedRedraw(&fList);
  }
  //____________________________________________________________________
  void
  OscilloscopeFrame::HandleKey(TGListTreeItem * f, UInt_t keysym, UInt_t mask)
  {
    if (!f) { 
      fList.SetSelected(0);
      return;
    }
    TGListTreeItem* next = 0;
    // TGListTreeItem* old = 0;
    switch (keysym) {
    case kKey_Up:
      next = f->GetPrevSibling();
      if (!next) { 
	next = f->GetParent();
	if (next) fList.CloseItem(next);
      }
      break;
    case kKey_Down:
      next = f->GetNextSibling();
      if (!next && f->GetParent()) {
	next = f->GetParent()->GetNextSibling();
	fList.CloseItem(f->GetParent());
      }
      break;
    case kKey_Left:
      next = f->GetParent();
      if (next) fList.CloseItem(next);
      break;
    case kKey_Right:
      next = f->GetFirstChild();
      if (next) fList.OpenItem(f);
      break;
    case kKey_PageUp:
      fList.PageUp(kTRUE);
      next = fList.GetSelected();
      break;
    case kKey_PageDown:
      fList.PageDown(kTRUE);
      next = fList.GetSelected();
      break;
    }
    if (next) gClient->NeedRedraw(&fList);
    if (next && next != f) {
      fList.ClearHighlighted();
      fList.SetSelected(next);
      HandleEntry(next,0);
    }
  }

  //____________________________________________________________________
  void
  OscilloscopeFrame::HandleEntry(TGListTreeItem* entry, Int_t id) 
  {
    TGraph* old = fCurrentGraph;
    if (entry) {
      if (!entry->GetUserData()) return;
      fCurrentGraph = static_cast<TGraph*>(entry->GetUserData());
    }
    if (old != fCurrentGraph) { 
      fCanvas.cd();
      fCanvas.Clear();
      fCurrentGraph->Draw("APL");
    }
    HandleDraw();
  }

  //____________________________________________________________________
  void
  OscilloScopeFrame::HandleDraw()
  {
    if (!fCurrentGraph) return;
    fCanvas.Modified();
    fCanvas.Update();
    fCanvas.cd();
  }
