// -*- mode: C++ -*-
#ifndef FMDXX_OSCILLOSCOPE
#define FMDXX_OSCILLOSCOPE
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef RCUGUI_LABELEDNUMBER_H
# include <rcugui/LabeledNumber.h>
#endif
#ifndef ROOT_TTimer
# include <TTimer.h>
#endif
#ifndef ROOT_TGMenu
# include <TGMenu.h>
#endif
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif
#ifndef ROOT_TGListTree
# include <TGListTree.h>
#endif
#ifndef ROOT_TRootEmbeddedCanvas
# include <TRootEmbeddedCanvas.h>
#endif
#ifndef ROOT_TCanvas
# include <TCanvas.h>
#endif
#ifndef ROOT_TGTextEntry
# include <TGTextEntry.h>
#endif
class TH1;
class TGStatusBar;
class TGCanvas;
class TGPicture;
namespace Rcuxx
{
  class Rcu;
  class Fmd;
  class Altro;
}
namespace ReadRaw
{
  class Reader;
}

namespace RcuData
{
  class RawVisitor;
}

namespace Fmdxx
{
  namespace Osc 
  {
    struct Board;
    struct Chip;
    struct Chan;
    struct Rcu 
    {
      Rcu() {}
      ~Rcu() { Clear(); }
      Board* Find(UShort_t id);
      void Reset(UShort_t n);
      void Clear();
      void End(UShort_t n);
      void Fill(UShort_t b, UShort_t a, UShort_t c);
    protected:
      typedef std::map<UShort_t, Board*> BoardList;
      BoardList fBoards;
    };
    struct Board 
    {
      Board(Int_t id)  : fId(id) {}
      ~Board() { Clear(); }
      Chip* Find(UShort_t id);
      void Reset(UShort_t n);
      void Clear();
      void End(UShort_t n);
      void Fill(UShort_t a, UShort_t c);
    protected:
      UShort_t fId;
      typedef std::map<UShort_t, Chip*> BoardList;
      ChipList fChips;
    };
    struct Chip
    {
      Chip(Int_t id) : fId(id) {}
      ~Chip() { Clear(); }
      Chan* Find(UShort_t id);
      void Reset(UShort_t n);
      void Clear();
      void End(UShort_t n);
      void Fill(UShort_t a, UShort_t c);
    protected:
      UShort_t fId;
      typedef std::map<UShort_t, Chan*> BoardList;
      ChanList fChans;
    };
    struct Chan 
    {
      Chan(Int_t id)  : fId(id) {}
      ~Chan() { Clear(); }
      TGraph* GetGraph();
      void Reset(UShort_t n);
      void Clear();
      void End(UShort_t n);
      void Fill(UShort_t a, UShort_t c);
    protected:
      UShort_t fId;
      TH1I          fHist;
      TGraphErrors  fGraph;
    };
      
  class OscilloscopeFrame 
  {
  public:
    enum {
      kStart, 
      kStop
    };
    /** Constructor */
    OscilloscopeFrame(TGCompositeFrame* parent);
    /** Destructor */
    virtual ~OscilloscopeFrame();
    /** Handle button with identifer @a id 
	@param id Handle button with @a id */
    virtual void HandleButtons(int id);
    /** Handle Entry */
    void HandleEntry(TGListTreeItem*,Int_t);
    /** Handle key */
    void HandleKey(TGListTreeItem*,UInt_t,UInt_t);
    /** Handle return */
    void HandleReturn(TGListTreeItem*);
    
  protected:
    /** RCU interface */ 
    Rcuxx::Rcu*   fRcu;
    /** FMD interface */
    Rcuxx::Fmd*   fFmd;
    /** ALTRO interface */
    Rcuxx::Altro* fAltro;
    
    /** Hints for mother frame */
    TGLayoutHints        fMotherHints;
    /** Main frame */
    TGVerticalFrame      fMother;

    /** Hints for mother frame */
    TGLayoutHints        fTopHints;
    /** Main frame */
    TGHorizontalFrame    fTopCont;
      
    /** Hints for operations frame */
    TGLayoutHints        fOperHints;
    /** Operations */
    TGVerticalFrame      fOper;

    // Operations
    /** Run control */
    TGButtonGroup        fRun;
    /** Stop processing */
    TGTextButton         fStart;
    /** Stop processing */
    TGTextButton         fStop;

    // Misc.
    /** Hints for misc frame */
    TGLayoutHints          fMiscHints;
    /** Misc frame */
    TGGroupFrame           fMisc;
    /** Over sampling rate */
    RcuGui::LabeledIntNumber fOverSample;
    /** Pulser value */
    RcuGui::LabeledIntNumber fPulser;
    /** Strip number */ 
    RcuGui::LabeledIntNumber fEvents;
    /** Layout hints for hold options */
    TGLayoutHints            fHoldOptHints;
    /** Hold options */
    TGHorizontalFrame        fHoldOpt;
    /** Number of points */
    RcuGui::LabelIntNumber   fNHold;
    /** Layout hints for slider */
    TGLayoutHints            fHoldRangeHints;
    /** Slider setting range on hold */
    TGHSlider                fHoldRange;

    // Bottom frame 
    /** Hints for bottom frame */
    TGLayoutHints        fBottomHints;
    /** Bottom frame */
    TGHorizontalFrame    fBottom;

    // Select tree
    /** Hints for list view */
    TGLayoutHints        fViewHints;
    /** View */
    TGCanvas             fView;
    /** List view */
    TGListTree           fList;
    /** Icon used */
    const TGPicture*     fIcon;

    // Canvas
    /** Embeded canvas hints */
    TGLayoutHints        fEmCanvasHints;
    /** Embeded canvas */
    TRootEmbeddedCanvas  fEmCanvas;
    /** Actual canvas */
    TCanvas              fCanvas;

    // Status
    /** Hints */
    TGLayoutHints        fStatusHints;
    /** Select what to show */
    TGGroupFrame         fStatus;
    /** LAst event read */
    LabeledIntView       fLast;
    /** Counter of read events */
    LabeledIntView       fCounts;
    /** Layout in source frame */
    TGLayoutHints        fSourceHints;
    /** Frame for source */
    TGHorizontalFrame    fSourceFrame;
    /** Label for source */
    TGLabel              fSourceLabel;
    /** View of source */
    TGTextEntry          fSource;

    /** Current histogram */
    TH1* fCurrentHisto;
    /** Current histogram */
    TGListTreeItem* fCurrentEntry;
    /** Other stuff */
    size_t               fCounter;
    /** Other stuff */
    size_t               fFreq;
    /** Other stuff */
    bool                 fIsStop;
    /** Other stuff */
    bool                 fNeedUpdate;

  };
}

  
    

