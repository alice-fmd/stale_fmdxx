//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    PedestalMaker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:19:52 2006
    @brief   
    @ingroup fmdxx_data        
*/
#include <fmdxx/PedestalMaker.h>
#include <rcudata/Channel.h>
#include <rcudata/DebugGuard.h>
#include "Spectra.h"
#include <TSystem.h>
#include <TFile.h>
#include <TError.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TObjArray.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TF1.h>
#include <TParameter.h>
#include <iostream>
#include <iomanip>
#include <fstream>
namespace 
{
  bool fDebug = false;
}

ClassImp(Fmdxx::PedestalMaker);

namespace Fmdxx 
{
  //____________________________________________________________________
  void PedestalMaker::SetOverSample(unsigned int over)
  {
    Spectra::Strip::fgChosenSample = (over == 1 ? 0 : 
				      over == 2 ? 1 : 
				      over == 4 ? 2 : 0);      
    MakerUtil::SetOverSample(over);
  }

  //____________________________________________________________________
  PedestalMaker::PedestalMaker()
    : fDoFit(false), 
      fCSV(0)
  {
    DGUARD("ctor");
  }

  //____________________________________________________________________
  void
  PedestalMaker::ProcessSpectra() 
  {
    DGUARD("processing all spectra, and writing to peds.csv");
    std::string outname("peds.csv");
    if (fOutput) { 
      outname = fOutput->GetName();
      size_t dotroot = outname.find(".root");
      if (dotroot != std::string::npos)
	outname.erase(dotroot, 5);
      outname += ".csv";
    }
    fCSV = new std::ofstream(outname.c_str());
    if (!fCSV) return;
    //*fCSV << "Board,Chip,Channel,Strip,Sample,Mean,RMS,Mu,Sigma" << std::endl;
    SpectrumMaker::ProcessSpectra();
    fCSV->close();
  }
  
  //____________________________________________________________________
  void
  PedestalMaker::ProcessSpectrum(TH1*         spectra, 
				 unsigned int rcu,
				 unsigned int board, 
				 unsigned int chip, 
				 unsigned int chan, 
				 unsigned int strip, 
				 unsigned int sample)
  {
    DGUARD("processing all spectrum for 0x%02x/0x%x/0x%02x/%03[%d]",
	   board, chip, chan, strip, sample);
    static int lastRcu   = -1;
    static int lastBoard = -1;
    static int lastChip  = -1;
    static int lastChan  = -1;
    static int lastStrip = -1;
    int timebin = fOffset + fExtraOffset + strip * fOverSample + sample;

    *fCSV << board << "," << chip << "," << chan << "," << timebin << ","
	  << std::flush;
#if 0
    // Check for new rcu 
    if (lastRcu < 0 || rcu != lastRcu) {
      *fCSV << rcu << ',';
      lastRcu   = rcu;
      lastBoard = -1;
    }
    else *fCSV << ',';

    // Check for new board 
    if (lastBoard < 0 || board != lastBoard) {
      *fCSV << board << ',';
      lastBoard = board;
      lastChip = -1;
    }
    else *fCSV << ',';

    // Check for new chip
    if (lastChip < 0 || chip != lastChip) {
      *fCSV << chip << ',';
      lastChip = chip;
      lastChan = -1;
    }
    else *fCSV << ',';

    // Check for new channel
    if (lastChan < 0 || chan != lastChan) {
      *fCSV << chan << ',';
      lastChan  = chan;
      lastStrip = -1;
    }
    else *fCSV << ',';

    // Check for new strip
    if (lastStrip < 0 || strip != lastStrip) {
      *fCSV << strip << ',';
      lastStrip = strip;
    }
    else *fCSV << ',';
    *fCSV << sample << ",";
#endif
    
    // Write out the result 
    Float_t mean = spectra->GetMean();
    Float_t rms  = spectra->GetRMS();
    *fCSV << mean << ',' << rms << ',';

    if (fDoFit) {
      spectra->Fit("gaus", "Q0");
      TF1* gaus = spectra->GetFunction("gaus");
      if (gaus) 
	*fCSV << gaus->GetParameter(1) << ',' << gaus->GetParameter(2);
      else 
	*fCSV << ',';
    }
    else *fCSV << ',';
    *fCSV << std::endl;
  }
}



//____________________________________________________________________
//
// EOF
//

  
