// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    PedestalMaker.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Make pedestals 
    @ingroup fmdxx_data    
*/
#ifndef FMDXX_DATA_PEDESTALMAKER
#define FMDXX_DATA_PEDESTALMAKER
#ifndef FMDXXX_SPECTRUMMAKER
# include <fmdxx/SpectrumMaker.h>
#endif
#ifndef FMDXX_MAKERUTIL
# include <fmdxx/MakerUtil.h>
#endif
#include <iosfwd>

class TFile;
class TH1;
class TH1D;
class TH1F;
class TObjArray;
class TLatex;
class TCanvas;
class TTree;

namespace Fmdxx 
{
  /** @class PedestalMaker 
      @brief Class to make pedestals 
      @ingroup fmdxx
  */
  class PedestalMaker : public SpectrumMaker
  {
  public:
    /** Constructor */
    PedestalMaker();
    /** Use fits 
	@param on If @c true, do fits */
    void SetFit(Bool_t on=kTRUE) { fDoFit = on; }
    void SetOverSample(unsigned int over);
  protected:
    virtual void ProcessSpectra();
    /** Process on ADC spectra 
	@param spec     The spectra
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number
	@param strip    Strip number 
        @param sample   Sample number. */
    virtual void ProcessSpectrum(TH1*         spec, 
				 unsigned int rcu, 
				 unsigned int board, 
				 unsigned int chip, 
				 unsigned int channel, 
				 unsigned int strip,
				 unsigned int sample);
    /** Whether to fit or not */
    Bool_t   fDoFit;
    /** Output file */
    std::ofstream* fCSV;
    
    ClassDef(PedestalMaker,0);
  };
}

#endif
//
// EOF
//

