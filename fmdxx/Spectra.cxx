//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Spectra.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup fmdxx_spectra
*/
#include "fmdxx/Spectra.h"
#include <rcudata/ProgressMeter.h>
#include <TObjArray.h>
#include <TDirectory.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <iostream>
#include <iomanip>
#include <cassert>

namespace 
{
  RcuData::ProgressMeter fgWriteMeter;
}

namespace Fmdxx 
{
  namespace Spectra
  {    
    //==================================================================
    Top::Top(unsigned int id) 
      : Base_t(id)
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetTitle(Form("RCU %d", id));
      fSummary->SetFillColor(6);
    }
    //________________________________________________________________
    const char* Top::GetName() const 
    {
      return Form("top_%01d", fId);
    }
    //________________________________________________________________
    Top::Elem_t* Top::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Top::WriteOut() 
    {
      fgWriteMeter.Reset(Count(),"Writing");
      TDirectory* savDir = gDirectory;
      for (Base_t::Iter_t i = Begin(); i != End(); ++i) 
	WriteElem(i->second);
      if (fSummary) fSummary->Write();
      std::cout << std::endl;
      if (savDir) savDir->cd();
    }
    //________________________________________________________________
    void Top::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Top::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Top::Fill(unsigned int rcu, 
		   unsigned int board,   
		   unsigned int chip, 
		   unsigned int channel, 
		   unsigned int strip, 
		   unsigned int s,       
		   unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Rcu* b = GetOrAdd(rcu);
      if (!b) { 
	std::cerr << "Didn' get board " << board << std::endl;
	return;
      }
      b->Fill(board, chip, channel, strip, s, adc);
    }
    //==================================================================
    Rcu::Rcu(unsigned int id, Top& top) 
      : Base_t(id), fMother(&top)
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetTitle(Form("RCU %d", id));
      fSummary->SetFillColor(6);
    }
    //________________________________________________________________
    const char* Rcu::GetName() const 
    {
      return Form("rcu_%01d", fId);
    }
    //________________________________________________________________
    Rcu::Elem_t* Rcu::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Rcu::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Rcu::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Rcu::Fill(unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Board* b = GetOrAdd(board);
      if (!b) { 
	std::cerr << "Didn' get board " << board << std::endl;
	return;
      }
      b->Fill(chip, channel, strip, s, adc);
    }

    //==================================================================
    Board::Board(unsigned int id, Rcu& rcu) 
      : Base_t(id), 
	fRcu(&rcu)
    {
      MakeSummary(1024, -.5, -1023.5);
      fSummary->SetTitle(Form("RCU %d, Board %2d", RcuNo(), id));
      fSummary->SetFillColor(5);
    }
    //________________________________________________________________
    const char* Board::GetName() const 
    {
      return Form("board_%02d", fId);
    }
    //________________________________________________________________
    Board::Elem_t* Board::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Board::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Board::CountElem(Elem_t* e) const { return e->Count(); }
    
    //________________________________________________________________
    void Board::Fill(unsigned int chip,  unsigned int channel, 
		     unsigned int strip, unsigned int s,     
		     unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Chip* c = GetOrAdd(chip);
      if (!c) return;
      c->Fill(channel, strip, s, adc);
    }
    //==================================================================
    Chip::Chip(unsigned int id, Board& board) 
      : Base_t(id), 
	fBoard(&board)
    {
      MakeSummary(1024, -.5, -1023.5);
      fSummary->SetTitle(Form("RCU %d, Board %2d, Chip %d", 
			      RcuNo(), BoardNo(), id));
      fSummary->SetFillColor(4);
    }
    //________________________________________________________________
    const char* Chip::GetName() const 
    {
      return Form("chip_%01d", fId);
    }
    //________________________________________________________________
    Chip::Elem_t* Chip::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chip::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chip::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chip::Fill(unsigned int channel, unsigned int strip, 
		    unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Chan* c = GetOrAdd(channel);
      if (!c) return;
      c->Fill(strip, s, adc);
    }

    //==================================================================
    Chan::Chan(unsigned int id, Chip& chip) 
      : Base_t(id), 
	fChip(&chip)
    {
      // MakeSummary(1024, -.5, -1023.5);
      // MakeSummary(128, -.5, -127.5);
      if (fSummary) delete fSummary;
      fSummary = new TH1I(GetName(), "Mean & RMS", 128, -.5, 127.5);
      fSummary->SetStats(kFALSE);
      fSummary->SetXTitle("strip #");
      fSummary->SetYTitle("#bar{ADC}");

      fSummary->SetTitle(Form("RCU %d, Board %2d, Chip %d, Channel %2d", 
			      RcuNo(), BoardNo(), ChipNo(), id));
      fSummary->SetFillColor(3);
      fSummary->SetFillStyle(3001);
      fSummary->SetDirectory(0);
      fSummary->SetDrawOption("BAR");
    }

    //________________________________________________________________
    const char* Chan::GetName() const 
    {
      return Form("channel_%02d", fId);
    }
    //________________________________________________________________
    Chan::Elem_t* Chan::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chan::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chan::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chan::Fill(unsigned int strip, 
		    unsigned int s,       unsigned int adc) 
    {
      // if (fSummary) fSummary->Fill(adc);
      Spectra::Strip* str = GetOrAdd(strip);
      if (!str) return;
      str->Fill(s, adc);
      TH1* h = str->GetSummary();
      fSummary->SetBinContent(strip+1, h->GetMean());
      fSummary->SetBinError(strip+1, h->GetRMS());
    }

    //==================================================================
    unsigned int Strip::fgChosenSample = 2;

    //________________________________________________________________
    Strip::Strip(unsigned int id, Chan& chan) 
      : Base_t(id), 
	fChan(&chan)
    {
      MakeSummary(1024, -.5, -1023.5);
      fSummary->SetTitle(Form("RCU %d, Board %2d, Chip %d, "
			      "Channel %2d, Strip %3d", 
			      RcuNo(), BoardNo(), ChipNo(), ChanNo(), id));
      fSummary->SetFillColor(2);
    }
    //________________________________________________________________
    const char* Strip::GetName() const 
    {
      return Form("strip_%03d", fId);
    }
    //________________________________________________________________
    size_t Strip::Count() const
    {
      size_t ret = fCont.size();
      return ret;
    }

    //________________________________________________________________
    TH1* Strip::GetOrAdd(unsigned int s) 
    {
      if (!fCont[s]) {
	size_t chan  = ChanNo();
	size_t chip  = ChipNo();
	size_t board = BoardNo();
	std::string name(Form("b%02d_a%d_c%02d_s%03d_n%02d", 
			      board, chip, chan, fId, s));
	std::string title(Form("ADC Spectrum for Board %d, Chip %d, " 
			       "Channel %2d, Strip %4d, Sample %2d", 
			       board, chip, chan, fId, s));
	// Info("Fill", "Making histogram %s", name.c_str());
	TH1I* hist = new TH1I(name.c_str(), title.c_str(), 1024, -.5, 1023.5);
	fCont[s] = hist;
	fCont[s]->SetXTitle("ADC counts");
	fCont[s]->SetYTitle("Events");
	fCont[s]->SetFillColor(2);
	fCont[s]->SetFillStyle(3001);
	fCont[s]->SetDirectory(0);
      }
      return fCont[s];
    }
    
    //________________________________________________________________
    void Strip::Fill(unsigned int s, unsigned int adc) 
    {
      // if (fSummary) fSummary->Fill(adc);
      if (fSummary && s == fgChosenSample) fSummary->Fill(adc);
      TH1* h = GetOrAdd(s);
      // std::cout << h->GetName() << " fill at " << adc << std::endl;
      h->AddBinContent(adc+1);
      h->SetEntries(fCont[s]->GetEntries()+1);
    }
    //__________________________________________________________________
    void Strip::WriteOut() 
    {
      TDirectory* savDir = gDirectory;
      std::string name(GetName());
      if (!gDirectory->GetDirectory(name.c_str())) 
	gDirectory->mkdir(name.c_str())->cd();
      else 
	gDirectory->cd(name.c_str());
      for (Iter_t i = Begin(); i != End(); ++i) {
	fgWriteMeter.Step();
	WriteElem(i->second);
      }
      if (fSummary) fSummary->Write();
      savDir->cd();
    }

    //__________________________________________________________________
    void Strip::WriteElem(Elem_t* e) 
    {
      if (!e) return;
      e->Write();
    }
    
    //__________________________________________________________________
    void
    Strip::Reset() 
    {
      Base_t::Reset();
      if (fSummary) fSummary->Reset();
    }
    //__________________________________________________________________
    void
    Strip::Clear() 
    {
      Base_t::Clear();
      if (fSummary) {
	delete fSummary;
	fSummary = 0;
      }
    }
  }
}
//
// EOF
//
