// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    SpectrumMaker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser spectrum. 
    @ingroup fmdxx_data    
*/
#include "config.h"
#include "fmdxx/SpectrumMaker.h"
#include <rcudata/Channel.h>
#include <rcudata/DebugGuard.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <TString.h>
#include <TSystem.h>
#include <TF1.h>
#include <iostream>

//____________________________________________________________________
Fmdxx::SpectrumMaker::SpectrumMaker()
  : fReader(0), 
    fOutput(0), 
    fMeter(-1, 10),
    fCurrent(0)
{
  DGUARD("ctor");
}
  
//____________________________________________________________________
void
Fmdxx::SpectrumMaker::Exec(const char* src, const char* out, 
			   int n, unsigned int skip,
			   bool tree, bool all, bool wait)
{
  DGUARD("%s, %s, %d, %d", src, out, n, skip);
  if (!Start(src, out, n, skip, tree, all, wait)) { 
    std::cout << "Start failed!" << std::endl;
    return;
  }
  std::cout << "Starting the loop" << std::endl;
  Loop();
  std::cout << "At end of loop" << std::endl;
  End();
}

//____________________________________________________________________
bool
Fmdxx::SpectrumMaker::Start(const char* src, const char* out, 
			    int n, unsigned int skip, 
			    bool tree, bool all, bool wait)
{
  DGUARD("%s, %s, %d, %d", src, out, n, skip);
  // Close old reader if any
  if (fReader) {
    delete fReader;
    fReader = 0;
  }

  // Make new reader 
  fReader = RcuData::Reader::Create(*this, src, n, skip, tree, all, wait);
  if (!fReader) {
    std::cerr << "SpectrumMaker: No reader made for input " 
	      << src << std::endl;
    return false;
  }
  // Open output if any
  if (out && out[0] != '\0') {
    fOutput = TFile::Open(out, "RECREATE");
    if (!fOutput) 
      std::cerr << "Failed to make output file " << out << std::endl;
  }  
  // Reset Progress meter.
  fMeter.Reset(n, "Event #", 10);
  return true;
}

//____________________________________________________________________
void
Fmdxx::SpectrumMaker::Loop()
{
  DGUARD("Looping over data");
  if (!fReader) {
    std::cerr << "No reader definend " << std::endl;
    return;
  }
  long ev = 0;

  // Loop over data
  while (true) {
    // Step counters if we need to 
    int  ret  = fReader->GetNextEvent();
    bool stop = false;
    switch (ret) {
    case RcuData::Reader::kData:       break;
    case RcuData::Reader::kSkip:       // Fall-through
    case RcuData::Reader::kNoData:     continue; break;
    case RcuData::Reader::kEndOfData:  // Fall-through
    case RcuData::Reader::kMaxReached: // Fall-through
    case RcuData::Reader::kError:      stop = true; break;
    }
    // Increase counter 
    ev++;
    // Reset current strip
    fCurrent = 0;
    // Step progress meter 
    fMeter.Step();
    // Get new estimate of events. 
    if (fReader->GetNumberOfEvents() != long(fMeter.N()))
      fMeter.SetN(fReader->GetNumberOfEvents());
    // If we're stopped (EOD, Max reached, error) break loop.
    if (stop) break;
  }
  std::cout << std::endl;
}

//____________________________________________________________________
void
Fmdxx::SpectrumMaker::End()
{
  DGUARD("At the end");
  ProcessSpectra();
  WriteOut();
}

//____________________________________________________________________
void
Fmdxx::SpectrumMaker::ProcessSpectra()
{
  DGUARD("Processing spectra");
  std::cout << "\nProcessing spectra ... " << std::endl;
  typedef Spectra::Top::Iter_t    rcuIter;
  typedef Spectra::Rcu::Iter_t    boardIter;
  typedef Spectra::Board::Iter_t  chipIter;
  typedef Spectra::Chip::Iter_t   channelIter;
  typedef Spectra::Chan::Iter_t   stripIter;
  typedef Spectra::Strip::Iter_t  histIter;
  
  for (rcuIter ir = fTop.Begin(); ir != fTop.End(); ++ir) {
    Spectra::Rcu* rcu = ir->second;
    for (boardIter ib = rcu->Begin(); ib != rcu->End(); ++ib) {
      Spectra::Board* board = ib->second;
      for (chipIter ia = board->Begin(); ia != board->End(); ++ia){
	Spectra::Chip* chip = ia->second;
	for (channelIter ic = chip->Begin(); ic != chip->End(); ++ic){
	  Spectra::Chan* chan = ic->second;
	  fMeter.Reset(chan->Count(), 
		       Form("Rcu %d, Board %d, Chip %d, Channel %2d",
			    rcu->Id(), board->Id(), chip->Id(), chan->Id()), 1);
	  for (stripIter is = chan->Begin(); is != chan->End(); ++is){
	    Spectra::Strip* strip = is->second;
	    for (histIter ih = strip->Begin(); ih != strip->End();++ih){
	      fMeter.Step();
	      TH1* spectrum = ih->second;
	      ProcessSpectrum(spectrum, rcu->Id(), board->Id(), chip->Id(), 
			      chan->Id(), strip->Id(), ih->first);
	    }
	  } // for Strip ...
	  std::cout <<  std::endl;
	} // for Chan ...
      }  // for Chip ...
    } // for Board ...
  }
}

//____________________________________________________________________
void
Fmdxx::SpectrumMaker::WriteOut()
{
  DGUARD("Write to disk");
  if (fReader) {
    delete fReader;
    fReader = 0;
  }
  if (fOutput) {
    fOutput->cd();
    fTop.WriteOut();
    std::cout << "Flushing to disk, please wait ... " << std::flush;
    fOutput->Write();
    fOutput->Close();
    fOutput = 0;
    std::cout << "done" << std::endl;
  }
}

  
//____________________________________________________________________
bool
Fmdxx::SpectrumMaker::GotChannel(RcuData::Channel& c, bool)
{
  DGUARD("Got a channel %p", &c);
  Spectra::Rcu*   rcu   = fTop.GetOrAdd(c.DDL());
  Spectra::Board* board = rcu->GetOrAdd(c.Board());
  Spectra::Chip*  chip  = board->GetOrAdd(c.Chip());
  fCurrent              = chip->GetOrAdd(c.ChanNo());
  return true;
}

//____________________________________________________________________
bool
Fmdxx::SpectrumMaker::GotData(RcuData::uint32_t t, RcuData::uint32_t adc) 
{
  DGUARD("Got ADC counts 0x%03x @ %4d", adc, t);
  // Check that we got the current channel 
  if (!fCurrent) { 
    std::cout << "No current object" << std::endl;
    return false;
  }
  // Skip pre-stuff
  if (t < fOffset + fExtraOffset) return false;

  // Fill in proper histogram
#if 0
  std::cout << "Filling " << std::setw(4) << t << " -> " 
	    << std::setw(2) << fCurrent->BoardNo() << "-" << std::setfill('0')
	    << std::setw(1) << fCurrent->ChipNo()  << "-"
	    << std::setw(2) << fCurrent->Id()      << "-"
	    << std::setw(3) << Timebin2Strip(t)    << "(" 
	    << std::setw(1) << Timebin2Sample(t)   << ") "
	    << std::setw(4) << adc << std::setfill(' ') << std::endl;
#endif
  fTop.Fill(fCurrent->RcuNo(), fCurrent->BoardNo(), 
	    fCurrent->ChipNo(), fCurrent->Id(), 
	    Timebin2Strip(t), Timebin2Sample(t), adc);
  return true;
}


//____________________________________________________________________
//
// EOF
//
