// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser spectrum. 
    @ingroup fmdxx_data    
*/
#ifndef FMDXX_DATA_SPECTRUMMAKER
#define FMDXX_DATA_SPECTRUMMAKER
#ifndef RCUDATA_Reader
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_READER
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_PROGRESSMETER
# include <rcudata/ProgressMeter.h>
#endif
#ifndef RCUDATA_TYPES
# include <rcudata/Types.h>
#endif
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef FMDXX_SPECTRA
# include <fmdxx/Spectra.h>
#endif
#ifndef FMDXX_MakerUtil
# include <fmdxx/MakerUtil.h>
#endif

namespace Fmdxx 
{
  /** @class SpectrumMaker
      @brief Class to find ADC spectra for each strip/sample
      @ingroup fmdxx
  */
  class SpectrumMaker : public MakerUtil, public RcuData::ChannelVisitor
  {
  public:
    /** Constructor */
    SpectrumMaker();
    /** Called when ever we get a new channel from reader.
	@param c      Channel object
	@param hasAll @c true if @a c has all information.  
	@return @c true, means continue with this channel */
    virtual bool GotChannel(RcuData::Channel& c, bool hasAll);
    /** Called when ever we get a new ADC value for the current
	channel. 
	@param t   Time bin
	@param adc ADC value 
	@return  @c true, means continue with this channel */
    virtual bool GotData(RcuData::uint32_t t, RcuData::uint32_t adc);
    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param tree  Whether to make an output tree 
	@param all   Analyse all events 
	@param wait  Wait for data */
    virtual bool Start(const char*  src, 
		       const char*  out, 
		       int          n=-1, 
		       unsigned int skip=0, 
		       bool         tree=false,
		       bool         all=true, 
		       bool         wait=true);
    /** Execute job.
	@param src   Input source
	@param out   Output file
	@param n     Max number of events to analyse (-1 means all)
	@param skip  Number of events to skip
	@param tree  Whether to make an output tree 
	@param all   Analyse all events 
	@param wait  Wait for data */
    virtual void Exec(const char*  src, 
		      const char*  out, 
		      int          n=-1, 
		      unsigned int skip=0, 
		      bool         tree=false,
		      bool         all=true, 
		      bool         wait=true);
    /** Loop over the data */
    virtual void Loop();
    /** At end of job */
    virtual void End();
  protected:
    /** Write out results */
    virtual void WriteOut();
    /** Process all strip spectra. */
    virtual void ProcessSpectra();
    /** Process a final spectra 
	@param spectra Spectra to process 
	@param board   Board #
	@param chip    Chip (ALTRO) # 
	@param chan    Channel (VA1) # 
	@param strip   Strip # 
	@param sample  Sample # */
    virtual void ProcessSpectrum(TH1*         spectra, 
				 unsigned int rcu,
				 unsigned int board, 
				 unsigned int chip, 
				 unsigned int chan, 
				 unsigned int strip, 
				 unsigned int sample) {}
    /** Reader */
    RcuData::Reader*       fReader;
    /** Output file */
    TFile*                 fOutput;
    /** Top-level of cache */
    Spectra::Top           fTop;
    /** Current channel */ 
    Spectra::Chan*         fCurrent;
    /** Progress meter */
    RcuData::ProgressMeter fMeter;
  };
}
#endif
//
// EOF
//

