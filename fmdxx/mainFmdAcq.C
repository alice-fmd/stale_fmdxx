//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
//
// ROOT based acquisition 
//
#include "config.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <fmdxx/Acq.h>
#include <TSystem.h>
#include <rcudata/Options.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuTRCFG1.h>

//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",          "\tThis help", false, false); 
  Option<bool>        vOpt('v', "version",       "\tShow version",false,false);
  Option<long>        nOpt('n', "events",        "\tNumber of events", 10);
  Option<unsigned>    cOpt('c', "boards",        "\tBit pattern of FECs",1); 
  Option<unsigned>    lOpt('l', "chips",         "\tBit pattern of ALTROs",7); 
  Option<std::string> dOpt('d', "debug",         "\tTurn on debug messages");
  Option<unsigned>    rOpt('r', "run",           "\tRun number\t");
  Option<std::string> oOpt('o', "output",        "\tOutput file name");
  Option<std::string> tOpt('t', "trigger",       "Trigger\t\t","External");
  Option<bool>        eOpt('e', "emulation",     "\tEmulation",false,false);
  Option<unsigned>    OOpt('O', "over-sampling", "Over sampling ratio", 4);
  Option<unsigned>    POpt('P', "pulser",        "\tPulser size\t", 0);
  Option<unsigned>    mOpt('m', "first-strip",   "First strip\t", 0); 
  Option<unsigned>    MOpt('M', "last-strip",    "Last strip\t", 127); 
  Option<unsigned>    BOpt('B', "shape-bias",    "Shaping bias\t");
  Option<unsigned>    VOpt('V', "shape-voltage", "VFS\t\t");
  Option<unsigned>    WOpt('W', "preamp-voltage","VFP\t\t");
  Option<unsigned>    HOpt('H', "hold-delay",    "Hold time in 25ns steps");
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(nOpt);
  cl.Add(cOpt);
  cl.Add(lOpt);
  cl.Add(dOpt);
  cl.Add(rOpt);
  cl.Add(oOpt);
  cl.Add(tOpt);
  cl.Add(eOpt);
  cl.Add(OOpt);
  cl.Add(POpt);
  cl.Add(mOpt);
  cl.Add(MOpt);
  cl.Add(BOpt);
  cl.Add(VOpt);
  cl.Add(WOpt);
  cl.Add(HOpt);
  cl.Add(vOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "fmddaq version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? 
			cl.Remain()[0] : "/dev/altro0");
  Rcuxx::Acq::Trigger_t mode = (tOpt->find("software") != std::string::npos ? 
				Rcuxx::Acq::kSoftwareTrigger : 
				Rcuxx::Acq::kExternalTrigger);
  
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet());
  if (!rcu) return 1;
  if (dOpt->find("rcuxx")!=std::string::npos) 
    rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
  if (dOpt->find("backend")!=std::string::npos) 
    rcu->SetDebug(Rcuxx::Rcu::kBackend,1);
  try {
    Rcuxx::Altro        altro(*rcu);
    Rcuxx::Fmd          fmd(*rcu);
    Fmdxx::Acq          acq(*rcu, fmd);
    altro.SetDebug(dOpt->find("altro") != std::string::npos);
    fmd.SetDebug(dOpt->find("fmd") != std::string::npos);

    acq.SetRange(mOpt, MOpt);
    if (OOpt > 0) acq.SetOver(OOpt);
    if (POpt > 0) acq.SetPulser(POpt);
    if (BOpt > 0) acq.SetShapeBias(BOpt);
    if (VOpt > 0) acq.SetVFS(VOpt);
    if (WOpt > 0) acq.SetVFP(WOpt);
    if (HOpt > 0) acq.SetHoldWait(HOpt);
    acq.SetWait(50000);
    acq.SetOutName(oOpt->c_str());
    
    // Install a signal handler so we may react propely to signals
    Rcuxx::Acq::InstallSignalHandler();
    
    // Return values.
    unsigned int ret = 0;

    // Active FE cards
    rcu->ACTFEC()->SetValue(0x0);
    unsigned int acl[4*8];
    for (size_t i = 0; i < 4 * 8; i++) acl[i] = 0;
    for (size_t i = 0; i < 4; i++) {
      if (!(cOpt & (1 << i)) || lOpt == 0x0) continue; 
      rcu->ACTFEC()->SetOn(i, true);
      for (size_t j = 0; j < 3; j++) {
	if (!(lOpt & (1 << j))) continue;
	rcu->ACL()->EnableChip(i, j, (j == 1 ? 0xff : 0xffff));
      }
    }
    rcu->ACTFEC()->Print();
    // rcu->ACL()->Set(0, 4*8, acl);

    // Trigger configuration 
    rcu->TRCFG1()->SetPop(true);
    rcu->TRCFG1()->SetBMD(Rcuxx::RcuTRCFG1::k4Buffers);
    // rcu->TRCFG1()->SetBMD(Rcuxx::RcuTRCFG1::k4Buffers);
    rcu->TRCFG1()->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2);
    rcu->TRCFG1()->SetTwv(4096); //  * samples + 100;

    acq.SetDebug(dOpt->find("acq") != std::string::npos);
    // Set up
    if ((ret = acq.Setup(rOpt, nOpt, mode))) throw ret;

    // Run it 
    if ((ret = acq.Run())) throw ret;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return ret;
  }
  
  // Return OK
  return 0;
}

//
// EOF
//
