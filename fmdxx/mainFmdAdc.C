/** @file    mainFmdAdc.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jan 16 18:26:44 2007
    @brief   Read monitor ADCs from a FEC.
*/
#ifndef __CINT__
#include <rcuxx/Rcu.h>
#include <rcuxx/Fmd.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>
#include <vector>
#include <unistd.h>
#include <TH1.h>
#include "config.h"
#include <rcudata/Options.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcCSR2.h>
#include <rcuxx/fmd/FmdAL_ANA_I.h>
#include <rcuxx/fmd/FmdAL_ANA_U.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdAL_DIG_U.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/fmd/FmdGTL_U.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdVA_REC_UM.h>
#include <rcuxx/fmd/FmdVA_REC_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_IM.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_SUP_UM.h>
#include <rcuxx/fmd/FmdVA_SUP_UP.h>
#endif


//____________________________________________________________________
std::string convSpaces(const std::string& in)
{
  std::string out;
  for (size_t i = 0; i < in.length(); i++) 
    out.push_back(in[i] == ' ' ? '_' : in[i]);
  return out;
}

//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",      "This help", false, false); 
  Option<bool>        vOpt('v', "version",   "Show version",false,false);
  Option<long>        nOpt('n', "events",    "Number of events", 10);
  Option<unsigned>    cOpt('c', "boards",    "Bit pattern of FECs",1); 
  Option<std::string> dOpt('d', "debug",     "Turn on debug messages");
  Option<std::string> oOpt('o', "output",    "Output file name");
  Option<unsigned>    tOpt('t', "tries",     "Number of tries\t", 20);
  Option<unsigned>    wOpt('w', "wait",      "Time to wait in us", 100);
  Option<bool>        eOpt('e', "emulation", "Emulation",false,false);
  Option<bool>        VOpt('V', "no-va1s",   "Turn off VA1s",false,false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(nOpt);
  cl.Add(cOpt);
  cl.Add(dOpt);
  cl.Add(oOpt);
  cl.Add(tOpt);
  cl.Add(wOpt);
  cl.Add(eOpt);
  cl.Add(VOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "fmdadc version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? 
			cl.Remain()[0] : "/dev/altro0");

  
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet());
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (dOpt->find("rcuxx")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
    if (dOpt->find("backend")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kBackend,1);
    
		  ;

    Rcuxx::Fmd fmd(*rcu);
    fmd.SetDebug(dOpt->find("fmd")!=std::string::npos);

    // Set FMDD address
    fmd.SetAddress(cOpt);

    // Return value 
    unsigned int ret = 0;

    // Disable triggers 
    if (rcu->L1_CMD()) {
      std::cout << "RCU: L1_CMD ... " << std::flush;
      if ((ret = rcu->L1_CMD()->Commit())) throw ret;
      usleep(wOpt);
      std::cout << "done" << std::endl;
    }

    // Reset FECs
    std::cout << "RCU: FECRST ... " << std::flush;
    if ((ret = rcu->FECRST()->Commit())) throw ret;
    usleep(wOpt);
    if ((ret = rcu->FECRST()->Commit())) throw ret;
    usleep(wOpt);
    std::cout << "done" << std::endl;

    if (VOpt.IsSet()) {
      Rcuxx::BcCSR2* csr2 = fmd.CSR2();
      if ((ret = csr2->Update())) throw ret;
      csr2->SetPASASwitch(false);
      csr2->Print();
      if ((ret = csr2->Commit())) throw ret;
      if ((ret = csr2->Update())) throw ret;
      csr2->Print();
    }
      
    if (oOpt->empty()) {
      std::stringstream s;
      s << "monitor_0x" << std::setw(2) << std::setfill('0') 
	<< std::hex << cOpt;
      oOpt.Handle('o',s.str().c_str());
    }
    // Open output file 
    TFile* file = TFile::Open(Form("%s.root", oOpt->c_str()), "RECREATE");
    if (!file) throw std::runtime_error("Failed to open output file");
    
    // Make a list of monitors to get 
    typedef std::vector<Rcuxx::BcMonitored*> MonList;
    MonList mons;
    mons.push_back(fmd.T1());
    mons.push_back(fmd.FLASH_I());
    mons.push_back(fmd.AL_DIG_I());
    mons.push_back(fmd.AL_ANA_I());
    mons.push_back(fmd.VA_REC_IP());
    mons.push_back(fmd.T2());
    mons.push_back(fmd.VA_SUP_IP());
    mons.push_back(fmd.VA_REC_IM());
    mons.push_back(fmd.VA_SUP_IM());
    mons.push_back(fmd.GTL_U());
    mons.push_back(fmd.T3());
    mons.push_back(fmd.T1SENS());
    mons.push_back(fmd.T2SENS());
    mons.push_back(fmd.AL_DIG_U());
    mons.push_back(fmd.AL_ANA_U());
    mons.push_back(fmd.T4());
    mons.push_back(fmd.VA_REC_UP());
    mons.push_back(fmd.VA_SUP_UP());
    mons.push_back(fmd.VA_SUP_UM());
    mons.push_back(fmd.VA_REC_UM());
  
    // Set up a tree 
    TTree*      tree     = new TTree("monitor", "Monitor");
    Int_t       adc[20]; //      = new Int_t[mons.size()];
    Float_t     nat[20]; //      = new Float_t[mons.size()];
    Float_t*    sumAdc   = new Float_t[mons.size()];
    Float_t*    sumNat   = new Float_t[mons.size()];
    Float_t*    sum2Adc  = new Float_t[mons.size()];
    Float_t*    sum2Nat  = new Float_t[mons.size()];
    Int_t       nFill    = 0;
    size_t      widest   = 0;
    std::string adc_leafs;
    std::string nat_leafs;
    for (size_t i = 0; i < mons.size();  ++i) {
      std::string name = convSpaces(mons[i]->Name());
      widest = std::max(widest, name.length());
      if (i != 0) {
	adc_leafs.append(":");
	nat_leafs.append(":");
      }
      adc_leafs.append(name);
      nat_leafs.append(name);
      adc_leafs.append("/I");
      nat_leafs.append("/F");
      sumAdc[i]  = 0;
      sumNat[i]  = 0;
      sum2Adc[i] = 0;
      sum2Nat[i] = 0;
      
    }
    tree->Branch("adc",     &adc, adc_leafs.c_str());
    tree->Branch("natural", &nat, nat_leafs.c_str());
    
    std::cout << "# of events: " << nOpt << std::endl;
    // Loop for requested events 
    for (unsigned event = 0; event < nOpt; event++) {
      std::cout << "Event # " << event << " " << std::flush;
      // Start conversion 

      fmd.STCNV()->Commit();
      // Wait until the conversion is done 
      int i = 0;
      for (i = 0; i < tOpt; i++) {
	usleep(1);
	std::cout << '.' << std::flush;
	fmd.CSR3()->Update();
	if (fmd.CSR3()->IsCnvEnd()) break;
      }

      // Wait a while 
      usleep(wOpt);

      // Check if we timed-out
      if (i == tOpt) {
	std::cout << " time-out" << std::endl;
	continue;
      }

      // put values in tree 
      for (size_t i = 0; i < mons.size(); i++) {
	std::cout << '+' << std::flush;
	mons[i]->Update();
	adc[i] = mons[i]->Current();
	nat[i] = mons[i]->CurrentNatural();
	sumAdc[i]  += adc[i];
	sumNat[i]  += nat[i];
	sum2Adc[i] += adc[i] * adc[i];
	sum2Nat[i] += nat[i] * nat[i];	
      }
      // std::cout << "\nGot ";
      // for (size_t i = 0; i < mons.size(); i++) 
      //   std::cout << "\t" << adc[i];
      
      nFill++;
      tree->Fill();
      std::cout << " done" << std::endl;
    }
    std::ofstream outf(Form("%s.csv", oOpt->c_str()));
    outf << "Name,Unit,\"Mean ADC\",\"Var Adc\",\"Mean Val\",\"Var Val\""
	 << std::endl;
    
    std::cout << std::setw(widest) << "Name" 
	      << " | Mean ADC | Var ADC  | Unit | Mean Val | Var Val\n"
	      << std::setw(widest+1) << std::setfill('-') << '-'
	      << "+----------+----------+------+----------+---------"
	      << std::setfill(' ') << std::endl;
    for (size_t i = 0;  i < mons.size(); ++i) {
      TH1* h = new TH1F(Form("adc%02d", i), mons[i]->Name().c_str(), 
			1024, -0.5, 1023);
      h->GetXaxis()->SetTitle("ADC"); // mons[i]->Unit());
      h->GetYaxis()->SetTitle("N");
      std::string var = convSpaces(mons[i]->Name().c_str());
      tree->Project(Form("adc%02d", i), Form("adc.%s", var.c_str()));
      Float_t meanAdc  = sumAdc[i] / nFill;
      Float_t varAdc   = TMath::Sqrt((sum2Adc[i] - meanAdc * meanAdc * nFill) 
				     / (nFill - 1));
      Float_t meanNat  = sumNat[i] / nFill;
      Float_t varNat   = TMath::Sqrt((sum2Nat[i] - meanNat * meanNat * nFill) 
				     / (nFill - 1));
      std::cout << std::setw(widest) << mons[i]->Name() << " | " 
		<< std::setw(8)      << meanAdc         << " | " 
		<< std::setw(8)      << varAdc          << " | " 
		<< std::setw(4)      << mons[i]->Unit() << " | " 
		<< std::setw(8)      << meanNat         << " | " 
		<< std::setw(8)      << varNat          << std::endl;
      outf << '"' << mons[i]->Name() << "\",\"" << mons[i]->Unit() 
	   << "\"," << meanAdc << ',' << varAdc << ',' 
	   << meanNat << ',' << varNat << std::endl;
    }
    outf.close();
    file->Write();
    file->Close();
  }
  catch (unsigned int ret) {
    if (!rcu) 
      std::cerr << "No device opened" << std::endl;
    else 
      std::cerr << "Error: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
//
// EOF
//
    
	
	
	
	 

