#include "config.h"
#include <TStyle.h>
#include <TSystem.h>
#include <rcugui/slow/Fec.h>
#include <rcugui/slow/GraphTH.h>
#include <rcugui/slow/Canvas.h>
#include <rcugui/slow/Table.h>
#include <rcugui/slow/Top.h>
#include <rcugui/Url.h>
#include <rcugui/LabeledNumber.h>
#include <rcudata/Options.h>
#include <iostream>
#include <string>
#include <TApplication.h>
#include <TH1F.h>

// TF1 funcT("funcT",  "1/4.00*x-256*(x>=512)");
TF1 funcT("funcT",  "0.25 * x");
TF1 funcTSens("funcTSens", 
	      "100 * 0.442401 * x / "
	      "(0.3851 * (2.2 * 512 - 0.221301 * x))"); // 512 is VA_REC_UP
TF1  funcV("funcV", "5. * x / 1023");
TF1  funcA("funcA", "5.25 * x / 1023");
TF1  funcM("funcM", 
	   "((x >= 512) * 5. * (x-512) - "
	   "((x <  512) * 5. * (512-x)))/1023"); // 512 is VA_REC_UP

TF1* funcVA_REC_UP = &funcV;
TF1* funcVA_SUP_UP = &funcV;
TF1* funcAL_ANA_U  = &funcV;
TF1* funcAL_DIG_U  = &funcV;
TF1* funcGTL_U     = &funcV;
TF1* funcVA_REC_UM = &funcM;
TF1* funcVA_SUP_UM = &funcM;
TF1* funcFLASH_I   = &funcA;
TF1* funcAL_DIG_I  = &funcA;
TF1* funcAL_ANA_I  = &funcA;
TF1* funcVA_REC_IP = &funcA;
TF1* funcVA_SUP_IP = &funcA;
TF1* funcVA_REC_IM = &funcA;
TF1* funcVA_SUP_IM = &funcA;


const std::vector<std::string>& 
makeHeaders()
{
  static std::vector<std::string> r;
  if (r.size() <= 0) {
    r.push_back("T1");
    r.push_back("T2");
    r.push_back("T3");
    r.push_back("T4");
    r.push_back("T1_SENS");
    r.push_back("T2_SENS");
    r.push_back("AL_DIG_I");
    r.push_back("AL_DIG_U");
    r.push_back("AL_ANA_I");
    r.push_back("AL_ANA_U");
    r.push_back("VA_REC_IP");
    r.push_back("VA_REC_UP");
    r.push_back("VA_REC_IM");
    r.push_back("VA_REC_UM");
    r.push_back("VA_SUP_IP");
    r.push_back("VA_SUP_UP");
    r.push_back("VA_SUP_IM");
    r.push_back("VA_SUP_UM");
    r.push_back("FLASH_I");
    r.push_back("GTL_U");
  }
  return r;
}

const std::vector<std::string>& 
makeLabels()
{
  static std::vector<std::string> r;
  if (r.size() <= 0) {
    r.push_back("Value");
    r.push_back("ADC");
    r.push_back("Threshold");
    r.push_back("ADC");
  }
  return r;
}

class Fec : public RcuGui::Slow::Fec
{
public:
  Fec(const std::string& name, unsigned char num, TGTab& tabs,
      bool horiz)
    : RcuGui::Slow::Fec(name, num, tabs, horiz),
      fTable(fInner, makeHeaders(), makeLabels(), !horiz), 
      fCs(&fInner, kVerticalFrame),
      fCT(fCs, "Temperatures", "Temperature [C]", 600, 300, 0, 60),
      fCA(fCs, "Currents",     "Currents [A]",    600, 300, 0, 3),
      fCV(fCs, "Voltages",     "Votlages [V]",    600, 300, -2.5, 3.6) 
  {
    fInner.AddFrame(&fCs, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,
					    2, 2, 5, 4));
    using RcuGui::Slow::GraphTH;
    Register(new GraphTH(MakeName("T1"), "Temperature 1",
			 fCT, fTable, kRed, &funcT));
    Register(new GraphTH(MakeName("T2"), "Temperature 2",
			 fCT, fTable, kGreen, &funcT));
    Register(new GraphTH(MakeName("T3"), "Temperature 3",
			 fCT, fTable, kBlue, &funcT));
    Register(new GraphTH(MakeName("T4"), "Temperature 4",
			 fCT, fTable, kCyan+1, &funcT));
    Register(new GraphTH(MakeName("T1_SENS"), "Temperature Sensor 1",
			 fCT, fTable, kMagenta, &funcTSens));
    Register(new GraphTH(MakeName("T2_SENS"), "Temperature Sensor 2",
			 fCT, fTable, kYellow+1,&funcTSens));

    Register(new GraphTH(MakeName("AL_DIG_I"), "ALTRO digital current", 
			 fCA, fTable, kRed, funcAL_DIG_I));
    Register(new GraphTH(MakeName("AL_DIG_U"), "ALTRO digitial voltage",
			 fCV, fTable, kRed, funcAL_DIG_U, false));
    Register(new GraphTH(MakeName("AL_ANA_I"), "ALTRO analog current", 
			 fCA, fTable, kGreen, funcAL_ANA_I));
    Register(new GraphTH(MakeName("AL_ANA_U"), "ALTRO analog voltage", 
			 fCV, fTable, kGreen,funcAL_ANA_U,
			 false));
    
    Register(new GraphTH(MakeName("VA_REC_IP"), "Reciever current (+)",
			 fCA, fTable, kBlue, funcVA_REC_IP));
    Register(new GraphTH(MakeName("VA_REC_UP"), "Reciever positive voltage",
			 fCV, fTable, kBlue, funcVA_REC_UP,  false));
    
    Register(new GraphTH(MakeName("VA_REC_IM"), "Reciever current (-)",
			 fCA, fTable, kCyan+1, funcVA_REC_IM));
    Register(new GraphTH(MakeName("VA_REC_UM"), "Reciever negative voltage",
			 fCV, fTable, kCyan+1, funcVA_REC_UM));

    Register(new GraphTH(MakeName("VA_SUP_IP"), "VA1 current (+)", 
			 fCA, fTable, kMagenta, funcVA_SUP_IP));
    Register(new GraphTH(MakeName("VA_SUP_UP"), "VA1 positive voltage",
			 fCV, fTable, kMagenta, funcVA_SUP_UP, 
		       false));

    Register(new GraphTH(MakeName("VA_SUP_IM"),"VA1 current (-)", 
			 fCA, fTable, kYellow+1, funcVA_SUP_IM));
    Register(new GraphTH(MakeName("VA_SUP_UM"), "VA1 negative voltage", 
			 fCV, fTable, kYellow+1,funcVA_SUP_UM));

    Register(new GraphTH(MakeName("FLASH_I"), "Flash current", 
			 fCA, fTable, kBlack, funcFLASH_I));
    Register(new GraphTH(MakeName("GTL_U"),"GTL voltage", 
			 fCV, fTable, kBlack, funcGTL_U,  false));
  }
public:
  RcuGui::Slow::Table fTable;
  TGCompositeFrame fCs;
  RcuGui::Slow::Canvas fCT;
  RcuGui::Slow::Canvas fCA;
  RcuGui::Slow::Canvas fCV;
};

  
class FecFactory : public RcuGui::Slow::FecFactory
{
public:
  RcuGui::Slow::Fec* MakeFec(const std::string& name, unsigned char num, 
			     TGTab& tabs)
  {
    std::cout << "Request FEC 0x" << int(num) << std::dec << std::endl;
    switch (int(num)) { 
    case 0x0: 
    case 0x1: 
    case 0x10:
    case 0x11:
      break;
    default:
      return 0;
    }
    std::cout << "Making fec for 0x" << std::hex << int(num) 
	      << std::dec << std::endl;
    return new Fec(name, num, tabs, true);
  }
};

int
main(int argc, char** argv)
{
  Option<bool> hOpt('h', "help",    "Show this help", false, false);
  Option<bool> vOpt('v', "version", "Show version", false, false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
                                                                              
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt) {
    cl.Help();
    std::cout 
      << "SOURCE should have the form\n\n"
      << "\tdim://DIM_DNS_NODE/FEESERVER_NAME\n\n"
      << "where DIM_DNS_NODE is the host name or ip address of the\n"
      << "host running the DIM domain name server, and FEESERVER_NAME\n"
      << "is the name of the server to monitor values from" 
      << std::endl;
    return 0;
  }


  if (vOpt) {
    std::cout << "rcugui version " << VERSION << std::endl;
    return 0;
  }
  RcuGui::Url url = (cl.Remain().size() > 0 ? cl.Remain()[0] :
                     "fee://localhost/FMD-FEE_0_0_0");
  std::cout << "URL is " << url.Raw() << std::endl;
  
  DimClient::setDnsNode(url.Host().c_str());
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);

  TApplication app("app", 0, 0);
  RcuGui::Slow::Top m(gSystem->BaseName(url.Path().c_str()));
  m.Rcu().SetFactory(new FecFactory);
  app.Run();

  return 0;
}
