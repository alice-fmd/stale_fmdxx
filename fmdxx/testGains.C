//
#include <fmdxx/Gains.h>
#include <TFile.h>
#include <TRandom.h>
#include <TApplication.h>
#include <TBrowser.h>
#include <TF1.h>
#include <stdexcept>

TF1* 
MakeResponse(unsigned int par) 
{
  TF1* ret = new TF1("response", "pol1", 0, 256);
  ret->SetParameters(0, 1);
  par = 1;
  return ret;
}


int
main()
{
  
  try {
    
    Fmdxx::Gains::Top top;
    size_t dpulse   = 32;
    size_t nevents  = 100;
    size_t par      = 1;
    size_t smear    = 2;
    TF1*   response = MakeResponse(par);
  
  
    for (size_t ircu = 3072; ircu < 3075; ircu++) {
      for (size_t iboard = 0; iboard < 2; iboard++) {
	for (size_t ichip = 0; ichip < 3; ichip++) {
	  for (size_t ichan = 0; ichan < 16; ichan++) {
	    for (size_t istrip = 0; istrip < 16; istrip++) {
	      std::cout << "Rcu="    << ircu 
			<< " Board=" << iboard 
			<< " Chip="  << ichip 
			<< " Chan="  << ichan 
			<< " Strip=" << istrip 
			<< std::endl;
	      for (size_t pulse = 0; pulse < 256; pulse += dpulse) {
		for (size_t events = 0; events < nevents; events++) {
		  unsigned adc = unsigned(TMath::Max(0., response->Eval(pulse) + 
						     gRandom->Gaus(0, smear)));
		  top.Fill(ircu, iboard, ichip, ichan, istrip, pulse, adc);
		}
	      }
	    }
	  }
	}
      }
    }
    std::cout << "Write out to test.root" << std::endl;  
    TFile* out = TFile::Open("test.root", "RECREATE");
    top.CalculateGain(response, par);
    std::cout << "Writing Rcu object" << std::endl;
    top.WriteOut();
    out->Close();
    std::cout << "done " << std::endl;
  }
  catch (std::exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
  }
  
  return 0;
}
