dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: fmdxx.m4,v 1.2 2009-02-09 23:09:50 hehi Exp $ 
dnl  
dnl  ROOT generic fmdxx framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl AC_FMDXX([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_FMDXX],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([fmdxx-prefix],
        [AC_HELP_STRING([--with-fmdxx-prefix],
		[Prefix where Fmd++ is installed])],
        fmdxx_prefix=$withval, fmdxx_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([fmdxx-url],
        [AC_HELP_STRING([--with-fmdxx-url],
		[Base URL where the Fmd++ dodumentation is installed])],
        fmdxx_url=$withval, fmdxx_url="")
    if test "x${FMDXX_CONFIG+set}" != xset ; then 
        if test "x$fmdxx_prefix" != "x" ; then 
	    FMDXX_CONFIG=$fmdxx_prefix/bin/fmdxx-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(FMDXX_CONFIG, fmdxx-config, no)
    fmdxx_min_version=ifelse([$1], ,0.3,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Fmd++ version >= $fmdxx_min_version)

    # Check if we got the script
    fmdxx_found=no    
    if test "x$FMDXX_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       FMDXX_CPPFLAGS=`$FMDXX_CONFIG --cppflags`
       FMDXX_INCLUDEDIR=`$FMDXX_CONFIG --includedir`
       FMDXX_LIBS=`$FMDXX_CONFIG --libs`
       FMDXX_LTLIBS=`$FMDXX_CONFIG --ltlibs`
       FMDXX_LIBDIR=`$FMDXX_CONFIG --libdir`
       FMDXX_LDFLAGS=`$FMDXX_CONFIG --ldflags`
       FMDXX_LTLDFLAGS=`$FMDXX_CONFIG --ltldflags`
       FMDXX_PREFIX=`$FMDXX_CONFIG --prefix`
       
       # Check the version number is OK.
       fmdxx_version=`$FMDXX_CONFIG -V` 
       fmdxx_vers=`echo $fmdxx_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       fmdxx_regu=`echo $fmdxx_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $fmdxx_vers -ge $fmdxx_regu ; then 
            fmdxx_found=yes
       fi
    fi
    AC_MSG_RESULT($fmdxx_found - is $fmdxx_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_FMDXX, [Whether we have fmdxx])


    if test "x$fmdxx_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS $FMDXX_LDFLAGS"
    	CPPFLAGS="$CPPFLAGS $FMDXX_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_fmdxx_reader_h=0
        AC_CHECK_HEADER([fmdxx/data/Reader.h], [have_fmdxx_reader_h=1])

        # Check the library. 
        have_libfmdxx=no
        AC_MSG_CHECKING(for -lfmdxx)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <fmdxx/data/Reader.h>],
                        [Fmdxx::Data::Reader::MakeReader("foo","bar")])], 
                        [have_libfmdxx=yes])
        AC_MSG_RESULT($have_libfmdxx)

        if test $have_fmdxx_reader_h -gt 0    && \
            test "x$have_libfmdxx"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_FMDXX)
        else 
            fmdxx_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
	CPPFLAGS=$save_CPPFLAGS
	LDFLAGS=$save_LDFLAGS
    fi

    AC_MSG_CHECKING(where the Fmd++ documentation is installed)
    if test "x$fmdxx_url" = "x" && \
	test ! "x$FMDXX_PREFIX" = "x" ; then 
       FMDXX_URL=${FMDXX_PREFIX}/share/doc/fmdxx/html
    else 
	FMDXX_URL=$fmdxx_url
    fi	
    AC_MSG_RESULT($FMDXX_URL)
   
    if test "x$fmdxx_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FMDXX_URL)
    AC_SUBST(FMDXX_PREFIX)
    AC_SUBST(FMDXX_CPPFLAGS)
    AC_SUBST(FMDXX_INCLUDEDIR)
    AC_SUBST(FMDXX_LDFLAGS)
    AC_SUBST(FMDXX_LIBDIR)
    AC_SUBST(FMDXX_LIBS)
    AC_SUBST(FMDXX_LTLIBS)
    AC_SUBST(FMDXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
